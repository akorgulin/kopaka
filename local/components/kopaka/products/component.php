<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
    $arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {
    $arParams["COUNT_RECORDS"] = false;
}

CBitrixComponent::includeComponentClass("bitrix:catalog.viewed.products");


if (!function_exists("getOrdersFilter")) {
    /**
     * Returns orders filter for GetBestSellerList method.
     * @return mixed[]
     */
    function getOrdersFilter($arParams, $data)
    {
        if (!empty($arParams['FILTER'])) {
            $filter = (defined('SITE_ID') && !SITE_ID ? array('=LID' => SITE_ID) : array());
            $subFilter = array("LOGIC" => "OR");

            $statuses = array(
                "CANCELED" => true,
                "ALLOW_DELIVERY" => true,
                "PAYED" => true,
                "DEDUCTED" => true
            );
            if ($arParams['PERIOD'] > 0) {
                $date = ConvertTimeStamp(AddToTimeStamp(array("DD" => "-" . $arParams['PERIOD'])));
                if (!empty($date)) {
                    foreach ($arParams['FILTER'] as &$field) {
                        if (isset($statuses[$field])) {
                            $subFilter[] = array(
                                ">=DATE_{$field}" => $date,
                                "={$field}" => "Y"
                            );
                        } else {
                            if (empty($data['ORDER_STATUS']) || in_array($field, $data['ORDER_STATUS'])) {
                                $subFilter[] = array(
                                    "=STATUS_ID" => $field,
                                    ">=DATE_UPDATE" => $date,
                                );
                            }
                        }
                    }
                    unset($field);
                }
            } else {
                foreach ($arParams['FILTER'] as &$field) {
                    if (isset($statuses[$field])) {
                        $subFilter[] = array(
                            "={$field}" => "Y"
                        );
                    } else {
                        if (empty($data['ORDER_STATUS']) || in_array($field, $data['ORDER_STATUS'])) {
                            $subFilter[] = array(
                                "=STATUS_ID" => $field,
                            );
                        }
                    }
                }
                unset($field);
            }
            $filter[] = $subFilter;
            return $filter;
        }

        return array();
    }

}
if (CModule::IncludeModule("iblock")) {
    if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";

    if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";

    $arParams['AJAX'] = $_REQUEST['AJAX'];

    $arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
    if ($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;

    $arResult['ORDABC'] = (int)$_REQUEST['abc'];
    $arResult['BRAND_ID'] = (int)$_REQUEST['id'];
    $arResult['BRAND_CODE'] = $_REQUEST['elmid'];
    $arResult['LETTER'] = mb_substr($_REQUEST['let'], 0, 1);

    $arFilter = array(
        'ACTIVE' => "Y",
        'IBLOCK_CODE' => $arParams['PRODUCTS_IBLOCK_CODE']
    );

    if ($arParams["NEW_PRODUCTS"] == "Y") {

        $arFilter["!PROPERTY_NEWPRODUCT_VALUE"] = false;
        $arFilter["INCLUDE_SUBSECTIONS"] = "Y";

        $rsElement = CIBlockElement::GetList(array('NAME' => 'ASC'), $arFilter, false, false);
        $arResult['NEW_PRODUCTS'] = [];
        $arResult['RUS'] = array();
        while ($data = $rsElement->Fetch()) {
            $arResult['NEW_PRODUCTS'][] = $data;
            $arResult['NEW_PRODUCTS_IDS'][] = $data["ID"];
        }
    }


    if ($arParams["BESTSELLERS"] == "Y") {

        $arResult['BESTSELLERS_PRODUCTS'] = [];
        $ordersfilter = getOrdersFilter($arParams, $data);

        if (!empty($ordersfilter)) {
            $productIds = array();
            $productIterator = CSaleProduct::GetBestSellerList(
                $arParams["BY"],
                array(),
                $ordersfilter,
                $arParams["COUNT_ITEMS"]
            );
            while ($product = $productIterator->fetch()) {
                $arResult['BESTSELLERS_PRODUCTS'][] = $product;
                $arResult['BESTSELLERS_PRODUCTS_IDS'][] = $product["ID"];
            }
        }
    }


    if ($arParams["SALE_PRODUCTS"] == "Y") {

        $arResult['SALE_PRODUCTS'] = [];
        $arFilterSale = array(
            'IBLOCK_CODE' => $arParams['PRODUCTS_IBLOCK_CODE'],
            "INCLUDE_SUBSECTIONS" => "Y"
        );
        $arFilterSale["!PROPERTY_SPECIALOFFER_VALUE"] = false;

        $productIterator = CIBlockElement::GetList(array('NAME' => 'ASC'), $arFilterSale);
        while ($product = $productIterator->fetch()) {
            $arResult['SALE_PRODUCTS'][] = $product;
            $arResult['SALE_PRODUCTS_IDS'][] = $product["ID"];
        }
    }

}


//if( !empty($arResult['BRAND_CODE']) )
//	foreach($arResult['BRANDS'] as $val)
//		$APPLICATION -> AddChainItem($val['NAME'], "");


$this->IncludeComponentTemplate();
?>