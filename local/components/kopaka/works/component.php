<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}

Loader::includeModule("highloadblock");
if (!CModule::IncludeModule("highloadblock"))
    return;

if( CModule::IncludeModule("iblock") )
{
	if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";
	
	if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";
	
	$arParams['AJAX'] = $_REQUEST['AJAX'];
	
	//$arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
	if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;
	
	$arResult['ORDABC'] = (int)$_REQUEST['abc'];
	$arResult['BRAND_ID'] = (int)$_REQUEST['id'];
	$arResult['BRAND_CODE'] = $_REQUEST['elmid'];
	$arResult['LETTER'] = mb_substr($_REQUEST['let'], 0, 1);
	
	$arFilter = array(
		'ACTIVE' => "Y",
		'IBLOCK_CODE' => $arParams['TYPES_IBLOCK_CODE']
	);

	$IBLOCK_ID = 0;

    $res = CIBlock::GetList(
        Array(),
        Array(
            'SITE_ID'=>SITE_ID,
            'ACTIVE'=>'Y',
            "CODE"=>$arParams['TYPES_IBLOCK_CODE']
        ), true
    );
    if($ar_res = $res->Fetch())
    {
        $IBLOCK_ID = $ar_res["ID"];
    }


	if($arResult['ORDABC'] > 0) $arNavStartParams = false;
	else {
		
		$arNavStartParams = array(
			"nPageSize" => $arParams['nPageSize'],
			//"iNumPage" => $iNumPage,
			"bShowAll" => false
		);
	
	}
	//  If the specified number of valid entries that do not need a navigation
	if ($arParams["COUNT_RECORDS"] > 0 ) {
		$arNavStartParams = false;
		$arNavStartParams = array(
			"nTopCount" => $arParams["COUNT_RECORDS"],
			"bShowAll" => false
		);
	}

    $arResult['TYPES_PROPERTY'] = [];

    $res = CIBlock::GetProperties($IBLOCK_ID, Array(), Array("CODE"=>"TYPE_WORK"));

    if($res_arr = $res->Fetch()) {

        $dbHblock = HL\HighloadBlockTable::getList(
            array(
                "select" => array('*'), //выбираем все поля
                "filter" => array("NAME" => "Typework", "TABLE_NAME" => $res_arr["USER_TYPE_SETTINGS"]["TABLE_NAME"]),
            )
        );
        if ($hldata = $dbHblock->Fetch())
        {
            $ID = $hldata["ID"];
            $hldata = HL\HighloadBlockTable::getById($ID)->fetch();
            $hlentity = HL\HighloadBlockTable::compileEntity($hldata);
            $entity_data_class = $hlentity->getDataClass();
            $entity_table_name = $hldata['TABLE_NAME'];
            $sTableID = 'tbl_'.$entity_table_name;

            $optionsQuery = array(
                "select" => array('*'), //выбираем все поля
                "order" => array("UF_SORT"=>"ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
            );
            if($arNavStartParams["nTopCount"]>0) {
                $optionsQuery["limit"] = $arNavStartParams["nTopCount"];
            }

            $rsData = $entity_data_class::getList($optionsQuery);

            while ($arEnum = $rsData->fetch())
            {

                $boolPict = true;
                if (!isset($arEnum['UF_NAME']))
                {
                    $boolName = false;
                    break;
                }

                if (!isset($arEnum['UF_FILE']) || (int)$arEnum['UF_FILE'] <= 0)
                    $boolPict = false;

                if ($boolPict)
                {
                    $arEnum['PREVIEW_PICTURE'] = CFile::GetFileArray($arEnum['UF_FILE']);
                    if (empty($arEnum['PREVIEW_PICTURE']))
                        $boolPict = false;
                }

                $descrExists = (isset($arEnum['UF_DESCRIPTION']) && (string)$arEnum['UF_DESCRIPTION'] !== '');
                if ($boolPict)
                {
                    if ($descrExists)
                    {
                        $width = $arParams["WIDTH_SMALL"];
                        $height = $arParams["HEIGHT_SMALL"];
                        $type = "PIC_TEXT";
                    }
                    else
                    {
                        $width = $arParams["WIDTH"];
                        $height = $arParams["HEIGHT"];
                        $type = "ONLY_PIC";
                    }

                    $arEnum['PREVIEW_PICTURE']['WIDTH'] = (int)$arEnum['PREVIEW_PICTURE']['WIDTH'];
                    $arEnum['PREVIEW_PICTURE']['HEIGHT'] = (int)$arEnum['PREVIEW_PICTURE']['HEIGHT'];
                    if (
                        $arEnum['PREVIEW_PICTURE']['WIDTH'] > $width
                        || $arEnum['PREVIEW_PICTURE']['HEIGHT'] > $height
                    )
                    {
                        $arEnum['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
                            $arEnum['PREVIEW_PICTURE'],
                            array("width" => $width, "height" => $height),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            true
                        );

                        $arEnum['PREVIEW_PICTURE']['SRC'] = $arEnum['PREVIEW_PICTURE']['src'];
                        $arEnum['PREVIEW_PICTURE']['WIDTH'] = $arEnum['PREVIEW_PICTURE']['width'];
                        $arEnum['PREVIEW_PICTURE']['HEIGHT'] = $arEnum['PREVIEW_PICTURE']['height'];
                    }
                }
                elseif ($descrExists)
                {
                    $type = "ONLY_TEXT";
                }

                $arResult['TYPES_PROPERTY'][$arEnum["ID"]] = $arEnum;
            }

        }
    }


}
$this->IncludeComponentTemplate();
?>