<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$colspan = 2;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;



$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$arParams["bxajaxIdBbitrix"] = $bxajaxid;
$component->bxajaxid = $bxajaxid;

?> <script type="text/javascript"> var bxAjaxIdBbitrix = '<?=$bxajaxid?>';</script>
<div id="comp_<?= $bxajaxid ?>" class="vehicles-list">
    <?if (strlen($arResult["MESSAGE"]) > 0):?>
        <?ShowNote($arResult["MESSAGE"])?>
    <?endif?>

    <?if($arResult["NO_USER"] == "N"):?>

        <?if (count($arResult["ELEMENTS"]) > 0):?>
            <?foreach ($arResult["ELEMENTS"] as $arElement):  ?>
                <?if ($arResult["CAN_EDIT"] == "Y"):?>
                    <?if ($arElement["CAN_EDIT"] == "Y"):?><a href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>"><?=GetMessage("IBLOCK_ADD_LIST_EDIT")?><?else:?>&nbsp;<?endif?></a></td>
                <?endif?>

                <a href="/catalog/technik/<?=$arElement["CODE"]?>/" class="item vehicle-card type-2">
                    <?if ($arResult["CAN_EDIT"] == "Y"):?>
                    <button class="card-edit" onclick="javascript:location.href='<?=$arParams["EDIT_URL"]?>?edit=Y&ID=<?=$arElement["ID"]?>'">
                        <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="17px" height="16px">
                            <path fill-rule="evenodd"  fill="rgb(244, 64, 41)"
                                  d="M16.277,3.651 L14.598,5.332 L11.179,1.910 L10.485,2.605 L13.903,6.028 L6.574,13.367 L3.156,9.945 L2.462,10.640 L5.880,14.063 L5.044,14.900 L5.029,14.884 C4.936,15.036 4.785,15.148 4.606,15.188 L1.419,15.899 C1.371,15.910 1.322,15.915 1.274,15.915 C1.100,15.915 0.931,15.847 0.806,15.721 C0.645,15.560 0.578,15.329 0.628,15.107 L1.338,11.916 C1.378,11.737 1.490,11.586 1.642,11.493 L1.626,11.477 L12.858,0.227 C13.056,0.030 13.378,0.030 13.575,0.228 L16.277,2.933 C16.475,3.131 16.475,3.453 16.277,3.651 Z"/>
                        </svg>
                    </button>
                    <?else:?>
                    <button class="card-edit" onclick="javascript:location.href='<?=$arParams["EDIT_URL"]?>?edit=Y&ID=<?=$arElement["ID"]?>'">
                        <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="17px" height="16px">
                            <path fill-rule="evenodd"  fill="rgb(244, 64, 41)"
                                  d="M16.277,3.651 L14.598,5.332 L11.179,1.910 L10.485,2.605 L13.903,6.028 L6.574,13.367 L3.156,9.945 L2.462,10.640 L5.880,14.063 L5.044,14.900 L5.029,14.884 C4.936,15.036 4.785,15.148 4.606,15.188 L1.419,15.899 C1.371,15.910 1.322,15.915 1.274,15.915 C1.100,15.915 0.931,15.847 0.806,15.721 C0.645,15.560 0.578,15.329 0.628,15.107 L1.338,11.916 C1.378,11.737 1.490,11.586 1.642,11.493 L1.626,11.477 L12.858,0.227 C13.056,0.030 13.378,0.030 13.575,0.228 L16.277,2.933 C16.475,3.131 16.475,3.453 16.277,3.651 Z"/>
                        </svg>
                    </button>
                    <?endif?>

                    <?if ($arResult["CAN_DELETE"] == "Y"):?>
                        <?if ($arElement["CAN_DELETE"] == "Y"):?>
							<button class="card-edit" onClick="javascript:location.href='?delete=Y&amp;CODE=<?=$arElement["ID"]?>&amp;<?=bitrix_sessid_get()?>'; return confirm('<?echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM")))?>')"><?=GetMessage("IBLOCK_ADD_LIST_DELETE")?>
								<svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="17px" height="16px">
                                <path fill-rule="evenodd"  fill="rgb(244, 64, 41)"
                                      d="M16.277,3.651 L14.598,5.332 L11.179,1.910 L10.485,2.605 L13.903,6.028 L6.574,13.367 L3.156,9.945 L2.462,10.640 L5.880,14.063 L5.044,14.900 L5.029,14.884 C4.936,15.036 4.785,15.148 4.606,15.188 L1.419,15.899 C1.371,15.910 1.322,15.915 1.274,15.915 C1.100,15.915 0.931,15.847 0.806,15.721 C0.645,15.560 0.578,15.329 0.628,15.107 L1.338,11.916 C1.378,11.737 1.490,11.586 1.642,11.493 L1.626,11.477 L12.858,0.227 C13.056,0.030 13.378,0.030 13.575,0.228 L16.277,2.933 C16.475,3.131 16.475,3.453 16.277,3.651 Z"/>
                            	</svg>
							</button>
						<?else:?>&nbsp;<?endif?>
                    <?endif?>
                    <div class="card-image">
                        <div class="badges-container">
							<?if($arElement["PROPERTY_STATUS_ENUM_ID"]==51) { ?>
                            	<span class="badge badge-danger text-uppercase"><?=$arElement["PROPERTY_STATUS_VALUE"]?></span>
							<?} else { ?>
								<span class="badge badge-success text-uppercase"><?=$arElement["PROPERTY_STATUS_VALUE"]?></span>
							<? } ?>
                        </div>
						<? $img = CFile::ResizeImageGet($arElement['PREVIEW_PICTURE'], ['width' => 224, 'height' => 140], BX_RESIZE_IMAGE_PROPORTIONAL, false,false);  ?>
                        <img src="<?=$img["src"]?>" width="224" height="140" alt="<?=$arElement['NAME']?>">
                    </div>
                    <div class="card-body">
                        <span class="title"><?=$arElement["NAME"]?></span>
                        <span class="desc">Экскаватор-погрузчик</span>
                        <ul class="card-specs">
							<? if($arElement["PROPERTY_MASS_VALUE"]) { ?>
                            <li class="item">
                                <span>Эксплуатационная масса:</span>
                                <span class="value"><?=$arElement["PROPERTY_MASS_VALUE"]?></span>
                            </li>
							<? } ?>
							<? if($arElement["PROPERTY_EMKOST_VALUE"]) { ?>
                            <li class="item">
                                <span>Емкость ковша погрузчика:</span>
                                <span class="value"><?=$arElement["PROPERTY_EMKOST_VALUE"]?></span>
                            </li>
							<? } ?>
							<? if($arElement["PROPERTY_GLUBINA_VALUE"]) { ?>
                            <li class="item">
                                <span>Макс. глубина копания:</span>
                                <span class="value"><?=$arElement["PROPERTY_GLUBINA_VALUE"]?></span>
                            </li>
							<? } ?>
							<? if($arElement["PROPERTY_ENGINE_VALUE"]) { ?>
                            <li class="item">
                                <span>Мощность двигателя:</span>
                                <span class="value"><?=$arElement["PROPERTY_ENGINE_VALUE"]?></span>
                            </li>
							<? } ?>
                        </ul>
                    </div>
                </a>
            <?endforeach;?>
        <?else:?>
            <h3><?=GetMessage("IBLOCK_ADD_LIST_EMPTY")?></h3>
        <?endif;?>
    <?endif?>
</div>
<?if (strlen($arResult["NAV_STRING"]) > 0):?><?=$arResult["NAV_STRING"]?><?endif?>