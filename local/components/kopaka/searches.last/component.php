<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

require_once($_SERVER["DOCUMENT_ROOT"] . "/local/src/geoCalc.php");

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Geo\GetCalc;

// the number of output items history list
if (isset($arParams["COUNT_RECORDS"])) {
    $arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {
    $arParams["COUNT_RECORDS"] = false;
}

Loader::includeModule("highloadblock");
if (!CModule::IncludeModule("highloadblock"))
    return;

$arResult['SEARCH_LAST'] = [];
$arResult['ID_PORDUCTS'] = [];
$arResult['SEARCH_HISTORY'] = [];

$IBLOCK_ID = 0;

$res = CIBlock::GetList(
    Array(),
    Array(
        'SITE_ID' => SITE_ID,
        'ACTIVE' => 'Y',
        "CODE" => $arParams['CATALOG_IBLOCK_CODE']
    ), true
);
if ($ar_res = $res->Fetch()) {
    $IBLOCK_ID = $ar_res["ID"];
    $arFilter["IBLOCK_ID"] = $IBLOCK_ID;
}

$dbHblock = HL\HighloadBlockTable::getList(
    array(
        "select" => array('*'), //выбираем все поля
        "filter" => array("NAME" => "SearchReference", "TABLE_NAME" => "kopaka_search_history"),
    )
);
if ($hldata = $dbHblock->Fetch()) {

    $ID = $hldata["ID"];
    $hldata = HL\HighloadBlockTable::getById($ID)->fetch();
    $hlentity = HL\HighloadBlockTable::compileEntity($hldata);
    $entity_data_class = $hlentity->getDataClass();
    $entity_table_name = $hldata['TABLE_NAME'];
    $sTableID = 'tbl_' . $entity_table_name;

    $optionsQuery = array(
        "select" => array('*'), //выбираем все поля
        "order" => array("UF_TIMESTAMP" => "DESC") // сортировка по полю UF_TIMESTAMP
    );
    if ($arNavStartParams["nTopCount"] > 0) {
        $optionsQuery["limit"] = $arNavStartParams["nTopCount"];
    }
    if ($USER->GetID() > 0) {
        $optionsQuery["filter"] = array("UF_USER" => $USER->GetID());
    } else {
        $optionsQuery["filter"] = array("UF_USER" => "");
    }

    $rsData = $entity_data_class::getList($optionsQuery);

    while ($arEnum = $rsData->fetch()) {
        $arResult['SEARCH_LAST'][] = $arEnum["UF_PHRASE"];
    }

    if (count($arResult['SEARCH_LAST']) > 0) {
        CModule::IncludeModule('search');
        //$q = $_GET['q'];    //запрос
        $module_id = "iblock"; //ищем в инфоблоках
        $obSearch = new CSearch;

        foreach ($arResult['SEARCH_LAST'] as $phrase) {
            $obSearch->Search(array(
                "QUERY" => $phrase,
                "MODULE_ID" => $module_id,
                "PARAM2" => $IBLOCK_ID //iblock_id
            ));
            if ($obSearch->errorno != 0) {
            } else {
                while ($arSearchItem = $obSearch->GetNext()) {
                    $arResult['ID_PORDUCTS'][$arSearchItem["ITEM_ID"]] = $arSearchItem["ITEM_ID"];
                }
            };
        }

        //Для подсчёта расстояний в круге радиусом 10 километров рядом
        $geoCalc = new GetCalc();

        /* Current Coordinates Guest */
        $page = [];
        $row = [];
        $lat = $_SESSION["latitude"];
        $lon = $_SESSION["longitude"];
        $page['map'] = $lat . "," . $lon;

        $rsElement = CIBlockElement::GetList(array(), array(
                "=ID" => array_keys($arResult['ID_PORDUCTS']),
                "IBLOCK_ID" => $IBLOCK_ID,
            )
        );
        $rsElement->NavStart($arParams["NAV_ON_PAGE"]);
        while ($obElement = $rsElement->GetNextElement()) {
            $fields = $obElement->GetFields();
            $properties = $obElement->GetProperties();

            //Точка перебора из инфоблока каталога техники
            $row['map'] = $properties["LAT"]["VALUE"] . "," . $properties["LNG"]["VALUE"];

            // Будут отсеяны все объекты, расстояние которых больше чем 10 км от данного объекта
            $place = explode(',', $page['map']);
            $target = explode(',', $row['map']);
            $kilometers = number_format($geoCalc->getDistanceBetweenPoints($place[0], $place[1], $target[1], $target[0])["kilometers"], 4);
            $fields["DISTANCE"] = substr($kilometers, 0, 3);
            $fields["DISTANCE"] .= " км.";

            $fields["PROPERTIES"] = $properties;
            $arResult['SEARCH_HISTORY'][] = $fields;
        }
    }
}

$this->IncludeComponentTemplate();
?>