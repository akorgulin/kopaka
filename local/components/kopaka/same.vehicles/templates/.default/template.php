<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<!-- Same vehicles -->
<section class="section vehicle-cards-section">
    <div class="container">
        <h2>Подобная техника рядом</h2>
        <div class="vehicle-cards-carousel owl-carousel">
            <a href="#" class="item vehicle-card">
                <div class="card-image">
                    <div class="badges-container">
                        <span class="badge badge-danger text-uppercase">Занят</span>
                        <span class="badge badge-danger">320 м.</span>
                    </div>
                    <img src="/uploads/vehicles-images/image-1.jpg" alt="">
                </div>
                <div class="card-body">
                    <span class="title">Volvo 3CX ECO</span>
                    <span class="desc">Экскаватор-погрузчик</span>
                    <ul class="card-specs">
                        <li class="item">
                            <span>Эксплуатационная масса:</span>
                            <span class="value">9006 кг</span>
                        </li>
                        <li class="item">
                            <span>Емкость ковша погрузчика:</span>
                            <span class="value">0,93 м³</span>
                        </li>
                        <li class="item">
                            <span>Макс. глубина копания:</span>
                            <span class="value">6,46 м</span>
                        </li>
                        <li class="item">
                            <span>Мощность двигателя:</span>
                            <span class="value">93 л.с.</span>
                        </li>
                    </ul>
                </div>
            </a>
            <a href="#" class="item vehicle-card">
                <div class="card-image">
                    <div class="badges-container">
                        <span class="badge badge-success text-uppercase">Свободен</span>
                        <span class="badge badge-danger">600 м.</span>
                    </div>
                    <img src="/uploads/vehicles-images/image-2.jpg" alt="">
                </div>
                <div class="card-body">
                    <span class="title">Volvo 3CX ECO</span>
                    <span class="desc">Экскаватор-погрузчик</span>
                    <ul class="card-specs">
                        <li class="item">
                            <span>Эксплуатационная масса:</span>
                            <span class="value">9006 кг</span>
                        </li>
                        <li class="item">
                            <span>Емкость ковша погрузчика:</span>
                            <span class="value">0,93 м³</span>
                        </li>
                        <li class="item">
                            <span>Макс. глубина копания:</span>
                            <span class="value">6,46 м</span>
                        </li>
                        <li class="item">
                            <span>Мощность двигателя:</span>
                            <span class="value">93 л.с.</span>
                        </li>
                    </ul>
                </div>
            </a>
            <a href="#" class="item vehicle-card">
                <div class="card-image">
                    <div class="badges-container">
                        <span class="badge badge-success">Свободен</span>
                        <span class="badge badge-danger">820 м.</span>
                    </div>
                    <img src="/uploads/vehicles-images/image-3.jpg" alt="">
                </div>
                <div class="card-body">
                    <span class="title">Volvo 3CX ECO</span>
                    <span class="desc">Экскаватор-погрузчик</span>
                    <ul class="card-specs">
                        <li class="item">
                            <span>Эксплуатационная масса:</span>
                            <span class="value">9006 кг</span>
                        </li>
                        <li class="item">
                            <span>Емкость ковша погрузчика:</span>
                            <span class="value">0,93 м³</span>
                        </li>
                        <li class="item">
                            <span>Макс. глубина копания:</span>
                            <span class="value">6,46 м</span>
                        </li>
                        <li class="item">
                            <span>Мощность двигателя:</span>
                            <span class="value">93 л.с.</span>
                        </li>
                    </ul>
                </div>
            </a>
        </div>
    </div>
</section>
<!-- End of Same vehicles -->
