<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

require_once($_SERVER["DOCUMENT_ROOT"] . "/local/src/geoCalc.php");

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Geo\GetCalc;

// the number of output brands
if (isset($arParams["COUNT_RECORDS"])) {
	$arParams["COUNT_RECORDS"] = intval($arParams["COUNT_RECORDS"]);
} else {	
	$arParams["COUNT_RECORDS"] = false;
}

Loader::includeModule("highloadblock");
if (!CModule::IncludeModule("highloadblock"))
    return;

if( CModule::IncludeModule("iblock") )
{
	if (!$arParams['SORT_FIELD']) $arParams['SORT_FIELD'] = "SORT";
	
	if (!$arParams['SORT_BY']) $arParams['SORT_BY'] = "ASC";
	
	$arParams['AJAX'] = $_REQUEST['AJAX'];
	
	//$arParams['nPageSize'] = (int)$_REQUEST['nPageSize'];
	if($arParams['nPageSize'] == 0) $arParams['nPageSize'] = 30;
	
	$arResult['ORDABC'] = (int)$_REQUEST['abc'];
	
	$arFilter = array(
		'ACTIVE' => "Y",
		'IBLOCK_CODE' => $arParams['CATALOG_IBLOCK_CODE']
	);

	$IBLOCK_ID = 0;

    $res = CIBlock::GetList(
        Array(),
        Array(
            'SITE_ID'=>SITE_ID,
            'ACTIVE'=>'Y',
            "CODE"=>$arParams['CATALOG_IBLOCK_CODE']
        ), true
    );
    if($ar_res = $res->Fetch())
    {
        $IBLOCK_ID = $ar_res["ID"];
        $arFilter["IBLOCK_ID"] = $IBLOCK_ID;
    }


	if($arResult['ORDABC'] > 0) $arNavStartParams = false;
	else {
		
		$arNavStartParams = array(
			"nPageSize" => $arParams['nPageSize'],
			//"iNumPage" => $iNumPage,
			"bShowAll" => false
		);
	
	}
	//  If the specified number of valid entries that do not need a navigation
	if ($arParams["COUNT_RECORDS"] > 0 ) {
		$arNavStartParams = false;
		$arNavStartParams = array(
			"nTopCount" => $arParams["COUNT_RECORDS"],
			"bShowAll" => false
		);
	}

    //Для подсчёта расстояний в круге радиусом 10 километров рядом
    $geoCalc = new GetCalc();

    /* Current Coordinates Guest */
    $page = [];
    $row = [];
    $lat = $_SESSION["latitude"];
    $lon = $_SESSION["longitude"];
    $page['map'] = $lat.",".$lon;



    $rsElement = CIBlockElement::GetList(
        array($arParams['SORT_FIELD'] => $arParams['SORT_BY']),
        $arFilter
    );
    while($obData = $rsElement -> GetNextElement())
    {
        $fields = $obData->getFields();
        $properties = $obData->getProperties();
        $fields["PROPERTIES"] = $properties;

        //Точка перебора из инфоблока каталога техники
        $row['map'] = $properties["LAT"]["VALUE"].",".$properties["LNG"]["VALUE"];
        
        // Будут отсеяны все объекты, расстояние которых больше чем 10 км от данного объекта
		$place = explode(',', $page['map']);
		$target = explode(',', $row['map']);

		$kilometers = number_format($geoCalc->getDistanceBetweenPoints($place[0],$place[1],$target[1],$target[0])["kilometers"],4);
        $fields["DISTANCE"] = substr($kilometers, 0, 3);

        $fields["DISTANCE"] .= " км.";

        if ($fields["DISTANCE"] > 10 ){
            continue;
        }

        $arResult["TECNIKS"][] = $fields;
    }

}
$this->IncludeComponentTemplate();
?>