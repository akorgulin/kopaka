<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["TECNIKS"])>0) { ?>
<!-- Same vehicles -->
<section class="section vehicle-cards-section">
    <div class="container">
        <h2>Подобная техника рядом</h2>
        <div class="vehicle-cards-carousel owl-carousel">
            <?foreach($arResult["TECNIKS"] as $i=>$tecnickRow) { ?>
            <a href="/catalog/technik/<?=$tecnickRow["CODE"]?>/" class="item vehicle-card">
                <div class="card-image">
                    <div class="badges-container">
                        <?if($tecnickRow["PROPERTIES"]["STATUS"]["ENUM_ID"]==51) { ?>
                            <span class="badge badge-danger text-uppercase"><?=$tecnickRow["PROPERTIES"]["STATUS"]["VALUE"]?></span>
                        <?} else { ?>
                            <span class="badge badge-success text-uppercase"><?=$tecnickRow["PROPERTIES"]["STATUS"]["VALUE"]?></span>
                        <? } ?>
                        <span class="badge badge-danger"><?=$tecnickRow["DISTANCE"]?></span>
                    </div>
                    <? $img = CFile::ResizeImageGet($tecnickRow['PREVIEW_PICTURE'], ['width' => 224, 'height' => 140], BX_RESIZE_IMAGE_PROPORTIONAL, false,false);  ?>
                    <img src="<?=$img["src"]?>" width="224" height="140" alt="<?=$tecnickRow['NAME']?>">
                </div>
                <div class="card-body">
                    <span class="title"><?=$tecnickRow["NAME"]?></span>
                    <span class="desc">Экскаватор-погрузчик</span>
                    <ul class="card-specs">
                        <? if($tecnickRow["PROPERTIES"]["MASS"]["VALUE"]) { ?>
                        <li class="item">
                            <span>Эксплуатационная масса:</span>
                            <span class="value"><?=$tecnickRow["PROPERTIES"]["MASS"]["VALUE"]?></span>
                        </li>
                        <? } ?>
                        <? if($tecnickRow["PROPERTIES"]["EMKOST"]["VALUE"]) { ?>
                        <li class="item">
                            <span>Емкость ковша погрузчика:</span>
                            <span class="value"><?=$tecnickRow["PROPERTIES"]["EMKOST"]["VALUE"]?></span>
                        </li>
                        <? } ?>
                        <? if($tecnickRow["PROPERTIES"]["GLUBINA"]["VALUE"]) { ?>
                        <li class="item">
                            <span>Макс. глубина копания:</span>
                            <span class="value"><?=$tecnickRow["PROPERTIES"]["GLUBINA"]["VALUE"]?></span>
                        </li>
                        <? } ?>
                        <? if($tecnickRow["PROPERTIES"]["ENGINE"]["VALUE"]) { ?>
                        <li class="item">
                            <span>Мощность двигателя:</span>
                            <span class="value"><?=$tecnickRow["PROPERTIES"]["ENGINE"]["VALUE"]?></span>
                        </li>
                        <? } ?>
                    </ul>
                </div>
            </a>
            <? } ?>
        </div>
    </div>
</section>
<!-- End of Same vehicles -->
<? } ?>
