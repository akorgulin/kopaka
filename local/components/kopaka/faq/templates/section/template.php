<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<section class="content_wrapper nobackground no_padding_bottom">
    <div class="wrapper center">
        <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400"><?= $APPLICATION->ShowTitle() ?></h1>
    </div>
    <div class="wrapper">
        <div class="white_wrapper" data-aos="fade-up" data-aos-delay="250">
            <div class="spoiler_wrapper">
                <?if(count($arResult['FAQ'])>0) { ?>
                    <? foreach ($arResult['FAQ'] as $itemFaq) { ?>
                        <div class="spoiler">
                            <div class="spoiler_title"><? echo $itemFaq["NAME"]; ?></div>
                            <div class="spoiler_content withouticon">
                                <span class="lnr lnr-bullhorn"></span>
                                <div class="grid x3 clearfix">
                                <? echo $itemFaq["~DETAIL_TEXT"]; ?>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                <? } else {?>
                    Нет вопросов для отображения
                <? } ?>
            </div>
        </div>
    </div>
</section>