<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--FAQ page-->
<section class="content_wrapper nobackground no_padding_bottom">
    <div class="wrapper center">
        <h1 class="page_heading small" data-aos="flip-up" data-aos-delay="400"><?= $APPLICATION->ShowTitle() ?></h1>
        <p class="centered_description" data-aos="fade-up" data-aos-delay="800">Мы всегда рады ответить на вопросы,
            помочь сориентироваться в ассортименте, объяснить все технические моменты заказа на сайте и рассказать, как
            устроены внутренние процессы работы на сайте Fijie!</p>
    </div>
    <div class="container">
        <div class="grid_blocks faq_list clearfix">
            <? foreach ($arResult['IBLOCK_CATEGORY'] as $itemCategory) { ?>
                <div class="item" data-aos="fade-up" data-aos-delay="400">
                    <div class="padding" data-mh="gridblockheight">
                        <? if ($itemCategory["PICTURE"]) { ?>
                            <div class="icon">
                                <img src="<? echo CFile::GetPath($itemCategory["PICTURE"]) ?>">
                            </div>
                        <? }

                        /*SVG Output */
                        $res = CIBlockProperty::GetByID("SVG", false, $itemCategory["CODE"]);
                        if ($ar_res = $res->GetNext()) {
                            ?>
                            <div class="icon"><?
                            echo html_entity_decode($ar_res["DEFAULT_VALUE"]["TEXT"]);
                            ?></div><?
                        }

                        if ($itemCategory["NAME"]) { ?>
                            <div class="title"><a
                                        href="/about/faq/<? echo $itemCategory["CODE"] ?>/"><? echo $itemCategory["NAME"] ?></a>
                            </div>
                        <? } ?>
                        <? if ($itemCategory["DESCRIPTION"]) { ?>
                            <div class="description"><? echo $itemCategory["DESCRIPTION"] ?></div>
                        <? } ?>
                        <? if ($itemCategory["CODE"]) { ?>
                            <a href="/about/faq/<? echo $itemCategory["CODE"] ?>/" class="btn mint">Подробнее</a>
                        <? } ?>
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</section>
