<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<!-- Footer -->
  <footer class="footer">
  <div class="container">
    <div class="contacts-list">
      <div class="item">
        <span>Телефон для связи</span>
          <?$APPLICATION->IncludeFile(
              SITE_TEMPLATE_PATH."/include/phone.php",
              Array(),
              Array("MODE"=>"text")
          );?>
      </div>
      <div class="item">
        <span>Электронная почта</span>
        <?$APPLICATION->IncludeFile(
          SITE_TEMPLATE_PATH."/include/email.php",
          Array(),
          Array("MODE"=>"text")
        );?>
      </div>
    </div>
    <span class="copyright">
        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/include/copyright.php",
            Array(),
            Array("MODE"=>"text")
        );?>
     </span>
    <p><?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/include/copyright_text.php",
            Array(),
            Array("MODE"=>"text")
        );?>
    </p>
  </div>
</footer>
  <!-- End of Footer -->
  
  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='https://www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<? $APPLICATION->ShowViewContent('googleMapinitDetail'); ?>
<? $APPLICATION->ShowViewContent('googleMapinit'); ?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
  
</body>
</html>