function simulateClick () {
    var event = new MouseEvent('click', {
        'view': window,
        'bubbles': true,
        'cancelable': true
    });
    var cb = document.getElementById('login');
    var canceled = !cb.dispatchEvent(event);
}

$(document).ready(function () {
    var $latitude = "";
    var $longitude = "";
    $latitude = $(".search-form .latitude").detach();
    $longitude = $(".search-form .longitude").detach();

    $(document).on("click","label[for='locationCheckbox']",function (e) {
        if($(".search-form .latitude").length==1) {
            $latitude = $(".search-form .latitude").detach();
        } else {
           $(".search-form .selectors").after($latitude);
        }
        if($(".search-form .longitude").length==1) {
            $longitude = $(".search-form .longitude").detach();
        } else {
            $(".search-form .selectors").after($longitude);
        }
    });

    $(document).on("click","div.hidden-tablet a[href='#']",function (e) {
        simulateClick();
        return false;
    });
});