<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$com_path = '/bitrix/components/nbrains/slider/';
$this->addExternalCss($com_path . "front/css/masterslider.css");
$this->addExternalJS($com_path . "front/js/masterslider.js");

$arBg = [];
$arBg[] = '';
$arBg[] = 'style="background-color: #c1b8f0;"';
$arBg[] = 'style="background-color: #8eda84;"';
$arBg[] = 'style="background-color: #d6c261;"';
$num = rand(0, 3);
?>

<!-- Our vehicles section -->
<section class="section our-vechicles-section">
    <div class="our-vechicles-carousel owl-carousel">
        <? foreach ($arResult["ITEMS"] as $arItem) { ?>
            <? if (strlen($arItem["PREVIEW_TEXT"]) > 1) { ?>
                <div class="item">
                    <div class="container">
                        <div class="inner">
                            <h3><?= $arItem["NAME"] ?></h3>
                            <p>
                                <?= trim(strip_tags($arItem["PREVIEW_TEXT"])); ?>
                            </p>
                            <? if ($arItem["PROPERTIES"]["SHOW_BUTTON"]["VALUE"] == "Y") { ?>
                                <div class="button-container">
                                    <? if ($arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]) { ?>
                                        <a class="btn btn-primary btn-sm"
                                           href="<?= $arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"] ?>">
                                            <span>Заказать</span>
                                            <?= html_entity_decode($arItem["PROPERTIES"]["ICO_BUTTON"]["VALUE"]["TEXT"]) ?>
                                        </a>
                                    <? } ?>
                                </div>
                            <? } ?>
                            <div class="counter hidden">
                                <a href="#">2870 единиц</a>
                                <span>в каталоге</span>
                            </div>
                        </div>
                        <? if ($arItem["PREVIEW_PICTURE"]["SRC"]) { ?>
                            <div class="item-image">
                                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
                            </div>
                        <? } ?>
                    </div>
                </div>
            <? } else { ?>
                <div class="item" style="background: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>) no-repeat 0 0; background-size: cover; ">
                    <? if ($arItem["PROPERTIES"]["SHOW_BUTTON"]["VALUE"] == "Y") { ?>
                        <div class="container">
                            <div class="inner">
                                <div class="button-container">
                                    <? if ($arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"]) { ?>
                                        <a class="btn btn-primary btn-sm"
                                           href="<?= $arItem["PROPERTIES"]["URL_BUTTON"]["VALUE"] ?>">
                                            <span>Заказать</span>
                                            <?= html_entity_decode($arItem["PROPERTIES"]["ICO_BUTTON"]["VALUE"]["TEXT"]) ?>
                                        </a>
                                    <? } ?>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                </div>
            <? } ?>
        <? } ?>
    </div>
</section>
<!-- End of Our vehicles section -->