<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- How it works section -->
<section class="section how-it-works-section">
    <div class="container">
        <h2>Как это работает</h2>
        <div class="how-it-works-list">
            <? foreach($arResult["STEPS"] as $counter=>$itemRow) { ?>
                <div class="item">
                    <div class="counter"><?=($counter+1)?></div>
                    <p><?= trim(strip_tags($itemRow["DETAIL_TEXT"])); ?></p>
                    <?if(($counter+1)!=count($arResult["STEPS"])) { ?>
                    <div class="dots-line">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                    <? } ?>
                </div>
            <? } ?>
        </div>
    </div>
</section>
<!-- End of How it works section -->