<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<!-- Types tehnics section -->
<div class="vehicle-type-container">
    <h4>Выберите тип техники</h4>
    <div class="vehicle-type-selection">
        <? foreach($arResult["TYPES_PROPERTY"] as $counter=>$itemRow) {  ?>
            <div class="item">
                <input type="checkbox" name="type_<?=$itemRow["ID"]?>"  value="<?=$itemRow["ID"]?>" id="type<?=$counter+10?>">
                <label for="type<?=$counter+10?>">
                <?if($itemRow["UF_DESCRIPTION"]) { ?>
                    <span class="icon-container">
                        <?=$itemRow["UF_DESCRIPTION"]?>
                    </span>
                    <span class="text">
                        <?= trim(strip_tags($itemRow["UF_NAME"])); ?>
                    </span>
                <? } ?>
                </label>
            </div>
        <? } ?>
    </div>
    <div class="vehicle-type-select form-group-row">
        <div class="form-group select-group">
            <label for="vehiclesType">Выберите тип техники</label>
            <select id="vehiclesType" class="styled-select" name="state" data-select-placeholder="Класс комфорта">
            <? foreach($arResult["TYPES_PROPERTY"] as $counter=>$itemRow) {  ?>
                    <option value="<?=$itemRow["ID"]?>" <? /*data-select2-id="2"*/ ?>><?= trim(strip_tags($itemRow["UF_NAME"])); ?></option>
                <? } ?>
            </select>
        </div>
    </div>
</div>


<!-- End of Work types section -->