<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- Work types section -->
<section class="section work-types-section">
    <div class="container">
        <h2>Выполняем следующие виды работ</h2>
        <div class="work-types-list">
            <? foreach($arResult["TYPES_PROPERTY"] as $counter=>$itemRow) {  ?>
                <?if($itemRow["UF_DESCRIPTION"]) { ?>
                <a <?if($itemRow["UF_LINK"]) { ?>href="<?=$itemRow["UF_LINK"]?>"<?} else {?>href="javascript:void();"<?}?> class="item">
                    <div class="icon-container">
                        <?=$itemRow["UF_DESCRIPTION"]?>
                    </div>
                    <div class="text-container">
                        <p><?= trim(strip_tags($itemRow["UF_NAME"])); ?></p>
                    </div>
                </a>
                <? } ?>
            <? } ?>
        </div>
    </div>
</section>
<!-- End of Work types section -->