<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
//dump($arResult["PROPERTY_LIST"]);
//foreach ($arResult["PROPERTY_LIST"] as $propertyID) {
//}
if (!empty($arResult["ERRORS"])):?>
    <?
    ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
    <? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>

<form class="form form-add-vehicle" name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post"
      enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <h2><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST") ?></h2>
    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
        <input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>"/>
    <? endif ?>
    <div class="data-table">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
        <div class="form-group-row">

            <? foreach ($arResult["PROPERTY_LIST"] as $propertyID):

            if (intval($propertyID) > 0) {
                if (
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                elseif (
                    (
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                        ||
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                    )
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
            } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
            } else {
                $inputNum = 1;
            }

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                $INPUT_TYPE = "USER_TYPE";
            else
                $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

            switch ($INPUT_TYPE):
            case "USER_TYPE":
            for ($i = 0;
            $i < $inputNum;
            $i++) {
            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
            } elseif ($i == 0) {
                $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                $description = "";
            } else {
                $value = "";
                $description = "";
            }
            ?>
            <?
            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
        </div>
        <div class="form-group-row <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>type-2<? } ?>">
        <div class="styled-checkbox type-1">
        <div class="checkbox-group">
        <? } else { ?>
            <div class="form-group select-group tablet-half">
        <? } ?><?
    echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
        array(
            $arResult["PROPERTY_LIST_FULL"][$propertyID],
            array(
                "VALUE" => $value,
                "DESCRIPTION" => $description,
            ),
            array(
                "VALUE" => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
                "DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
                "FORM_NAME" => "iblock_add",
            ),
        ));
    ?>
        <?
        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
            <label for="<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 70 70" style="enable-background:new 0 0 70 70;"
                     xml:space="preserve">
                  <path d="M26.474,70c-2.176,0-4.234-1.018-5.557-2.764L3.049,43.639C0.725,40.57,1.33,36.2,4.399,33.875
                    c3.074-2.326,7.441-1.717,9.766,1.35l11.752,15.518L55.474,3.285c2.035-3.265,6.332-4.264,9.604-2.232
                    c3.268,2.034,4.266,6.334,2.23,9.602l-34.916,56.06c-1.213,1.949-3.307,3.175-5.6,3.279C26.685,69.998,26.58,70,26.474,70z"/>
                </svg>
            </label>
        <? } ?>

        <script type="text/javascript">
            <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>');
            <? } else { ?>

            <? if (intval($propertyID) > 0) {  ?>
            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>');
            <? } ?>
            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").addClass("styled-select");

            <? } ?>
        </script>
        <?
        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
            </div><span><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></span>
            </div>
            </div>
        <? } else { ?>
        </div>
    <? } ?>
    <?
    }
    break;
    case "TAGS":
        $APPLICATION->IncludeComponent(
            "bitrix:search.tags.input",
            "",
            array(
                "VALUE" => $arResult["ELEMENT"][$propertyID],
                "NAME" => "PROPERTY[" . $propertyID . "][0]",
                "TEXT" => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
            ), null, array("HIDE_ICONS" => "Y")
        );
        break;
    case "HTML":
        $LHE = new CHTMLEditor;
        $LHE->Show(array(
            'name' => "PROPERTY[" . $propertyID . "][0]",
            'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
            'inputName' => "PROPERTY[" . $propertyID . "][0]",
            'content' => $arResult["ELEMENT"][$propertyID],
            'width' => '100%',
            'minBodyWidth' => 350,
            'normalBodyWidth' => 555,
            'height' => '200',
            'bAllowPhp' => false,
            'limitPhpAccess' => false,
            'autoResize' => true,
            'autoResizeOffset' => 40,
            'useFileDialogs' => false,
            'saveOnBlur' => true,
            'showTaskbars' => false,
            'showNodeNavi' => false,
            'askBeforeUnloadPage' => true,
            'bbCode' => false,
            'siteId' => SITE_ID,
            'controlsMap' => array(
                array('id' => 'Bold', 'compact' => true, 'sort' => 80),
                array('id' => 'Italic', 'compact' => true, 'sort' => 90),
                array('id' => 'Underline', 'compact' => true, 'sort' => 100),
                array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
                array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
                array('id' => 'Color', 'compact' => true, 'sort' => 130),
                array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
                array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
                array('separator' => true, 'compact' => false, 'sort' => 145),
                array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
                array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
                array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
                array('separator' => true, 'compact' => false, 'sort' => 200),
                array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
                array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
                array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
                array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
                array('separator' => true, 'compact' => false, 'sort' => 290),
                array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
                array('id' => 'More', 'compact' => true, 'sort' => 400)
            ),
        ));
        break;
    case "T":
    for ($i = 0;
    $i < $inputNum;
    $i++) {

    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
    } elseif ($i == 0) {
        $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
    } else {
        $value = "";
    }
    ?>
        <textarea cols="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                  rows="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] ?>"
                  name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
    <?
    }
    break;

    case "S":
    case "N":
    for ($i = 0;
    $i < $inputNum;
    $i++) {
    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
    } elseif ($i == 0) {
        $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

    } else {
        $value = "";
    }

    $class = "";
    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="LAT") {
        $value = $_SESSION["latitude"];
        $class = "hidden-xs";
    }
    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="LNG") {
        $value = $_SESSION["longitude"];
        $class = "hidden-xs";
    }
    ?>
        <div class="form-group <?=$class?>">
            <input type="text" class="form-control" id="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                   name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                   size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>"
                   value="<?= $value ?>"
                   <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?>/><?
            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime1"):?><?
                $APPLICATION->IncludeComponent(
                    'bitrix:main.calendar',
                    '',
                    array(
                        'FORM_NAME' => 'iblock_add',
                        'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
                        'INPUT_VALUE' => $value,
                    ),
                    null,
                    array('HIDE_ICONS' => 'Y')
                );
                ?>
                <small><?= GetMessage("IBLOCK_FORM_DATE_FORMAT") ?><?= FORMAT_DATETIME ?></small><?
            endif;

            ?>
            <? if (intval($propertyID) > 0) { ?>
                <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></label>
            <? } else { ?>
                <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?></label>
            <? } ?>

            <script type="text/javascript">
                <? if (intval($propertyID) > 0): ?>
                $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>');
                <? endif ?>

                $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").addClass("styled-select");
            </script>
        </div>

    <?
    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "ADRESSWORK") { ?>
        <div class="map-container">
            <div class="map" id="map" style="position: relative; overflow: hidden;">
                <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                    <div class="gm-style"
                         style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;">
                        <div tabindex="0"
                             style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;">
                            <div style="z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);">
                                <div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;">
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                        <div style="position: absolute; z-index: 987; transform: matrix(1.41406, 0, 0, 1.41406, -23, -195);">
                                            <div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                            <div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                            <div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                            <div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                            <div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                            <div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px;">
                                                <div style="width: 256px; height: 256px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div>
                                <div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div>
                                <div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                                        <div style="position: absolute; z-index: 987; transform: matrix(1.41406, 0, 0, 1.41406, -23, -195);">
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;"></div>
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 0px;"></div>
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: -256px;"></div>
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -256px;"></div>
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 256px;"></div>
                                            <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 256px;"></div>
                                        </div>
                                    </div>
                                    <div style="width: 30px; height: 34px; overflow: hidden; position: absolute; left: -15px; top: 112px; z-index: 146;">
                                        <img alt="" src="img/marker-pin.png" draggable="false"
                                             style="position: absolute; left: 0px; top: 0px; user-select: none; width: 30px; height: 34px; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                    </div>
                                    <div style="width: 30px; height: 34px; overflow: hidden; position: absolute; left: -15px; top: -181px; z-index: -147;">
                                        <img alt="" src="img/marker-pin.png" draggable="false"
                                             style="position: absolute; left: 0px; top: 0px; user-select: none; width: 30px; height: 34px; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                    </div>
                                </div>
                                <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                    <div style="position: absolute; z-index: 987; transform: matrix(1.41406, 0, 0, 1.41406, -23, -195);">
                                        <div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4952!3i2561!4i256!2m3!1e0!2sm!3i463172293!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=130930"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4951!3i2561!4i256!2m3!1e0!2sm!3i463172293!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=123536"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4951!3i2560!4i256!2m3!1e0!2sm!3i463172293!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=33226"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4952!3i2560!4i256!2m3!1e0!2sm!3i463172293!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=40620"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4952!3i2562!4i256!2m3!1e0!2sm!3i463172280!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=118920"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;">
                                            <img draggable="false" alt="" role="presentation"
                                                 src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i13!2i4951!3i2562!4i256!2m3!1e0!2sm!3i463172280!3m14!2sru-RU!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmd8cC5jOiNmZjIxMjEyMSxzLmU6bC5pfHAudjpvZmYscy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy5lOmwudC5zfHAuYzojZmYyMTIxMjEscy50OjF8cy5lOmd8cC5jOiNmZjc1NzU3NSxzLnQ6MTd8cy5lOmwudC5mfHAuYzojZmY5ZTllOWUscy50OjE5fHMuZTpsLnQuZnxwLmM6I2ZmYmRiZGJkLHMudDoxOHxwLnY6b2ZmLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmNzU3NTc1LHMudDo0MHxzLmU6Z3xwLmM6I2ZmMTgxODE4LHMudDo0MHxzLmU6bC50LmZ8cC5jOiNmZjYxNjE2MSxzLnQ6NDB8cy5lOmwudC5zfHAuYzojZmYxYjFiMWIscy50OjN8cy5lOmcuZnxwLmM6I2ZmMmMyYzJjLHMudDozfHMuZTpsLnQuZnxwLmM6I2ZmOGE4YThhLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMzczNzM3LHMudDo0OXxzLmU6Z3xwLmM6I2ZmM2MzYzNjLHMudDo3ODV8cy5lOmd8cC5jOiNmZjRlNGU0ZSxzLnQ6NTF8cy5lOmwudC5mfHAuYzojZmY2MTYxNjEscy50OjR8cy5lOmwudC5mfHAuYzojZmY3NTc1NzUscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMCxzLnQ6NnxzLmU6Zy5mfHAuYzojZmYyYjJiMmIscy50OjZ8cy5lOmwudC5mfHAuYzojZmYzZDNkM2Q!4e0&amp;key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&amp;token=111526"
                                                 style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gm-style-pbc"
                                 style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0; transition-duration: 0.8s;">
                                <p class="gm-style-pbt">Чтобы изменить масштаб, прокручивайте карту, удерживая клавишу
                                    Ctrl.</p></div>
                            <div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;">
                                <div style="z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);">
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div>
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div>
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;">
                                        <div style="background-image: url(&quot;img/marker-cluster.png&quot;); background-position: 0px 0px; height: 48px; line-height: 48px; width: 48px; text-align: center; cursor: pointer; top: 35.6542px; left: -24.2669px; color: white; position: absolute; font-size: 18px; font-family: Arial, sans-serif; font-weight: bold;">
                                            7
                                        </div>
                                        <div style="background-image: url(&quot;img/marker-cluster.png&quot;); background-position: 0px 0px; height: 48px; line-height: 48px; width: 48px; text-align: center; cursor: pointer; top: -28.7318px; left: -23.9457px; color: white; position: absolute; font-size: 18px; font-family: Arial, sans-serif; font-weight: bold;">
                                            3
                                        </div>
                                        <div></div>
                                        <div></div>
                                        <div title=""
                                             style="width: 30px; height: 34px; overflow: hidden; position: absolute; opacity: 0; cursor: pointer; touch-action: none; left: -15px; top: 112px; z-index: 146;">
                                            <img alt="" src="img/marker-pin.png" draggable="false"
                                                 style="position: absolute; left: 0px; top: 0px; user-select: none; width: 30px; height: 34px; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                        <div title=""
                                             style="width: 30px; height: 34px; overflow: hidden; position: absolute; opacity: 0; cursor: pointer; touch-action: none; left: -15px; top: -181px; z-index: -147;">
                                            <img alt="" src="img/marker-pin.png" draggable="false"
                                                 style="position: absolute; left: 0px; top: 0px; user-select: none; width: 30px; height: 34px; border: 0px; padding: 0px; margin: 0px; max-width: none;">
                                        </div>
                                    </div>
                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                        <iframe aria-hidden="true" frameborder="0" src="about:blank"
                                style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe>
                        <div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;">
                            <a target="_blank" rel="noopener"
                               href="https://maps.google.com/maps?ll=55.738548,37.619946&amp;z=14&amp;hl=ru-RU&amp;gl=US&amp;mapclient=apiv3"
                               title="Открыть эту область в Google Картах (в новом окне)"
                               style="position: static; overflow: visible; float: none; display: inline;">
                                <div style="width: 66px; height: 26px; cursor: pointer;"><img alt=""
                                                                                              src="https://maps.gstatic.com/mapfiles/api-3/images/google_white5.png"
                                                                                              draggable="false"
                                                                                              style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;">
                                </div>
                            </a></div>
                        <div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-sizing: border-box; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 300px; height: 180px; position: absolute; left: 136px; top: 110px;">
                            <div style="padding: 0px 0px 10px; font-size: 16px;">Картографические данные</div>
                            <div style="font-size: 13px;">Картографические данные © 2019 Google</div>
                            <button draggable="false" title="Закрыть" aria-label="Закрыть" type="button"
                                    class="gm-ui-hover-effect"
                                    style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: absolute; cursor: pointer; user-select: none; top: 0px; right: 0px; width: 37px; height: 37px;">
                                <img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22%23000000%22%3E%0A%20%20%20%20%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22%2F%3E%0A%20%20%20%20%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                     style="pointer-events: none; display: block; width: 13px; height: 13px; margin: 12px;">
                            </button>
                        </div>
                        <div class="gmnoprint"
                             style="z-index: 1000001; position: absolute; right: 130px; bottom: 0px; width: 215px;">
                            <div draggable="false" class="gm-style-cc"
                                 style="user-select: none; height: 14px; line-height: 14px;">
                                <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                    <div style="width: 1px;"></div>
                                    <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                </div>
                                <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;">
                                    <a style="text-decoration: none; cursor: pointer; display: none;">Картографические
                                        данные</a><span>Картографические данные © 2019 Google</span></div>
                            </div>
                        </div>
                        <div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;">
                            <div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">
                                Картографические данные © 2019 Google
                            </div>
                        </div>
                        <div class="gmnoprint gm-style-cc" draggable="false"
                             style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;">
                            <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                <div style="width: 1px;"></div>
                                <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                            </div>
                            <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;">
                                <a href="https://www.google.com/intl/ru-RU_US/help/terms_maps.html" target="_blank"
                                   rel="noopener"
                                   style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Условия
                                    использования</a></div>
                        </div>
                        <button draggable="false" title="Включить полноэкранный режим"
                                aria-label="Включить полноэкранный режим" type="button"
                                class="gm-control-active gm-fullscreen-control"
                                style="background: none rgb(255, 255, 255); border: 0px; margin: 10px; padding: 0px; position: absolute; cursor: pointer; user-select: none; border-radius: 2px; height: 40px; width: 40px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; overflow: hidden; display: none; top: 0px; right: 0px;">
                            <img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%20018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                 style="height: 18px; width: 18px;"><img
                                    src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                    style="height: 18px; width: 18px;"><img
                                    src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C0v2v4h2V2h4V0H2H0z%20M16%2C0h-4v2h4v4h2V2V0H16z%20M16%2C16h-4v2h4h2v-2v-4h-2V16z%20M2%2C12H0v4v2h2h4v-2H2V12z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                    style="height: 18px; width: 18px;"></button>
                        <div draggable="false" class="gm-style-cc"
                             style="user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;">
                            <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                <div style="width: 1px;"></div>
                                <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                            </div>
                            <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;">
                                <a target="_blank" rel="noopener" title="Сообщить об ошибке на карте или снимке"
                                   href="https://www.google.com/maps/@55.7385479,37.6199462,14z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3"
                                   style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Сообщить
                                    об ошибке на карте</a></div>
                        </div>
                        <div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false"
                             controlwidth="40" controlheight="81"
                             style="margin: 10px; user-select: none; position: absolute; bottom: 95px; right: 40px;">
                            <div class="gmnoprint" controlwidth="40" controlheight="81"
                                 style="position: absolute; left: 0px; top: 0px;">
                                <div draggable="false"
                                     style="user-select: none; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 40px; height: 81px;">
                                    <button draggable="false" title="Увеличить" aria-label="Увеличить" type="button"
                                            class="gm-control-active"
                                            style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; overflow: hidden; width: 40px; height: 40px; top: 0px; left: 0px;">
                                        <img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23666%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                             style="height: 18px; width: 18px;"><img
                                                src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23333%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                                style="height: 18px; width: 18px;"><img
                                                src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpolygon%20fill%3D%22%23111%22%20points%3D%2218%2C7%2011%2C7%2011%2C0%207%2C0%207%2C7%200%2C7%200%2C11%207%2C11%207%2C18%2011%2C18%2011%2C11%2018%2C11%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                                style="height: 18px; width: 18px;"></button>
                                    <div style="position: relative; overflow: hidden; width: 30px; height: 1px; margin: 0px 5px; background-color: rgb(230, 230, 230); top: 0px;"></div>
                                    <button draggable="false" title="Уменьшить" aria-label="Уменьшить" type="button"
                                            class="gm-control-active"
                                            style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; overflow: hidden; width: 40px; height: 40px; top: 0px; left: 0px;">
                                        <img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                             style="height: 18px; width: 18px;"><img
                                                src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                                style="height: 18px; width: 18px;"><img
                                                src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218%22%20height%3D%2218%22%20viewBox%3D%220%200%2018%2018%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C7h18v4H0V7z%22%2F%3E%0A%3C%2Fsvg%3E%0A"
                                                style="height: 18px; width: 18px;"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="application/javascript">
            function initMap() {

                if (document.getElementById('map') === null) return false;

                var clusterStyles = [{
                    textColor: 'white',
                    url: '<?=SITE_TEMPLATE_PATH?>/img/marker-cluster.png',
                    height: 48,
                    width: 48,
                    textSize: 18
                }];

                var locations = [
                    {lat: 55.7311500, lng: 37.6199100},
                    {lat: 55.7322479, lng: 37.6199252},
                    {lat: 55.7333477, lng: 37.6199362},
                    {lat: 55.7344478, lng: 37.6199472},
                    {lat: 55.7355480, lng: 37.6199582},
                    {lat: 55.7366481, lng: 37.6199662},
                    {lat: 55.7377482, lng: 37.6199762},
                    {lat: 55.7388483, lng: 37.6199862},
                    {lat: 55.7399484, lng: 37.6199962},
                    {lat: 55.7185479, lng: 37.6199461},
                    {lat: 55.7285479, lng: 37.6199463},
                    {lat: 55.7385479, lng: 37.6199465},
                    {lat: 55.7485479, lng: 37.6199466},
                    {lat: 55.7585479, lng: 37.6199467},
                    {lat: 55.7685479, lng: 37.6199468},
                    {lat: 55.7785479, lng: 37.6199470},
                    {lat: 55.7885474, lng: 37.6199465},
                    {lat: 55.796756, lng: 37.6062273},
                    {lat: 55.756757, lng: 37.6062273},
                    {lat: 55.756758, lng: 37.6062273},
                    {lat: 55.756760, lng: 37.6062273},
                    {lat: 55.756761, lng: 37.6062273},
                    {lat: 55.756766, lng: 37.6062273},
                    {lat: 55.756758, lng: 37.6062270},
                    {lat: 55.756758, lng: 37.6062260},
                    {lat: 55.756758, lng: 37.6062265},
                ];
                var styledMapType = new google.maps.StyledMapType(
                    [
                        {
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#212121"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.country",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#9e9e9e"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.locality",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#bdbdbd"
                                }
                            ]
                        },
                        {
                            "featureType": "administrative.province",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#181818"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "color": "#1b1b1b"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#2c2c2c"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#8a8a8a"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#373737"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#3c3c3c"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway.controlled_access",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#4e4e4e"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#616161"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#757575"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#000000"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#2b2b2b"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#3d3d3d"
                                }
                            ]
                        }
                    ],
                    {name: 'Styled Map'});
                var centerPos = {lat: 55.7385479, lng: 37.6199462};

                var map = new google.maps.Map(
                    document.getElementById('map'), {
                        zoom: 13.5,
                        center: centerPos,
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        rotateControl: false,
                        fullscreenControl: false,
                    });

                var markers = locations.map(function (location, i) {
                    return new google.maps.Marker({
                        position: location,
                        icon: '<?=SITE_TEMPLATE_PATH?>/img/marker-pin.png',
                    });
                });

                var markerCluster = new MarkerClusterer(map, markers, {
                    styles: clusterStyles
                });

                map.mapTypes.set('styled_map', styledMapType);
                map.setMapTypeId('styled_map');
            }
        </script>

        <script async defer
                src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&callback=initMap">
        </script>

    <?
    } ?>
    <?
    }
    break;

    case "F":
    for ($i = 0;
    $i < $inputNum;
    $i++) {
    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
    ?>
    <input type="hidden"
           name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
           value="<?= $value ?>"/>
    <input type="file"
           size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
           name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>
        <?

        if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
        ?>
    <input type="checkbox"
           name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
           id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y"
           <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> /><label
            for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
        <?

        if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
        ?>
    <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"
         height="<?= $arResult["ELEMENT_FILES"][$value]["HEIGHT"] ?>"
         width="<?= $arResult["ELEMENT_FILES"][$value]["WIDTH"] ?>" border="0"/>
        <?
        } else {
        ?>
        <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
        <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?> b
        [
        <a href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
    <?
    }
    }
    }

    break;
    case "L":

    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
        $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
    else
        $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

    switch ($type):
    case "checkbox":
    case "radio":
    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
    $checked = false;
    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
        if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
            foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                if ($arElEnum["VALUE"] == $key) {
                    $checked = true;
                    break;
                }
            }
        }
    } else {
        if ($arEnum["DEF"] == "Y") $checked = true;
    }

    ?>
    <input type="<?= $type ?>"
           name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
           value="<?= $key ?>"
           id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?>
           <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> />
        <label for="property_<?= $key ?>"><?= $arEnum["VALUE"] ?></label>
        <?
        }
        break;

        case "dropdown":
        case "multiselect":
        ?>
        <div class="form-group select-group tablet-half">
            <select id="PROPERTY[<?= $propertyID ?>]"
                    data-select-placeholder="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>"
                    class="styled-select"
                    name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
                <option value=""><?
                    echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?></option>
                <?
                if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                else $sKey = "ELEMENT";

                foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                    $checked = false;
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                            if ($key == $arElEnum["VALUE"]) {
                                $checked = true;
                                break;
                            }
                        }
                    } else {
                        if ($arEnum["DEF"] == "Y") $checked = true;
                    }
                    ?>
                    <option value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                    <?
                }
                ?>
            </select>
        </div>
        <?
        break;

        endswitch;
        break;
        endswitch;
        endforeach; ?>
    </div>
<? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
    <div class="form-group-row">
        <?= GetMessage("IBLOCK_FORM_CAPTCHA_TITLE") ?>
        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
        <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
             height="40" alt="CAPTCHA"/>
    </div>
    <div class="form-group-row">
        <?= GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT") ?><span class="starrequired">*</span>:
        <input type="text" name="captcha_word" maxlength="50" value="">
    </div>
<? endif ?>

<? endif ?>
    <div class="button-container">
        <button class="btn btn-primary add_request">
            <span><?= GetMessage("IBLOCK_FORM_SUBMIT") ?></span>
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px" width="474.8px" height="474.801px" viewBox="0 0 474.8 474.801"
                 style="enable-background:new 0 0 474.8 474.801;" xml:space="preserve">
              <path d="M396.283,257.097c-1.14-0.575-2.282-0.862-3.433-0.862c-2.478,0-4.661,0.951-6.563,2.857l-18.274,18.271
                c-1.708,1.715-2.566,3.806-2.566,6.283v72.513c0,12.565-4.463,23.314-13.415,32.264c-8.945,8.945-19.701,13.418-32.264,13.418
                H82.226c-12.564,0-23.319-4.473-32.264-13.418c-8.947-8.949-13.418-19.698-13.418-32.264V118.622
                c0-12.562,4.471-23.316,13.418-32.264c8.945-8.946,19.7-13.418,32.264-13.418H319.77c4.188,0,8.47,0.571,12.847,1.714
                c1.143,0.378,1.999,0.571,2.563,0.571c2.478,0,4.668-0.949,6.57-2.852l13.99-13.99c2.282-2.281,3.142-5.043,2.566-8.276
                c-0.571-3.046-2.286-5.236-5.141-6.567c-10.272-4.752-21.412-7.139-33.403-7.139H82.226c-22.65,0-42.018,8.042-58.102,24.126
                C8.042,76.613,0,95.978,0,118.629v237.543c0,22.647,8.042,42.014,24.125,58.098c16.084,16.088,35.452,24.13,58.102,24.13h237.541
                c22.647,0,42.017-8.042,58.101-24.13c16.085-16.084,24.134-35.45,24.134-58.098v-90.797
                C402.001,261.381,400.088,258.623,396.283,257.097z"></path>
                <path d="M467.95,93.216l-31.409-31.409c-4.568-4.567-9.996-6.851-16.279-6.851c-6.275,0-11.707,2.284-16.271,6.851
                L219.265,246.532l-75.084-75.089c-4.569-4.57-9.995-6.851-16.274-6.851c-6.28,0-11.704,2.281-16.274,6.851l-31.405,31.405
                c-4.568,4.568-6.854,9.994-6.854,16.277c0,6.28,2.286,11.704,6.854,16.274l122.767,122.767c4.569,4.571,9.995,6.851,16.274,6.851
                c6.279,0,11.704-2.279,16.274-6.851l232.404-232.403c4.565-4.567,6.854-9.994,6.854-16.274S472.518,97.783,467.95,93.216z"></path>
            </svg>
        </button>
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on("click", "button.add_request", function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $("input[name='iblock_submit']").click();
                    return false;
                });
            });
        </script>

        <input type="submit" name="iblock_submit" style="display:none;"
               value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
        <? if (strlen($arParams["LIST_URL"]) > 0): ?>
            <input style="display:none;" type="submit" name="iblock_apply"
                   value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
            <input style="display:none;" type="button" name="iblock_cancel"
                   value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                   onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';">
        <? endif ?>
    </div>

    </div>
</form>