<?
$MESS["IBLOCK_FORM_SUBMIT"] = "Сохранить";
$MESS["IBLOCK_FORM_APPLY"] = "Применить";
$MESS["IBLOCK_FORM_RESET"] = "Сбросить";
$MESS["IBLOCK_FORM_CANCEL"] = "Отмена";
$MESS["IBLOCK_FORM_BACK"] = "Назад к списку";
$MESS["IBLOCK_FORM_DATE_FORMAT"] = "Формат: ";
$MESS["IBLOCK_FORM_FILE_NAME"] = "Файл";
$MESS["IBLOCK_FORM_FILE_SIZE"] = "Размер";
$MESS["IBLOCK_FORM_FILE_DOWNLOAD"] = "скачать";
$MESS["IBLOCK_FORM_FILE_DELETE"] = "удалить файл";
$MESS["IBLOCK_FORM_CAPTCHA_TITLE"] = "Защита от автоматического заполнения";
$MESS["IBLOCK_FORM_CAPTCHA_PROMPT"] = "Введите слово с картинки";
$MESS["CT_BIEAF_PROPERTY_VALUE_NA"] = "(не установлено)";
$MESS["CT_BIEAF_PROPERTY_VALUE_REQUEST"] = "Добавить технику";
$MESS["CT_BIEAF_PROPERTY_VALUE_REQUEST_UP"] = "Редактировать технику";
$MESS["CT_BIEAF_PROPERTY_VALUE_REQUEST_PHONE"] = "Привязать телефон водителя/оператора";
$MESS["CT_BIEAF_PROPERTY_VALUE_REQUEST_LINK"] = "ПРИВЯЗАТЬ";
$MESS["TEXT_SUBMIT"] = "Добавить технику";
?>