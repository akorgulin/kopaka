<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

use \Bitrix\Conversion\Internals\MobileDetect;
$detect = new MobileDetect;

if (!empty($arResult["ERRORS"])):?>
    <?
    ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
    <? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>


<form class="form form-add-vehicle" name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post"
      enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
	<?if($arParams["ID"]) { ?>
    	<h2><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST_UP") ?></h2>
	 <?} else { ?>
    	<h2><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST") ?></h2>
	<?} ?>
    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
        <input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>"/>
    <? endif ?>

    <? foreach ($arResult["PROPERTY_LIST"] as $j=>$propertyID) {
        $type = $arResult["PROPERTY_LIST_FULL"][$propertyID];
        $inputNum = 1;
        for ($i = 0;
         $i < $inputNum;
         $i++) {
            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
            } elseif ($i == 0) {
                $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                $description = "";
            } else {
                $value = "";
                $description = "";
            }

        }


        if($j>0) break; ?>
        <!-- Types tehnics section -->
        <div class="vehicle-type-container">
            <h4>Выберите тип техники</h4>
            <div class="vehicle-type-selection">
                <? $resultAr = array_slice($type["ENUM"],0,10);  ?>
                <? foreach($resultAr as $counter=>$itemRow) { ?>
                    <div class="item">
                        <input type="checkbox" name="PROPERTY[<?=$type["ID"] ?>][0]" <?if($value==$itemRow["ID"] || $itemRow["UF_XML_ID"]==$arResult["ELEMENT_PROPERTIES"][49][0]["VALUE"]){ echo "checked"; }?> value="<?=$itemRow["ID"]?>" id="type<?=$counter+10?>">
                        <label for="type<?=$counter+10?>">
                            <?if($itemRow["UF_DESCRIPTION"]) { ?>
                                <span class="icon-container">
                            <?=$itemRow["UF_DESCRIPTION"]?>
                        </span>
                                <span class="text">
                            <?= trim(strip_tags($itemRow["UF_NAME"])); ?>
                        </span>
                            <? } ?>
                        </label>
                    </div>
                <? } ?>
            </div>
            <?if($detect->isMobile()) {?>
            <div class="vehicle-type-select form-group-row">
                <div class="form-group select-group">
                    <label for="vehiclesType">Выберите тип техники</label>
                    <select id="vehiclesType" class="styled-select" name="PROPERTY[49][0]" data-select-placeholder="Тип техники">
                        <? foreach($resultAr as $counter=>$itemRow) {  ?>
                            <option value="<?=$itemRow["ID"]?>" <? /*data-select2-id="2"*/ ?>><?= trim(strip_tags($itemRow["UF_NAME"])); ?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
            <?php
            }?>
        </div>
        <!-- End of Work types section -->
    <? } ?>

    <div class="characteristics-container">
        <h4>Характеристики техники</h4>
        <div class="form-block">
            <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
            <? foreach ($arResult["PROPERTY_LIST"] as $i=>$propertyID):
            if($i==0) continue;

            if (intval($propertyID) > 0) {
                if (
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                elseif (
                    (
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                        ||
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                    )
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
            } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
            } else {
                $inputNum = 1;
            }

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                $INPUT_TYPE = "USER_TYPE";
            else
                $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];



            switch ($INPUT_TYPE):
            case "USER_TYPE":
            for ($i = 0;
            $i < $inputNum;
            $i++) {
            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
            } elseif ($i == 0) {
                $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                $description = "";
            } else {
                $value = "";
                $description = "";
            }

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
        <div class="form-group-row <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>type-2<? } ?>">
            <div class="styled-checkbox type-1">
                <div class="checkbox-group">
                    <? } else { ?>
                    <div class="form-group select-group tablet-half">
                        <? } ?><?
                        echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
                            array(
                                $arResult["PROPERTY_LIST_FULL"][$propertyID],
                                array(
                                    "VALUE" => $value,
                                    "DESCRIPTION" => $description,
                                ),
                                array(
                                    "VALUE" => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
                                    "DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
                                    "FORM_NAME" => "iblock_add",
                                ),
                            ));

                        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                            <label for="<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 70 70" style="enable-background:new 0 0 70 70;"
                                     xml:space="preserve">
                                  <path d="M26.474,70c-2.176,0-4.234-1.018-5.557-2.764L3.049,43.639C0.725,40.57,1.33,36.2,4.399,33.875
                                    c3.074-2.326,7.441-1.717,9.766,1.35l11.752,15.518L55.474,3.285c2.035-3.265,6.332-4.264,9.604-2.232
                                    c3.268,2.034,4.266,6.334,2.23,9.602l-34.916,56.06c-1.213,1.949-3.307,3.175-5.6,3.279C26.685,69.998,26.58,70,26.474,70z"/>
                                </svg>
                            </label>
                        <? } ?>

                        <script type="text/javascript">
                            <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>');
                            <? } else { ?>

                            <? if (intval($propertyID) > 0) {  ?>
                            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>');
                            <? } ?>
                            $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").addClass("styled-select");

                            <? } ?>
                        </script>
                        <?
                        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                    </div>
                    <span><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></span>
                </div>
            </div>
            <? } else { ?>
        </div>
        <? }
        }
        break;
        case "TAGS":
            $APPLICATION->IncludeComponent(
                "bitrix:search.tags.input",
                "",
                array(
                    "VALUE" => $arResult["ELEMENT"][$propertyID],
                    "NAME" => "PROPERTY[" . $propertyID . "][0]",
                    "TEXT" => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
                ), null, array("HIDE_ICONS" => "Y")
            );
            break;
        case "HTML":
            $LHE = new CHTMLEditor;
            $LHE->Show(array(
                'name' => "PROPERTY[" . $propertyID . "][0]",
                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
                'inputName' => "PROPERTY[" . $propertyID . "][0]",
                'content' => $arResult["ELEMENT"][$propertyID],
                'width' => '100%',
                'minBodyWidth' => 350,
                'normalBodyWidth' => 555,
                'height' => '200',
                'bAllowPhp' => false,
                'limitPhpAccess' => false,
                'autoResize' => true,
                'autoResizeOffset' => 40,
                'useFileDialogs' => false,
                'saveOnBlur' => true,
                'showTaskbars' => false,
                'showNodeNavi' => false,
                'askBeforeUnloadPage' => true,
                'bbCode' => false,
                'siteId' => SITE_ID,
                'controlsMap' => array(
                    array('id' => 'Bold', 'compact' => true, 'sort' => 80),
                    array('id' => 'Italic', 'compact' => true, 'sort' => 90),
                    array('id' => 'Underline', 'compact' => true, 'sort' => 100),
                    array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
                    array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
                    array('id' => 'Color', 'compact' => true, 'sort' => 130),
                    array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
                    array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
                    array('separator' => true, 'compact' => false, 'sort' => 145),
                    array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
                    array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
                    array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
                    array('separator' => true, 'compact' => false, 'sort' => 200),
                    array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
                    array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
                    array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
                    array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
                    array('separator' => true, 'compact' => false, 'sort' => 290),
                    array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
                    array('id' => 'More', 'compact' => true, 'sort' => 400)
                ),
            ));
            break;
        case "T":
            for ($i = 0; $i < $inputNum; $i++) {

                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                } elseif ($i == 0) {
                    $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                } else {
                    $value = "";
                }
                ?>
                <textarea cols="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                          rows="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] ?>"
                          name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
                <?
            }
            break;

        case "S":
        case "N":
            if($propertyID==76) break;
            for ($i = 0; $i < $inputNum; $i++) {
                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                } elseif ($i == 0) {
                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                } else {
                    $value = "";
                }
                $class = "";
                if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="LAT") {
                    $value = $_SESSION["latitude"];
                    $class = "hidden-xs";
                }
                if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="LNG") {
                    $value = $_SESSION["longitude"];
                    $class = "hidden-xs";
                }
                if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="TECHBUY") {
                    $class = "hidden-xs";
                }
                ?>
                <div class="form-group-row <?=$class?>">
                    <div class="form-group tablet-half">
                        <input type="text" class="form-control" id="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                               name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                               size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>"
                               value="<?= $value ?>"
                               <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?>/><?
                        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime1"):?><?
                            $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                    'FORM_NAME' => 'iblock_add',
                                    'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
                                    'INPUT_VALUE' => $value,
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                            <small><?= GetMessage("IBLOCK_FORM_DATE_FORMAT") ?><?= FORMAT_DATETIME ?></small><?
                        endif;

                        ?>
                        <? if (intval($propertyID) > 0) { ?>
                            <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></label>
                        <? } else { ?>
                            <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?></label>
                        <? } ?>

                        <script type="text/javascript">
                            <? if (intval($propertyID) > 0): ?>
                            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>');
                            <? endif ?>

                            $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").addClass("styled-select");
                        </script>
                    </div>
                </div>
                <?
            }
            break;

        case "L":


            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
            else
                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

            switch ($type):
                case "checkbox":
                case "radio":
                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                        $checked = false;
                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                            if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
                                foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                                    if ($arElEnum["VALUE"] == $key) {
                                        $checked = true;
                                        break;
                                    }
                                }
                            }
                        } else {
                            if ($arEnum["DEF"] == "Y") $checked = true;
                        }

                        ?>
                        <input type="<?= $type ?>"
                               name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
                               value="<?= $key ?>"
                               id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?>
                               <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> />
                        <label for="property_<?= $key ?>"><?= $arEnum["VALUE"] ?></label>
                        <?
                    }
                    break;

                case "dropdown":
                case "multiselect":

                    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]=="TECHBUY") {
                        $class = "hidden-xs techbuy";
                    }
                    ?>
                    <div class="form-group-row <?=$class?>">
                    <div class="form-group select-group">
                        <select id="PROPERTY[<?= $propertyID ?>]"
                                data-select-placeholder="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>"
                                class="styled-select"
                                name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
                            <option value=""><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></option>
                            <?
                            if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                            else $sKey = "ELEMENT";

                            foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                                $checked = false;
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                        if ($key == $arElEnum["VALUE"]) {
                                            $checked = true;
                                            break;
                                        }
                                    }
                                } else {
                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                }
                                ?>
                                <option value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                                <?
                            }
                            ?>
                        </select>
                    </div>
                    </div><?
                    break;

            endswitch;
            break;
        endswitch;
        endforeach; ?>

            <? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
                <div class="form-group-row">
                    <?= GetMessage("IBLOCK_FORM_CAPTCHA_TITLE") ?>
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                </div>
                <div class="form-group-row">
                    <?= GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT") ?><span class="starrequired">*</span>:
                    <input type="text" name="captcha_word" maxlength="50" value="">
                </div>
            <? endif ?>

            <? endif ?>
        </div>
    </div>

    <?if($detect->isMobile()) {?>
    <div class="upload-image-mobile">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])){ ?>
            <? foreach ($arResult["PROPERTY_LIST"] as $propertyID):

                if (intval($propertyID) > 0) {
                    if (
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                        &&
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                    )
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                    elseif (
                        (
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                            ||
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                        )
                        &&
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                    )
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                    $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                    $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                } else {
                    $inputNum = 1;
                }

                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                    $INPUT_TYPE = "USER_TYPE";
                else
                    $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                switch ($INPUT_TYPE):
                    case "S":
                    case "N":
                        if($propertyID!=76) break;
                        for ($i = 0; $i < $inputNum; $i++) {
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                            } elseif ($i == 0) {
                                $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                            } else {
                                $value = "";
                            }
                            $class = "";
                            if($i==0) {
                                $class = "active";
                            } else {
                                $class = "hidden-xs";
                            }
                            ?>
                            <div class="phone-apply-container <?=$class?>">
                                <?php if($i==0) { ?>
                                <h4><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST_PHONE") ?></h4>
                                <? } ?>
                                <div class="form-group-row">
                                    <div class="form-group full-width">
                                        <input type="text" class="form-control phone-control phone_<?=$propertyID?>_<?= $i ?>" data-propid="<?=$propertyID?>_<?= $i ?>" id="phoneApply"
                                               placeholder="+7 ( ___ ) ___ - __ - __"  im-insert="true">
                                        <input type="text" class="copy-phone-field phonec_<?=$propertyID?>_<?= $i ?>" id="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                               name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                               size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>"
                                               value="<?= $value ?>"
                                               <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?>/>
                                    </div>
                                </div>
                                <?php //if($i==0) { ?>
                                    <div class="button-container">
                                        <a href="javascript:void(true);" class="btn btn-secondary" id="<?=$propertyID?>_<?= $i ?>">
                                            <span><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST_LINK") ?></span>
                                        </a>
                                    </div>
                                <? //} ?>
                                <!--</form>-->
                            </div>
                            <?
                        }
                        break;

                    case "F":
                        for ($i = 0; $i < $inputNum; $i++) {
                            $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                            ?>
                            <?php
                            $class = "";
                            $classblock = "active";
                            $visibility = "class='imagesInputMobile'";

                            if ($i >= 1) {
                                $class = "class='hidden-xs'";
                                $classblock = "hidden-xs";
                                $visibility = "class='unvisibl-xs imagesInputMobile'";
                            } ?>
                            <?
                            if ($i == 0) {
                                $class = "class='hidden-xs'";
                                $visibility = "class='unvisibl-xs imagesInputMobile'";
                            } ?>
                            <div  class="upload-wrapper <?= $classblock ?> photo_item_<?= $propertyID ?>_<?=$i?>">
                                <div class="empty-placeholder" id="active">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="157px"
                                         height="137px">
                                        <path fill-rule="evenodd" fill="rgb(0, 0, 0)"
                                              d="M150.604,130.174 C146.530,134.249 141.616,136.286 135.858,136.286 L21.144,136.286 C15.385,136.286 10.471,134.249 6.396,130.174 C2.324,126.102 0.287,121.186 0.287,115.428 L0.287,42.428 C0.287,36.671 2.324,31.756 6.396,27.682 C10.471,23.609 15.385,21.572 21.144,21.572 L39.394,21.572 L43.548,10.492 C44.581,7.831 46.467,5.536 49.210,3.607 C51.954,1.679 54.765,0.714 57.644,0.714 L99.358,0.714 C102.237,0.714 105.048,1.679 107.791,3.607 C110.536,5.536 112.422,7.831 113.455,10.492 L117.608,21.572 L135.858,21.572 C141.617,21.572 146.530,23.609 150.604,27.682 C154.677,31.756 156.714,36.671 156.713,42.428 L156.713,115.428 C156.713,121.186 154.676,126.102 150.604,130.174 ZM104.287,53.144 C97.143,46.002 88.549,42.430 78.501,42.430 C68.452,42.430 59.856,46.001 52.714,53.144 C45.572,60.287 42.000,68.881 42.000,78.930 C42.000,88.977 45.571,97.574 52.714,104.716 C59.857,111.858 68.452,115.430 78.501,115.430 C88.549,115.430 97.145,111.858 104.287,104.716 C111.428,97.573 115.000,88.977 115.000,78.930 C115.000,68.881 111.430,60.286 104.287,53.144 ZM78.501,102.393 C72.037,102.393 66.510,100.099 61.920,95.510 C57.331,90.920 55.036,85.394 55.036,78.930 C55.036,72.466 57.331,66.940 61.920,62.350 C66.510,57.759 72.037,55.465 78.501,55.465 C84.963,55.465 90.491,57.759 95.081,62.350 C99.670,66.940 101.965,72.466 101.965,78.930 C101.965,85.394 99.670,90.920 95.081,95.510 C90.491,100.099 84.963,102.393 78.501,102.393 Z"></path>
                                    </svg>
                                </div>
                            </div>
                            <div class="button-container <?= $classblock ?> item_<?= $propertyID ?>_<?=$i?>">
                                <a href="javascript:void(false);" class="btn btn-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="28px"
                                         height="27px">
                                        <path fill-rule="evenodd" fill="rgb(244, 64, 41)"
                                              d="M26.125,19.294 C24.905,20.700 23.264,21.541 21.508,21.673 C21.485,21.673 21.467,21.673 21.450,21.673 L17.178,21.673 C16.747,21.673 16.401,21.313 16.401,20.862 C16.401,20.412 16.747,20.051 17.178,20.051 L21.421,20.051 C24.196,19.829 26.447,17.234 26.447,14.249 C26.447,11.431 24.513,8.962 21.951,8.506 C21.606,8.446 21.341,8.151 21.306,7.785 C20.984,4.265 18.180,1.610 14.783,1.610 C12.256,1.610 9.930,3.147 8.854,5.532 C8.681,5.911 8.260,6.091 7.881,5.947 C7.570,5.827 7.236,5.767 6.896,5.767 C5.307,5.767 4.012,7.118 4.012,8.776 C4.012,9.142 4.069,9.485 4.184,9.809 C4.311,10.182 4.173,10.596 3.845,10.794 C2.434,11.654 1.559,13.239 1.559,14.939 C1.559,17.570 3.540,19.907 5.894,20.057 L10.822,20.057 C11.254,20.057 11.600,20.418 11.600,20.868 C11.600,21.319 11.254,21.679 10.822,21.679 L5.871,21.679 C5.854,21.679 5.842,21.679 5.825,21.679 C4.259,21.589 2.791,20.814 1.680,19.505 C0.598,18.225 0.005,16.609 0.005,14.945 C0.005,12.885 0.966,10.951 2.549,9.749 C2.492,9.437 2.457,9.118 2.457,8.788 C2.457,6.235 4.449,4.157 6.896,4.157 C7.184,4.157 7.472,4.187 7.748,4.241 C8.387,3.075 9.291,2.084 10.379,1.351 C11.686,0.468 13.212,-0.000 14.778,-0.000 C16.804,-0.000 18.750,0.787 20.247,2.216 C21.594,3.502 22.475,5.196 22.774,7.052 C25.791,7.881 27.996,10.879 28.002,14.249 C28.002,16.111 27.340,17.901 26.125,19.294 ZM9.890,15.954 C9.694,15.954 9.493,15.870 9.343,15.714 C9.038,15.396 9.038,14.885 9.343,14.567 L13.459,10.272 C13.603,10.116 13.805,10.032 14.006,10.032 C14.208,10.032 14.409,10.122 14.553,10.272 L18.669,14.567 C18.975,14.885 18.975,15.396 18.669,15.714 C18.364,16.032 17.875,16.032 17.576,15.714 L14.783,12.801 L14.783,26.190 C14.783,26.641 14.438,27.001 14.006,27.001 C13.574,27.001 13.229,26.641 13.229,26.190 L13.229,12.801 L10.437,15.714 C10.287,15.876 10.085,15.954 9.890,15.954 Z"></path>
                                    </svg>
                                    <span>Загрузить фотографии</span>
                                </a>
                                <input <?= $class ?> type="hidden"
                                       name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                       value="<?= $value ?>"/>
                                <input <?= $visibility ?> id="<?= $propertyID ?>_<?=$i?>"   type="file"
                                       size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                       name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>
                                <?

                                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
                                    ?>
                                    <input type="checkbox"
                                           name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                           id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y"
                                           <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> />
                                    <label
                                            for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
                                    <?

                                    if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
                                        ?>
                                        <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"
                                             height="<?= $arResult["ELEMENT_FILES"][$value]["HEIGHT"] ?>"
                                             width="<?= $arResult["ELEMENT_FILES"][$value]["WIDTH"] ?>" border="0"/>
                                        <?
                                    } else {
                                        ?>
                                        <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
                                        <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?> b
                                        [
                                        <a href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
                                        <?
                                    }
                                } ?>
                            </div>
                            <?
                        }

                        break;
                endswitch;
            endforeach;
        } ?>
    </div>
    <?}?>

    <?if(!$detect->isMobile()) {?>
    <? /*Photos Block code begin */ ?>
    <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
        <? foreach ($arResult["PROPERTY_LIST"] as $propertyID):

            if (intval($propertyID) > 0) {
                if (
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                elseif (
                    (
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                        ||
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                    )
                    &&
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                )
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
            } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
            } else {
                $inputNum = 1;
            }

            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                $INPUT_TYPE = "USER_TYPE";
            else
                $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

            switch ($INPUT_TYPE):
                case "F":

                    ?>
                    <div class="form-group-row type-2 desktop-only">
                        <div class="styled-checkbox type-1">
                            <div class="checkbox-group">
                                <input type="checkbox" id="checkbox1">
                                <label for="checkbox1">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 70 70"
                                         style="enable-background:new 0 0 70 70;" xml:space="preserve">
                  <path d="M26.474,70c-2.176,0-4.234-1.018-5.557-2.764L3.049,43.639C0.725,40.57,1.33,36.2,4.399,33.875
                    c3.074-2.326,7.441-1.717,9.766,1.35l11.752,15.518L55.474,3.285c2.035-3.265,6.332-4.264,9.604-2.232
                    c3.268,2.034,4.266,6.334,2.23,9.602l-34.916,56.06c-1.213,1.949-3.307,3.175-5.6,3.279C26.685,69.998,26.58,70,26.474,70z"></path>
                </svg>
                                </label>
                            </div>
                            <span>Данная техника продается</span>
                        </div>
                        <div class="desktop-only-load-images">
                            <?php
                            for ($i = 0; $i < $inputNum; $i++) {
                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];


                                $class = "";
                                $classblock = "active";
                                $visibility = "class='imagesInput'";
                                if ($i >= 1) {
                                    $class = "class='hidden-xs'";
                                    $classblock = "hidden-xs";
                                    $visibility = "class='unvisibl-xs imagesInput'";
                                } ?>
                                <div id="load_photo" class="<?= $classblock ?> item_<?= $propertyID ?>_<?=$i?>">
                                <a href="javascript:void(false);" class="btn btn-secondary <?= $classblock ?>">
                                    <?
                                    if ($i == 0) { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="28px" height="27px">
                                            <path fill-rule="evenodd" fill="rgb(244, 64, 41)"
                                                  d="M26.125,19.294 C24.905,20.700 23.264,21.541 21.508,21.673 C21.485,21.673 21.467,21.673 21.450,21.673 L17.178,21.673 C16.747,21.673 16.401,21.313 16.401,20.862 C16.401,20.412 16.747,20.051 17.178,20.051 L21.421,20.051 C24.196,19.829 26.447,17.234 26.447,14.249 C26.447,11.431 24.513,8.962 21.951,8.506 C21.606,8.446 21.341,8.151 21.306,7.785 C20.984,4.265 18.180,1.610 14.783,1.610 C12.256,1.610 9.930,3.147 8.854,5.532 C8.681,5.911 8.260,6.091 7.881,5.947 C7.570,5.827 7.236,5.767 6.896,5.767 C5.307,5.767 4.012,7.118 4.012,8.776 C4.012,9.142 4.069,9.485 4.184,9.809 C4.311,10.182 4.173,10.596 3.845,10.794 C2.434,11.654 1.559,13.239 1.559,14.939 C1.559,17.570 3.540,19.907 5.894,20.057 L10.822,20.057 C11.254,20.057 11.600,20.418 11.600,20.868 C11.600,21.319 11.254,21.679 10.822,21.679 L5.871,21.679 C5.854,21.679 5.842,21.679 5.825,21.679 C4.259,21.589 2.791,20.814 1.680,19.505 C0.598,18.225 0.005,16.609 0.005,14.945 C0.005,12.885 0.966,10.951 2.549,9.749 C2.492,9.437 2.457,9.118 2.457,8.788 C2.457,6.235 4.449,4.157 6.896,4.157 C7.184,4.157 7.472,4.187 7.748,4.241 C8.387,3.075 9.291,2.084 10.379,1.351 C11.686,0.468 13.212,-0.000 14.778,-0.000 C16.804,-0.000 18.750,0.787 20.247,2.216 C21.594,3.502 22.475,5.196 22.774,7.052 C25.791,7.881 27.996,10.879 28.002,14.249 C28.002,16.111 27.340,17.901 26.125,19.294 ZM9.890,15.954 C9.694,15.954 9.493,15.870 9.343,15.714 C9.038,15.396 9.038,14.885 9.343,14.567 L13.459,10.272 C13.603,10.116 13.805,10.032 14.006,10.032 C14.208,10.032 14.409,10.122 14.553,10.272 L18.669,14.567 C18.975,14.885 18.975,15.396 18.669,15.714 C18.364,16.032 17.875,16.032 17.576,15.714 L14.783,12.801 L14.783,26.190 C14.783,26.641 14.438,27.001 14.006,27.001 C13.574,27.001 13.229,26.641 13.229,26.190 L13.229,12.801 L10.437,15.714 C10.287,15.876 10.085,15.954 9.890,15.954 Z"></path>
                                        </svg>
                                        <span>Загрузить фотографии</span>
                                        <? $class = "class='hidden-xs'";
                                        $visibility = "class='unvisibl-xs imagesInput'";
                                    } else { ?>
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                               xmlns:xlink="http://www.w3.org/1999/xlink" width="28px" height="27px">
                                            <path fill-rule="evenodd" fill="rgb(244, 64, 41)"
                                                  d="M26.125,19.294 C24.905,20.700 23.264,21.541 21.508,21.673 C21.485,21.673 21.467,21.673 21.450,21.673 L17.178,21.673 C16.747,21.673 16.401,21.313 16.401,20.862 C16.401,20.412 16.747,20.051 17.178,20.051 L21.421,20.051 C24.196,19.829 26.447,17.234 26.447,14.249 C26.447,11.431 24.513,8.962 21.951,8.506 C21.606,8.446 21.341,8.151 21.306,7.785 C20.984,4.265 18.180,1.610 14.783,1.610 C12.256,1.610 9.930,3.147 8.854,5.532 C8.681,5.911 8.260,6.091 7.881,5.947 C7.570,5.827 7.236,5.767 6.896,5.767 C5.307,5.767 4.012,7.118 4.012,8.776 C4.012,9.142 4.069,9.485 4.184,9.809 C4.311,10.182 4.173,10.596 3.845,10.794 C2.434,11.654 1.559,13.239 1.559,14.939 C1.559,17.570 3.540,19.907 5.894,20.057 L10.822,20.057 C11.254,20.057 11.600,20.418 11.600,20.868 C11.600,21.319 11.254,21.679 10.822,21.679 L5.871,21.679 C5.854,21.679 5.842,21.679 5.825,21.679 C4.259,21.589 2.791,20.814 1.680,19.505 C0.598,18.225 0.005,16.609 0.005,14.945 C0.005,12.885 0.966,10.951 2.549,9.749 C2.492,9.437 2.457,9.118 2.457,8.788 C2.457,6.235 4.449,4.157 6.896,4.157 C7.184,4.157 7.472,4.187 7.748,4.241 C8.387,3.075 9.291,2.084 10.379,1.351 C11.686,0.468 13.212,-0.000 14.778,-0.000 C16.804,-0.000 18.750,0.787 20.247,2.216 C21.594,3.502 22.475,5.196 22.774,7.052 C25.791,7.881 27.996,10.879 28.002,14.249 C28.002,16.111 27.340,17.901 26.125,19.294 ZM9.890,15.954 C9.694,15.954 9.493,15.870 9.343,15.714 C9.038,15.396 9.038,14.885 9.343,14.567 L13.459,10.272 C13.603,10.116 13.805,10.032 14.006,10.032 C14.208,10.032 14.409,10.122 14.553,10.272 L18.669,14.567 C18.975,14.885 18.975,15.396 18.669,15.714 C18.364,16.032 17.875,16.032 17.576,15.714 L14.783,12.801 L14.783,26.190 C14.783,26.641 14.438,27.001 14.006,27.001 C13.574,27.001 13.229,26.641 13.229,26.190 L13.229,12.801 L10.437,15.714 C10.287,15.876 10.085,15.954 9.890,15.954 Z"></path>
                                        </svg>
                                        <span>Загрузить фотографии</span>
                                    <?}?>

                                    <input <?= $class ?> type="hidden"
                                                         name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                                         value="<?= $value ?>"/>
                                    <input <?=$visibility ?> id="<?= $propertyID ?>_<?=$i?>" type="file"
                                                         size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                                         name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>

                                </a>
								<?

                                    if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
                                        ?>

                                        <?

                                        if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
                                            ?>
                                            <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"
                                                 height="90"
                                                 width="90" border="0"/>
                                            <?
                                        } else {
                                            ?>
                                            <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
                                            <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?> b
                                            [
                                            <a href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
                                            <?
                                        }?><br>
                                        <input type="checkbox"
                                               name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                               id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y"
                                               <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> />
                                        <label
                                                for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
										<?
                                    } ?>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                    <?
                    break;
            endswitch;
        endforeach;
    endif ?>
    <? /*Photos Block code end */ ?>
    <?}?>

    <?if($detect->isMobile()) {?>
    <div class="phone-apply-container">
        <div class="styled-checkbox type-1">
            <div class="checkbox-group">
                <input type="checkbox" id="checkbox2">
                <label for="checkbox2">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px" viewBox="0 0 70 70" style="enable-background:new 0 0 70 70;"
                         xml:space="preserve">
                      <path d="M26.474,70c-2.176,0-4.234-1.018-5.557-2.764L3.049,43.639C0.725,40.57,1.33,36.2,4.399,33.875
                        c3.074-2.326,7.441-1.717,9.766,1.35l11.752,15.518L55.474,3.285c2.035-3.265,6.332-4.264,9.604-2.232
                        c3.268,2.034,4.266,6.334,2.23,9.602l-34.916,56.06c-1.213,1.949-3.307,3.175-5.6,3.279C26.685,69.998,26.58,70,26.474,70z"></path>
                    </svg>
                </label>
            </div>
            <span>Данная техника продается</span>
        </div>
    </div>
    <?}?>

    <div class="button-container">
        <button class="btn btn-primary add_request">
            <span><?= GetMessage("TEXT_SUBMIT") ?></span>
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px" width="474.8px" height="474.801px" viewBox="0 0 474.8 474.801"
                 style="enable-background:new 0 0 474.8 474.801;" xml:space="preserve">
              <path d="M396.283,257.097c-1.14-0.575-2.282-0.862-3.433-0.862c-2.478,0-4.661,0.951-6.563,2.857l-18.274,18.271
                c-1.708,1.715-2.566,3.806-2.566,6.283v72.513c0,12.565-4.463,23.314-13.415,32.264c-8.945,8.945-19.701,13.418-32.264,13.418
                H82.226c-12.564,0-23.319-4.473-32.264-13.418c-8.947-8.949-13.418-19.698-13.418-32.264V118.622
                c0-12.562,4.471-23.316,13.418-32.264c8.945-8.946,19.7-13.418,32.264-13.418H319.77c4.188,0,8.47,0.571,12.847,1.714
                c1.143,0.378,1.999,0.571,2.563,0.571c2.478,0,4.668-0.949,6.57-2.852l13.99-13.99c2.282-2.281,3.142-5.043,2.566-8.276
                c-0.571-3.046-2.286-5.236-5.141-6.567c-10.272-4.752-21.412-7.139-33.403-7.139H82.226c-22.65,0-42.018,8.042-58.102,24.126
                C8.042,76.613,0,95.978,0,118.629v237.543c0,22.647,8.042,42.014,24.125,58.098c16.084,16.088,35.452,24.13,58.102,24.13h237.541
                c22.647,0,42.017-8.042,58.101-24.13c16.085-16.084,24.134-35.45,24.134-58.098v-90.797
                C402.001,261.381,400.088,258.623,396.283,257.097z"></path>
                <path d="M467.95,93.216l-31.409-31.409c-4.568-4.567-9.996-6.851-16.279-6.851c-6.275,0-11.707,2.284-16.271,6.851
                L219.265,246.532l-75.084-75.089c-4.569-4.57-9.995-6.851-16.274-6.851c-6.28,0-11.704,2.281-16.274,6.851l-31.405,31.405
                c-4.568,4.568-6.854,9.994-6.854,16.277c0,6.28,2.286,11.704,6.854,16.274l122.767,122.767c4.569,4.571,9.995,6.851,16.274,6.851
                c6.279,0,11.704-2.279,16.274-6.851l232.404-232.403c4.565-4.567,6.854-9.994,6.854-16.274S472.518,97.783,467.95,93.216z"></path>
            </svg>
        </button>
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on("click", "button.add_request", function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $("input[name='iblock_submit']").trigger("click");
                    return false;
                });
            });
        </script>

        <input type="submit" name="iblock_submit" style="display:none;"
               value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
        <? if (strlen($arParams["LIST_URL"]) > 0): ?>
            <input style="display:none;" type="submit" name="iblock_apply"
                   value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
            <input style="display:none;" type="button" name="iblock_cancel"
                   value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                   onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';">
        <? endif ?>
    </div>
</form>