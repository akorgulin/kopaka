<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- Advantages section -->
<section class="section advantages-section">
    <div class="container">
        <h2>Почему это удобно</h2>
        <div class="advantages-list">
            <? foreach($arResult["STEPS"] as $counter=>$itemRow) { ?>
                <div class="item">
                    <div class="icon-container"></div>
                    <p><?= trim(strip_tags($itemRow["DETAIL_TEXT"])); ?></p>
                </div>
            <? } ?>
        </div>
    </div>
</section>
<!-- End of Advantages section -->