<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- Logos area -->
<section class="logos_area">
    <div class="container">
        <div class="row left">
            <h1 class="page_header aos-init aos-animate left" data-aos="fade-up" data-aos-delay="300"><?$APPLICATION->ShowTitle()?>:</h1>
            <? foreach($arResult["BRANDS_PROPERTY"] as $i=>$itemBrand) { ?>
                <?if($itemBrand["PREVIEW_PICTURE"]["src"]) { ?>
                <div class="logo_item center" data-aos="fade-up" data-aos-delay="200">
                    <a href="/brands/<?=strtolower($itemBrand["UF_XML_ID"])?>/" >
                        <img src="<?=$itemBrand["PREVIEW_PICTURE"]["src"]?>" alt="">
                    </a>
                </div>
                <? } ?>
            <? } ?>
        </div>
    </div>
</section>
