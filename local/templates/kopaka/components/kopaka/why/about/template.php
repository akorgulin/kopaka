<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$rows = count($arResult["BRANDS"])/5;
$brands = [];
for($i=0;$i<=$rows;) {
    $start = $i*4;
    $brands[$i] = array_slice($arResult["BRANDS"],$start, 4);
    $i++;
}
?>
<!-- Logos area -->
<section class="logos_area">
    <div class="container">
        <div class="row">
        <? foreach($brands as $i=>$itemBrandRow) {  ?>
            <?if($i==0) {?>
                <h2 class="logo_block" data-aos="fade-up" data-aos-delay="200">Институт красоты FIJIE представляет следующие бренды</h2>
            <? } ?>
            <?if($i==1) {?>
                <h2 data-aos="fade-up" data-aos-delay="200">Для профессионалов</h2>
            <? } ?>
            <? foreach($itemBrandRow as $itemBrand) { ?>
                <div class="logo_item center" data-aos="fade-up" data-aos-delay="200">
                    <a <?if($itemBrand["CODE"]) { ?> href="/brands/<?=strtolower($itemBrand["CODE"])?>/"  <? } ?>>
                        <img src="<?=CFile::GetPath($itemBrand["PREVIEW_PICTURE"])?>" alt="">
                    </a>
                </div>
            <? } ?>
            <?if($i==0) {?>
            <hr data-aos="fade-up" data-aos-delay="200">
            <? } ?>
        <? } ?>
        </div>
    </div>
</section>
