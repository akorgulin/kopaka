<?
use Bitrix\Main;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Controller\PhoneAuth;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Json;
use Bitrix\Sale;
use Bitrix\Sale\Delivery;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Location\GeoIp;
use Bitrix\Sale\Location\LocationTable;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\PersonType;
use Bitrix\Sale\Result;
use Bitrix\Sale\Services\Company;
use Bitrix\Sale\Shipment;

Class UserData {

    protected function getNormalizedPhone($phone)
    {
        if ($this->arParams['USE_PHONE_NORMALIZATION'] === 'Y')
        {
            $phone = NormalizePhone((string)$phone, 3);
        }

        return $phone;
    }
    
    /**
     * Generation of user registration fields (login, password, etc)
     *
     * @param array $userProps
     * @return array
     * @throws Main\ArgumentNullException
     */
    public function generateUserData($userProps = array())
    {
        global $USER;

        $userEmail = isset($userProps['EMAIL']) ? trim((string)$userProps['EMAIL']) : '';
        $newLogin = $userEmail;

        if (empty($userEmail))
        {
            $newEmail = false;
            $normalizedPhone = $this->getNormalizedPhone($userProps['PHONE']);

            if (!empty($normalizedPhone))
            {
                $newLogin = $normalizedPhone;
            }
        }
        else
        {
            $newEmail = $userEmail;
        }

        if (empty($newLogin))
        {
            $newLogin = randString(5).mt_rand(0, 99999);
        }

        $pos = strpos($newLogin, '@');
        if ($pos !== false)
        {
            $newLogin = substr($newLogin, 0, $pos);
        }

        if (strlen($newLogin) > 47)
        {
            $newLogin = substr($newLogin, 0, 47);
        }

        $newLogin = str_pad($newLogin, 3, '_');

        $dbUserLogin = CUser::GetByLogin($newLogin);
        if ($userLoginResult = $dbUserLogin->Fetch())
        {
            do
            {
                $newLoginTmp = $newLogin.mt_rand(0, 99999);
                $dbUserLogin = CUser::GetByLogin($newLoginTmp);
            }
            while ($userLoginResult = $dbUserLogin->Fetch());

            $newLogin = $newLoginTmp;
        }

        $newName = '';
        $newLastName = '';
        $payerName = isset($userProps['PAYER']) ? trim((string)$userProps['PAYER']) : '';

        if (!empty($payerName))
        {
            $arNames = explode(' ', $payerName);

            if (isset($arNames[1]))
            {
                $newName = $arNames[1];
                $newLastName = $arNames[0];
            }
            else
            {
                $newName = $arNames[0];
            }
        }

        $groupIds = [];
        $defaultGroups = Option::get('main', 'new_user_registration_def_group', '');

        if (!empty($defaultGroups))
        {
            $groupIds = explode(',', $defaultGroups);
        }

        $arPolicy = $USER->GetGroupPolicy($groupIds);

        $passwordMinLength = (int)$arPolicy['PASSWORD_LENGTH'];
        if ($passwordMinLength <= 0)
        {
            $passwordMinLength = 6;
        }

        $passwordChars = array(
            'abcdefghijklnmopqrstuvwxyz',
            'ABCDEFGHIJKLNMOPQRSTUVWXYZ',
            '0123456789',
        );
        if ($arPolicy['PASSWORD_PUNCTUATION'] === 'Y')
        {
            $passwordChars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
        }

        $newPassword = $newPasswordConfirm = randString($passwordMinLength + 2, $passwordChars);

        return array(
            'NEW_EMAIL' => $newEmail,
            'NEW_LOGIN' => $newLogin,
            'NEW_NAME' => $newName,
            'NEW_LAST_NAME' => $newLastName,
            'NEW_PASSWORD' => $newPassword,
            'NEW_PASSWORD_CONFIRM' => $newPasswordConfirm,
            'GROUP_ID' => $groupIds
        );
    }
}
?>