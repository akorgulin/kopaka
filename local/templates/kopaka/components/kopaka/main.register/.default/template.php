<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$userData = new UserData();
$newUserData = $userData->generateUserData();

if($arResult["SHOW_SMS_FIELD"] == true)
{
    CJSCore::Init('phone_auth');
}
?>


    <?if($USER->IsAuthorized()):?>
     Вы авторизованы!
    <?else:?>
    <?
    if (count($arResult["ERRORS"]) > 0):?><div id="bx_register_error"><?
        foreach ($arResult["ERRORS"] as $key => $error)
            if (intval($key) == 0 && $key !== 0)
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

        ShowError(implode("<br />", $arResult["ERRORS"]));

    ?></div><?elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
    ?>
        <p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
    <?endif?>

    <?if($arResult["SHOW_SMS_FIELD"] == true):?>

        <form method="post" class="form" action="<?=POST_FORM_ACTION_URI?>" name="regform">
            <?
            if($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
            endif;
            ?>
            <input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
            <div class="form-group-container">
                <div class="form-group">
                    <input size="30" type="text" class="form-control" name="SMS_CODE" placeholder="<?=GetMessage("main_register_sms")?>" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" />
                </div>
            </div>
            <div class="button-container">
                <button class="btn btn-primary">
                    <span><?= GetMessage("main_register_sms_send") ?></span>
                    <div id="svgnode">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                    <path d="M331.685,425.378c-7.478,7.479-7.478,19.584,0,27.043c7.479,7.478,19.584,7.478,27.043,0l131.943-131.962
                          c3.979-3.979,5.681-9.276,5.412-14.479c0.269-5.221-1.434-10.499-5.412-14.477L358.728,159.56
                          c-7.459-7.478-19.584-7.478-27.043,0c-7.478,7.478-7.478,19.584,0,27.042l100.272,100.272H19.125C8.568,286.875,0,295.443,0,306
                          c0,10.557,8.568,19.125,19.125,19.125h412.832L331.685,425.378z M535.5,38.25H153c-42.247,0-76.5,34.253-76.5,76.5v76.5h38.25
                          v-76.5c0-21.114,17.117-38.25,38.25-38.25h382.5c21.133,0,38.25,17.136,38.25,38.25v382.5c0,21.114-17.117,38.25-38.25,38.25H153
                          c-21.133,0-38.25-17.117-38.25-38.25v-76.5H76.5v76.5c0,42.247,34.253,76.5,76.5,76.5h382.5c42.247,0,76.5-34.253,76.5-76.5
                          v-382.5C612,72.503,577.747,38.25,535.5,38.25z"></path></div>
                    <input type="hidden"  name="code_submit_button" value="<?echo GetMessage("main_register_sms_send")?>" />
                    <input type="submit"  class="phone-reg-code-submit" name="code_submit_button" value="<?echo GetMessage("main_register_sms_send")?>" />
                </svg>
                </button>
            </div>
        </form>

        <script>
            new BX.PhoneAuth({
                containerId: 'bx_register_resend',
                errorContainerId: 'bx_register_error',
                interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
                data:
                <?=CUtil::PhpToJSObject([
                    'signedData' => $arResult["SIGNED_DATA"],
                ])?>,
                onError:
                    function(response)
                    {
                        var errorDiv = BX('bx_register_error');
                        var btnDiv = BX('bx_register_resend_link');

                        var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
                        errorNode.innerHTML = '';
                        for(var i = 0; i < response.errors.length; i++)
                        {
                            errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
                        }
                        errorDiv.style.display = '';
                    }
            });
        </script>

        <div id="bx_register_resend"></div>
    <?else:?>

        <h4><?=GetMessage("AUTH_REGISTER")?></h4>

        <form method="post" class="form" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
            <?
            if($arResult["BACKURL"] <> ''):
                ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?
            endif;
            ?>

                <?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
                <div class="form-group-container <?if($FIELD!="PHONE_NUMBER"){?>hidden<?} else { ?>phone<?}?>">
                    <div class="form-group">
                         <?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>

                        <select name="REGISTER[AUTO_TIME_ZONE]" onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
                            <option value=""><?echo GetMessage("main_profile_time_zones_auto_def")?></option>
                            <option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_yes")?></option>
                            <option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>><?echo GetMessage("main_profile_time_zones_auto_no")?></option>
                        </select>


                        <select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
                            <?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name):?>
                                <option value="<?=htmlspecialcharsbx($tz)?>"<?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>><?=htmlspecialcharsbx($tz_name)?></option>
                            <?endforeach?>
                        </select>

                        <?else:?>
                            <?
                                switch ($FIELD)
                                {
                                    case "PASSWORD":
                                        ?><input size="30" type="password" class="form-control" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="bx-auth-input" />
                                    <?if($arResult["SECURE_AUTH"]):?>
                                        <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                                        <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                                        </noscript>
                                        <script type="text/javascript">
                                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                        </script>
                                    <?endif?>
                                        <?
                                        break;
                                    case "CONFIRM_PASSWORD":
                                        ?><input size="30" type="password" class="form-control" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" /><?
                                        break;

                                    case "PERSONAL_GENDER":
                                        ?><select name="REGISTER[<?=$FIELD?>]">
                                        <option value=""><?=GetMessage("USER_DONT_KNOW")?></option>
                                        <option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_MALE")?></option>
                                        <option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>><?=GetMessage("USER_FEMALE")?></option>
                                        </select><?
                                        break;

                                    case "PERSONAL_COUNTRY":
                                    case "WORK_COUNTRY":
                                        ?><select name="REGISTER[<?=$FIELD?>]"><?
                                        foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
                                        {
                                            ?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
                                            <?
                                        }
                                        ?></select><?
                                        break;

                                    case "PERSONAL_PHOTO":
                                    case "WORK_LOGO":
                                        ?><input size="30" type="file" class="form-control" name="REGISTER_FILES_<?=$FIELD?>" /><?
                                        break;

                                    case "PERSONAL_NOTES":
                                    case "WORK_NOTES":
                                        ?><textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]"><?=$arResult["VALUES"][$FIELD]?></textarea><?
                                        break;
                                    default:
                                        if ($FIELD == "PERSONAL_BIRTHDAY"):?><small><?=$arResult["DATE_FORMAT"]?></small><br /><?endif;
                                        ?>
                                        <?if($FIELD=="PHONE_NUMBER"){?>
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <path fill-rule="evenodd" fill="rgb(175, 181, 189)" d="M22.500,43.000 L5.500,43.000 C3.025,43.000 1.000,41.017 1.000,38.593 L1.000,20.999 C0.448,20.999 -0.000,20.552 -0.000,19.999 L-0.000,14.999 C-0.000,14.447 0.448,13.999 1.000,13.999 L1.000,11.999 C0.448,11.999 -0.000,11.552 -0.000,10.999 L-0.000,8.999 C-0.000,8.447 0.448,7.999 1.000,7.999 L1.000,4.406 C1.000,1.982 3.025,-0.001 5.500,-0.001 L22.500,-0.001 C24.975,-0.001 27.000,1.982 27.000,4.406 L27.000,38.593 C27.000,41.017 24.975,43.000 22.500,43.000 ZM14.000,40.000 C15.105,40.000 16.000,39.104 16.000,38.000 C16.000,36.895 15.105,36.000 14.000,36.000 C12.895,36.000 12.000,36.895 12.000,38.000 C12.000,39.104 12.895,40.000 14.000,40.000 ZM17.000,1.999 L11.000,1.999 C10.448,1.999 10.000,2.447 10.000,2.999 C10.000,3.552 10.448,3.999 11.000,3.999 L17.000,3.999 C17.552,3.999 18.000,3.552 18.000,2.999 C18.000,2.447 17.552,1.999 17.000,1.999 ZM24.000,5.999 L4.000,5.999 L4.000,33.999 L24.000,33.999 L24.000,5.999 Z"></path>
                                            </svg>
                                        <?}?>
                                        <input size="30" type="text" class="form-control <?if($FIELD=="PHONE_NUMBER"){?>phone-control<?}?>" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" /><?
                                        if ($FIELD == "PERSONAL_BIRTHDAY")
                                            $APPLICATION->IncludeComponent(
                                                'bitrix:main.calendar',
                                                '',
                                                array(
                                                    'SHOW_INPUT' => 'N',
                                                    'FORM_NAME' => 'regform',
                                                    'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
                                                    'SHOW_TIME' => 'N'
                                                ),
                                                null,
                                                array("HIDE_ICONS"=>"Y")
                                            );
                                        ?><?
                                }?>
                    <?endif?>
                    </div>
                </div>
                <?if($FIELD=="LOGIN"){?>
                    <script type="text/javascript">
                        $("input[name='REGISTER[<?= $FIELD ?>]']").val("<?=$newUserData["NEW_LOGIN"]?>");
                    </script>
                <?}?>
                <?if($FIELD=="PASSWORD"){?>
                    <script type="text/javascript">
                        $("input[name='REGISTER[<?= $FIELD ?>]']").val("<?=$newUserData["NEW_PASSWORD"]?>");
                    </script>
                <?}?>
                <?if($FIELD=="CONFIRM_PASSWORD"){?>
                    <script type="text/javascript">
                        $("input[name='REGISTER[<?= $FIELD ?>]']").val("<?=$newUserData["NEW_PASSWORD_CONFIRM"]?>");
                    </script>
                <?}?>
                <?endforeach?>
                <?// ********************* User properties ***************************************************?>
                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                <div class="form-group-container hidden">
                    <div class="form-group">
                    <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                        <?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:system.field.edit",
                                    $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                    array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS"=>"Y"));?>
                    <?endforeach;?>
                    </div>
                </div>
                <?endif;?>
                <?// ******************** /User properties ***************************************************?>
                <?
                /* CAPTCHA */
                if ($arResult["USE_CAPTCHA"] == "Y")
                {
                    ?>
                    <div class="form-group-container hidden">
                        <div class="form-group">
                            <?=GetMessage("REGISTER_CAPTCHA_TITLE")?>
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                             <?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span>
                             <input type="text" class="form-control" name="captcha_word" maxlength="50" value="" autocomplete="off" />
                        </div>
                    </div>
                    <?
                }
                /* !CAPTCHA */
                ?>

                 <div class="button-container">

                     <button class="btn btn-primary">
                        <span><?= GetMessage("AUTH_REGISTER_BTN") ?></span>
                         <div id="svgnode">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                          <path d="M331.685,425.378c-7.478,7.479-7.478,19.584,0,27.043c7.479,7.478,19.584,7.478,27.043,0l131.943-131.962
                                  c3.979-3.979,5.681-9.276,5.412-14.479c0.269-5.221-1.434-10.499-5.412-14.477L358.728,159.56
                                  c-7.459-7.478-19.584-7.478-27.043,0c-7.478,7.478-7.478,19.584,0,27.042l100.272,100.272H19.125C8.568,286.875,0,295.443,0,306
                                  c0,10.557,8.568,19.125,19.125,19.125h412.832L331.685,425.378z M535.5,38.25H153c-42.247,0-76.5,34.253-76.5,76.5v76.5h38.25
                                  v-76.5c0-21.114,17.117-38.25,38.25-38.25h382.5c21.133,0,38.25,17.136,38.25,38.25v382.5c0,21.114-17.117,38.25-38.25,38.25H153
                                  c-21.133,0-38.25-17.117-38.25-38.25v-76.5H76.5v76.5c0,42.247,34.253,76.5,76.5,76.5h382.5c42.247,0,76.5-34.253,76.5-76.5
                                  v-382.5C612,72.503,577.747,38.25,535.5,38.25z"></path>
                        </svg>
                         </div>
                         <input type="hidden"  name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER_BTN")?>" />
                        <input type="submit"  class="phone-reg-submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER_BTN")?>" />
                     </button>
                </div>
        </form>
    <?endif //$arResult["SHOW_SMS_FIELD"] == true ?>
<?endif?>