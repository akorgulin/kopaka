<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;

?>
<? CUtil::InitJSCore(array("image")); ?>

<!--Category-->
<section class="content_wrapper nobackground clearfix">
    <div class="wrapper center">
        <h1 class="page_heading small dark" data-aos="flip-up" data-aos-delay="400"><?=$arResult["BLOG"]["NAME"]?></h1>

        <div class="cat_list_open" data-aos="flip-up" data-aos-delay="500"><span class="lnr lnr-chevron-down"></span>Показать
            категории
        </div>
        <?
        $arGroups = [];
        foreach ($arResult["POST"] as $ind => $CurPost) {

            foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){

                if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_GROUP") {
                    if (!empty($arPostField["VALUE"])) {
                        $arGroups[$arPostField["VALUE"]] = $arPostField["VALUE"];
                    }
                }
                ?>
            <?}
        }
        ?>
        <ul class="categories_list clearfix" data-aos="flip-up" data-aos-delay="500">
            <?foreach ($arGroups as $ind => $group_name) {?>
                <li <?if($_REQUEST["group"]==$group_name){?>class="current"<?}?>><a href="?group=<?=$group_name?>"><?=$group_name?></a></li>
            <?}?>
        </ul>
    </div>
    <div class="container">
        <div class="grid_blocks category_blog clearfix">
            <?
            if (!empty($arResult["OK_MESSAGE"])) {
                ?>
                <div class="blog-notes blog-note-box">
                    <div class="blog-note-text">
                        <ul>
                            <?
                            foreach ($arResult["OK_MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }
            if (!empty($arResult["MESSAGE"])) {
                ?>
                <div class="blog-textinfo blog-note-box">
                    <div class="blog-textinfo-text">
                        <ul>
                            <?
                            foreach ($arResult["MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }
            if (!empty($arResult["ERROR_MESSAGE"])) {
                ?>
                <div class="blog-errors blog-note-box blog-note-error">
                    <div class="blog-error-text">
                        <ul>
                            <?
                            foreach ($arResult["ERROR_MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }

            if (count($arResult["POST"]) > 0) {
                foreach ($arResult["POST"] as $ind => $CurPost) {
                    $className = "blog-post";
                    if ($ind == 0)
                        $className .= " blog-post-first";
                    elseif (($ind + 1) == count($arResult["POST"]))
                        $className .= " blog-post-last";
                    if ($ind % 2 == 0)
                        $className .= " blog-post-alt";
                    $className .= " blog-post-year-" . $CurPost["DATE_PUBLISH_Y"];
                    $className .= " blog-post-month-" . IntVal($CurPost["DATE_PUBLISH_M"]);
                    $className .= " blog-post-day-" . IntVal($CurPost["DATE_PUBLISH_D"]);
                    //dump($CurPost);

                    $horisontal = false;
                    $class= false;
                    foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){?>
                        <?
                        if($arPostField["FIELD_NAME"]=="UF_BLOG_POST_CAT" && !empty($arPostField["VALUE"]) && $arPostField["VALUE"]==329) {
                            $horisontal = false;
                            $class = "without_overlay without_overlay aos-init aos-animate";
                        } else if($arPostField["FIELD_NAME"]=="UF_BLOG_POST_CAT" && !empty($arPostField["VALUE"]) && $arPostField["VALUE"]==330) {
                            $horisontal = true;
                            $class= "";
                        }
                        ?>
                    <?} ?>

                    <?foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField) {?>
                        <?
                        if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC" && !$horisontal) {
                            if (!empty($arPostField["VALUE"]) && $arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC") {
                                $arFile = CFile::GetFileArray($arPostField["VALUE"]);
                                $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($CurPost["DATE_PUBLISH_DATE"]));

                                $html = "";
                                $html = '<div class="image aos-init aos-animate" data-aos="fade-left" data-aos-delay="300">';
                                $html .= '<div class="overlay">';
                                $html .= '<div class="position_wrap">';
                                $html .= '<div class="date aos-init aos-animate" data-aos="fade-left" data-aos-delay="350">'. $itemDate .'</div>';
                                $html .= '<div class="post_title aos-init aos-animate" data-aos="fade-left" data-aos-delay="400"><a href="'.str_replace("/blog/","/blog/detail.php",$CurPost["urlToEdit"]).'">Скрабы, щетки или спонжи? Что и когда лучше использовать?</a></div>';
                                $html .= '</div>';
                                $html .= '</div>';
                                $html .= '<img src="'.$arFile["SRC"].'">';
                                $html .= '</div>';

                            }
                        }
                        if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC" && $horisontal) {
                            if (!empty($arPostField["VALUE"]) && $arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC") {
                                $arFile = CFile::GetFileArray($arPostField["VALUE"]);
                                $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($CurPost["DATE_PUBLISH_DATE"]));
                                $html = "";
                                $html = '<div class="image" data-aos="fade-left" data-aos-delay="300">';
                                $html .= '<a href="'.str_replace("/blog/","/blog/detail.php",$CurPost["urlToEdit"]).'"><img src="'.$arFile["SRC"].'" alt="" class="js-tilt"></a>';
                                $html .= '</div>';
                                $html .= '<div class="date" data-aos="fade-left" data-aos-delay="350">'.$itemDate.'</div>';
                                $html .= '<div class="post_title" data-aos="fade-left" data-aos-delay="400"><a href="'.str_replace("/blog/","/blog/detail.php",$CurPost["urlToEdit"]) .'" title="'. $CurPost["TITLE"] .'">'. $CurPost["TITLE"] .'</a></div>';
                                $html .= '<div class="description" data-aos="fade-left" data-aos-delay="500">'.$CurPost["TEXT_FORMATED"].'</div>';

                            }
                        }
                        ?>
                    <?} ?>

                    <div class="item" data-aos="fade-up" data-aos-delay="400">
                        <div class="post_item <?=$class?>" data-aos="fade-left" data-aos-delay="200">
                            <?=$html?>
                        </div>
                    </div>
                    <?
                }
                if (strlen($arResult["NAV_STRING"]) > 0)
                    echo $arResult["NAV_STRING"];
            } elseif (!empty($arResult["BLOG"]))
                echo GetMessage("BLOG_BLOG_BLOG_NO_AVAIBLE_MES");
            ?>


        </div>
        <div class="center" data-aos="fade-up" data-aos-delay="400">
            <a href="#" class="ajax_loadmore"><span class="lnr lnr-arrow-down"></span>Загрузить ещё</a>
        </div>
    </div>
</section>