<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;

?>
<? CUtil::InitJSCore(array("image")); ?>

<div class="cat_list_open" data-aos="flip-up" data-aos-delay="500"><span class="lnr lnr-chevron-down"></span>Показать
    категории
</div>
<?
$arGroups = [];
foreach ($arResult["POST"] as $ind => $CurPost) {

    foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){

        if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_GROUP") {
            if (!empty($arPostField["VALUE"])) {
                $arGroups[$arPostField["VALUE"]] = $arPostField["VALUE"];
            }
        }
        ?>
    <?}
}
?>
<ul class="categories_list inversion clearfix aos-init aos-animate" data-aos="flip-up" data-aos-delay="500">
    <?foreach ($arGroups as $ind => $group_name) {?>
        <li <?if($_REQUEST["group"]==$group_name){?>class="current"<?}?>><a href="<?$APPLICATION->getCurPageParam();?>&group=<?=$group_name?>"><?=$group_name?></a></li>
    <?}?>
</ul>