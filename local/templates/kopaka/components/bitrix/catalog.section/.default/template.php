<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$templateLibrary = array('popup', 'ajax', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCS_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_FEW');

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');

$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];

if ($showTopPager)
{
	?>
	<div data-pagination-num="<?=$navParams['NavNum']?>">
		<!-- pagination-container -->
		<?=$arResult['NAV_STRING']?>
		<!-- pagination-container -->
	</div>
	<?
}
?>



                <?
                if (!empty($arResult['ITEMS']) && !empty($arResult['ITEM_ROWS']))
                {
                    $areaIds = array();

                    foreach ($arResult['ITEMS'] as $item)
                    {
                        $uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
                        $areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
                        $this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
                        $this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
                    }
                    ?>
                    <!-- Map section -->
                    <section class="section map-section">
                        <div id="map" class="map"></div>
                        <div class="menu-container">
                            <div class="item">
                                <a href="#">
                                    <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            width="28px" height="27px">
                                        <path fill-rule="evenodd"  fill="rgb(226, 230, 216)"
                                              d="M27.707,26.717 C27.323,27.101 26.701,27.101 26.317,26.717 L20.275,20.676 C18.122,22.731 15.212,24.000 12.000,24.000 C5.373,24.000 -0.000,18.627 -0.000,12.000 C-0.000,5.373 5.373,-0.000 12.000,-0.000 C18.627,-0.000 24.000,5.373 24.000,12.000 C24.000,14.706 23.093,17.194 21.582,19.202 L27.707,25.327 C28.091,25.711 28.091,26.333 27.707,26.717 ZM22.000,12.000 C22.000,6.477 17.523,2.000 12.000,2.000 C6.477,2.000 2.000,6.477 2.000,12.000 C2.000,17.523 6.477,22.000 12.000,22.000 C17.523,22.000 22.000,17.523 22.000,12.000 Z"/>
                                    </svg>
                                    <span class="text">Найти технику</span>
                                </a>
                            </div>
                            <div class="item">
                                <a href="#" class="red">
                                    <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            width="27px" height="26px">
                                        <path fill-rule="evenodd"  fill="rgb(226, 230, 216)"
                                              d="M26.000,14.000 L14.000,14.000 L14.000,25.000 C14.000,25.552 13.552,26.000 13.000,26.000 C12.448,26.000 12.000,25.552 12.000,25.000 L12.000,14.000 L1.000,14.000 C0.448,14.000 -0.000,13.552 -0.000,13.000 C-0.000,12.448 0.448,12.000 1.000,12.000 L12.000,12.000 L12.000,1.000 C12.000,0.448 12.448,-0.000 13.000,-0.000 C13.552,-0.000 14.000,0.448 14.000,1.000 L14.000,12.000 L26.000,12.000 C26.552,12.000 27.000,12.448 27.000,13.000 C27.000,13.552 26.552,14.000 26.000,14.000 Z"/>
                                    </svg>
                                    <span class="text">Предложить технику</span>
                                </a>
                            </div>
                        </div>
                    </section>
                    <!-- End of Map section -->
                <? $this->SetViewTarget('googleMapinit'); ?>
                    <script type="application/javascript">
                        function initMap() {

                            if(document.getElementById('map') === null) return false;

                            var clusterStyles = [{
                                textColor: 'white',
                                url: '<?=SITE_TEMPLATE_PATH?>/img/marker-cluster.png',
                                height: 48,
                                width: 48,
                                textSize: 18
                            }];

                            var locations = [
                                {lat: 55.7311500, lng: 37.6199100},
                                {lat: 55.7322479, lng: 37.6199252},
                                {lat: 55.7333477, lng: 37.6199362},
                                {lat: 55.7344478, lng: 37.6199472},
                                {lat: 55.7355480, lng: 37.6199582},
                                {lat: 55.7366481, lng: 37.6199662},
                                {lat: 55.7377482, lng: 37.6199762},
                                {lat: 55.7388483, lng: 37.6199862},
                                {lat: 55.7399484, lng: 37.6199962},
                                {lat: 55.7185479, lng: 37.6199461},
                                {lat: 55.7285479, lng: 37.6199463},
                                {lat: 55.7385479, lng: 37.6199465},
                                {lat: 55.7485479, lng: 37.6199466},
                                {lat: 55.7585479, lng: 37.6199467},
                                {lat: 55.7685479, lng: 37.6199468},
                                {lat: 55.7785479, lng: 37.6199470},
                                {lat: 55.7885474, lng: 37.6199465},
                                {lat: 55.796756, lng: 37.6062273},
                                {lat: 55.756757, lng: 37.6062273},
                                {lat: 55.756758, lng: 37.6062273},
                                {lat: 55.756760, lng: 37.6062273},
                                {lat: 55.756761, lng: 37.6062273},
                                {lat: 55.756766, lng: 37.6062273},
                                {lat: 55.756758, lng: 37.6062270},
                                {lat: 55.756758, lng: 37.6062260},
                                {lat: 55.756758, lng: 37.6062265},
                            ];
                            var styledMapType = new google.maps.StyledMapType(
                                [
                                    {
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#212121"
                                            }
                                        ]
                                    },
                                    {
                                        "elementType": "labels.icon",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#757575"
                                            }
                                        ]
                                    },
                                    {
                                        "elementType": "labels.text.stroke",
                                        "stylers": [
                                            {
                                                "color": "#212121"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#757575"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative.country",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#9e9e9e"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative.locality",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#bdbdbd"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "administrative.province",
                                        "stylers": [
                                            {
                                                "visibility": "off"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#757575"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi.park",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#181818"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi.park",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#616161"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "poi.park",
                                        "elementType": "labels.text.stroke",
                                        "stylers": [
                                            {
                                                "color": "#1b1b1b"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#2c2c2c"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#8a8a8a"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.arterial",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#373737"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.highway",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#3c3c3c"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.highway.controlled_access",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#4e4e4e"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "road.local",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#616161"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "transit",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#757575"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "water",
                                        "elementType": "geometry",
                                        "stylers": [
                                            {
                                                "color": "#000000"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "water",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                            {
                                                "color": "#2b2b2b"
                                            }
                                        ]
                                    },
                                    {
                                        "featureType": "water",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                            {
                                                "color": "#3d3d3d"
                                            }
                                        ]
                                    }
                                ],
                                {name: 'Styled Map'});
                            var centerPos = {lat: 55.7385479, lng: 37.6199462};

                            var map = new google.maps.Map(
                                document.getElementById('map'), {
                                    zoom: 13.5,
                                    center: centerPos,
                                    mapTypeControl: false,
                                    scaleControl: false,
                                    streetViewControl: false,
                                    rotateControl: false,
                                    fullscreenControl: false,
                                });

                            var markers = locations.map(function(location, i) {
                                return new google.maps.Marker({
                                    position: location,
                                    icon: '<?=SITE_TEMPLATE_PATH?>/img/marker-pin.png',
                                });
                            });

                            var markerCluster = new MarkerClusterer(map, markers, {
                                styles: clusterStyles
                            });

                            map.mapTypes.set('styled_map', styledMapType);
                            map.setMapTypeId('styled_map');
                        }
                    </script>
                    <script async defer src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U&callback=initMap"></script>
                <?$this->EndViewTarget();?>
                    <!-- items-container -->
                    <?
                    foreach ($arResult['ITEM_ROWS'] as $rowData)
                    {
                        $rowItems = array_splice($arResult['ITEMS'], 0, $rowData['COUNT']);

                        ?>

                            <?
                            switch ($rowData['VARIANT'])
                            {
                                case 0:
                                    ?>
                                    <div class="col-xs-12 product-item-small-card">
                                        <div class="row">
                                            <div class="col-xs-12 product-item-big-card">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <?
                                                        $item = reset($rowItems);
                                                        $APPLICATION->IncludeComponent(
                                                            'bitrix:catalog.item',
                                                            '',
                                                            array(
                                                                'RESULT' => array(
                                                                    'ITEM' => $item,
                                                                    'AREA_ID' => $areaIds[$item['ID']],
                                                                    'TYPE' => $rowData['TYPE'],
                                                                    'BIG_LABEL' => 'N',
                                                                    'BIG_DISCOUNT_PERCENT' => 'N',
                                                                    'BIG_BUTTONS' => 'N',
                                                                    'SCALABLE' => 'N'
                                                                ),
                                                                'PARAMS' => $generalParams
                                                                    + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                            ),
                                                            $component,
                                                            array('HIDE_ICONS' => 'Y')
                                                        );
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 1:
                                    ?>
                                    <div class="col-xs-12 product-item-small-card">
                                        <div class="row">
                                            <?
                                            foreach ($rowItems as $item)
                                            {
                                                ?>
                                                <div class="col-xs-6 product-item-big-card">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?
                                                            $APPLICATION->IncludeComponent(
                                                                'bitrix:catalog.item',
                                                                '',
                                                                array(
                                                                    'RESULT' => array(
                                                                        'ITEM' => $item,
                                                                        'AREA_ID' => $areaIds[$item['ID']],
                                                                        'TYPE' => $rowData['TYPE'],
                                                                        'BIG_LABEL' => 'N',
                                                                        'BIG_DISCOUNT_PERCENT' => 'N',
                                                                        'BIG_BUTTONS' => 'N',
                                                                        'SCALABLE' => 'N'
                                                                    ),
                                                                    'PARAMS' => $generalParams
                                                                        + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                                ),
                                                                $component,
                                                                array('HIDE_ICONS' => 'Y')
                                                            );
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 2:
                                    ?>
                                    <?
                                    foreach ($rowItems as $item)
                                    {
                                        ?>
                                        <?
                                        $APPLICATION->IncludeComponent(
                                            'bitrix:catalog.item',
                                            'catalog',
                                            array(
                                                'RESULT' => array(
                                                    'ITEM' => $item,
                                                    'AREA_ID' => $areaIds[$item['ID']],
                                                    'TYPE' => $rowData['TYPE'],
                                                    'BIG_LABEL' => 'N',
                                                    'BIG_DISCOUNT_PERCENT' => 'N',
                                                    'BIG_BUTTONS' => 'Y',
                                                    'SCALABLE' => 'N'
                                                ),
                                                'PARAMS' => $generalParams
                                                    + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                            ),
                                            $component,
                                            array('HIDE_ICONS' => 'Y')
                                        );
                                        ?>
                                        <?
                                    }
                                    ?>
                                    <?
                                    break;

                                case 3:
                                    ?>
                                    <div class="col-xs-12 product-item-small-card">
                                        <div class="row">
                                            <?
                                            foreach ($rowItems as $item)
                                            {
                                                ?>
                                                <div class="col-xs-6 col-md-3">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $item,
                                                                'AREA_ID' => $areaIds[$item['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 4:
                                    $rowItemsCount = count($rowItems);
                                    ?>
                                    <div class="col-sm-6 product-item-big-card">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?
                                                $item = array_shift($rowItems);
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.item',
                                                    '',
                                                    array(
                                                        'RESULT' => array(
                                                            'ITEM' => $item,
                                                            'AREA_ID' => $areaIds[$item['ID']],
                                                            'TYPE' => $rowData['TYPE'],
                                                            'BIG_LABEL' => 'N',
                                                            'BIG_DISCOUNT_PERCENT' => 'N',
                                                            'BIG_BUTTONS' => 'Y',
                                                            'SCALABLE' => 'Y'
                                                        ),
                                                        'PARAMS' => $generalParams
                                                            + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                                unset($item);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 product-item-small-card">
                                        <div class="row">
                                            <?
                                            for ($i = 0; $i < $rowItemsCount - 1; $i++)
                                            {
                                                ?>
                                                <div class="col-xs-6">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $rowItems[$i],
                                                                'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 5:
                                    $rowItemsCount = count($rowItems);
                                    ?>
                                    <div class="col-sm-6 product-item-small-card">
                                        <div class="row">
                                            <?
                                            for ($i = 0; $i < $rowItemsCount - 1; $i++)
                                            {
                                                ?>
                                                <div class="col-xs-6">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $rowItems[$i],
                                                                'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 product-item-big-card">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?
                                                $item = end($rowItems);
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.item',
                                                    '',
                                                    array(
                                                        'RESULT' => array(
                                                            'ITEM' => $item,
                                                            'AREA_ID' => $areaIds[$item['ID']],
                                                            'TYPE' => $rowData['TYPE'],
                                                            'BIG_LABEL' => 'N',
                                                            'BIG_DISCOUNT_PERCENT' => 'N',
                                                            'BIG_BUTTONS' => 'Y',
                                                            'SCALABLE' => 'Y'
                                                        ),
                                                        'PARAMS' => $generalParams
                                                            + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                                unset($item);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 6:
                                    ?>
                                    <div class="col-xs-12 product-item-small-card">
                                        <div class="row">
                                            <?
                                            foreach ($rowItems as $item)
                                            {
                                                ?>
                                                <div class="col-xs-6 col-sm-4 col-md-2">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $item,
                                                                'AREA_ID' => $areaIds[$item['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 7:
                                    $rowItemsCount = count($rowItems);
                                    ?>
                                    <div class="col-sm-6 product-item-big-card">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?
                                                $item = array_shift($rowItems);
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.item',
                                                    '',
                                                    array(
                                                        'RESULT' => array(
                                                            'ITEM' => $item,
                                                            'AREA_ID' => $areaIds[$item['ID']],
                                                            'TYPE' => $rowData['TYPE'],
                                                            'BIG_LABEL' => 'N',
                                                            'BIG_DISCOUNT_PERCENT' => 'N',
                                                            'BIG_BUTTONS' => 'Y',
                                                            'SCALABLE' => 'Y'
                                                        ),
                                                        'PARAMS' => $generalParams
                                                            + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                                unset($item);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 product-item-small-card">
                                        <div class="row">
                                            <?
                                            for ($i = 0; $i < $rowItemsCount - 1; $i++)
                                            {
                                                ?>
                                                <div class="col-xs-6 col-md-4">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $rowItems[$i],
                                                                'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 8:
                                    $rowItemsCount = count($rowItems);
                                    ?>
                                    <div class="col-sm-6 product-item-small-card">
                                        <div class="row">
                                            <?
                                            for ($i = 0; $i < $rowItemsCount - 1; $i++)
                                            {
                                                ?>
                                                <div class="col-xs-6 col-md-4">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $rowItems[$i],
                                                                'AREA_ID' => $areaIds[$rowItems[$i]['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N',
                                                                'SCALABLE' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$rowItems[$i]['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 product-item-big-card">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?
                                                $item = end($rowItems);
                                                $APPLICATION->IncludeComponent(
                                                    'bitrix:catalog.item',
                                                    '',
                                                    array(
                                                        'RESULT' => array(
                                                            'ITEM' => $item,
                                                            'AREA_ID' => $areaIds[$item['ID']],
                                                            'TYPE' => $rowData['TYPE'],
                                                            'BIG_LABEL' => 'N',
                                                            'BIG_DISCOUNT_PERCENT' => 'N',
                                                            'BIG_BUTTONS' => 'Y',
                                                            'SCALABLE' => 'Y'
                                                        ),
                                                        'PARAMS' => $generalParams
                                                            + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                    ),
                                                    $component,
                                                    array('HIDE_ICONS' => 'Y')
                                                );
                                                unset($item);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                    break;

                                case 9:
                                    ?>
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <?
                                            foreach ($rowItems as $item)
                                            {
                                                ?>
                                                <div class="col-xs-12 product-item-line-card">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:catalog.item',
                                                        '',
                                                        array(
                                                            'RESULT' => array(
                                                                'ITEM' => $item,
                                                                'AREA_ID' => $areaIds[$item['ID']],
                                                                'TYPE' => $rowData['TYPE'],
                                                                'BIG_LABEL' => 'N',
                                                                'BIG_DISCOUNT_PERCENT' => 'N',
                                                                'BIG_BUTTONS' => 'N'
                                                            ),
                                                            'PARAMS' => $generalParams
                                                                + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <?
                                    break;
                            }
                            ?>

                        <?
                    }
                    unset($generalParams, $rowItems);
                    ?>
                    <!-- items-container -->
                    <?
                }
                else
                {
                    // load css for bigData/deferred load
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.item',
                        '',
                        array(),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                }


                if ($showLazyLoad)
                {
                    ?>
                    <div class="row bx-<?=$arParams['TEMPLATE_THEME']?>">
                        <div class="btn btn-default btn-lg center-block" style="margin: 15px;"
                            data-use="show-more-<?=$navParams['NavNum']?>">
                            <?=$arParams['MESS_BTN_LAZY_LOAD']?>
                        </div>
                    </div>
                    <?
                }

                $signer = new \Bitrix\Main\Security\Sign\Signer;
                $signedTemplate = $signer->sign($templateName, 'catalog.section');
                $signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
                ?>
                <script>
                    BX.message({
                        BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
                        BASKET_URL: '<?=$arParams['BASKET_URL']?>',
                        ADD_TO_BASKET_OK: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
                        TITLE_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR')?>',
                        TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS')?>',
                        TITLE_SUCCESSFUL: '<?=GetMessageJS('ADD_TO_BASKET_OK')?>',
                        BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR')?>',
                        BTN_MESSAGE_SEND_PROPS: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS')?>',
                        BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE')?>',
                        BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
                        COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK')?>',
                        COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
                        COMPARE_TITLE: '<?=GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE')?>',
                        PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCS_CATALOG_PRICE_TOTAL_PREFIX')?>',
                        RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
                        RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
                        BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
                        BTN_MESSAGE_LAZY_LOAD: '<?=CUtil::JSEscape($arParams['MESS_BTN_LAZY_LOAD'])?>',
                        BTN_MESSAGE_LAZY_LOAD_WAITER: '<?=GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER')?>',
                        SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
                    });
                    var <?=$obName?> = new JCCatalogSectionComponent({
                        siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
                        componentPath: '<?=CUtil::JSEscape($componentPath)?>',
                        navParams: <?=CUtil::PhpToJSObject($navParams)?>,
                        deferredLoad: false, // enable it for deferred load
                        initiallyShowHeader: '<?=!empty($arResult['ITEM_ROWS'])?>',
                        bigData: <?=CUtil::PhpToJSObject($arResult['BIG_DATA'])?>,
                        lazyLoad: !!'<?=$showLazyLoad?>',
                        loadOnScroll: !!'<?=($arParams['LOAD_ON_SCROLL'] === 'Y')?>',
                        template: '<?=CUtil::JSEscape($signedTemplate)?>',
                        ajaxId: '<?=CUtil::JSEscape($arParams['AJAX_ID'])?>',
                        parameters: '<?=CUtil::JSEscape($signedParams)?>',
                        container: '<?=$containerName?>'
                    });
                </script>

                </div>

                <?if ($showBottomPager)
                {
                    ?>
                    <div data-pagination-num="<?=$navParams['NavNum']?>">
                        <!-- pagination-container -->
                        <?=$arResult['NAV_STRING']?>
                        <!-- pagination-container -->
                    </div>
                    <?
                }?>
<?if(!$arParams["SEARCH"]) { ?>
    </div>
</section>
    <? } ?>

<!-- Category Description -->
<? $pic = CFile::GetPAth($arResult["PICTURE"]["ID"]); ?>
<?if($arResult["NAME"] && $pic && $arResult["DESCRIPTION"]) { ?>
<section class="category_description">
    <div class="container">
        <div class="row">
            <? if($pic) { ?>
            <div class="col-4">
                <img src="<?=$pic?>" alt="">
            </div>
            <? } ?>
            <div class="col-8">
                <h4><?=$arResult["NAME"]?></h4>
                <?=$arResult["DESCRIPTION"]?>
            </div>
            <div class="col-12">
                <div class="devider_line"></div>
            </div>
        </div>
    </div>
</section>
<? } ?>