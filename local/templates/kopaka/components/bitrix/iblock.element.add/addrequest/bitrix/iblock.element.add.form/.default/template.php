<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
//dump($arResult["PROPERTY_LIST"]);
//foreach ($arResult["PROPERTY_LIST"] as $propertyID) {
//}
if (!empty($arResult["ERRORS"])):?>
    <?
    ShowError(implode("<br />", $arResult["ERRORS"])) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
    <? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>

<form class="form form-add-vehicle" name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <h2><?= GetMessage("CT_BIEAF_PROPERTY_VALUE_REQUEST") ?></h2>
    <? if ($arParams["MAX_FILE_SIZE"] > 0): ?>
        <input type="hidden" name="MAX_FILE_SIZE" value="<?= $arParams["MAX_FILE_SIZE"] ?>" />
    <? endif ?>
    <div class="data-table">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>

            <div class="form-group-row <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>type-2<? } ?>">

            <? foreach ($arResult["PROPERTY_LIST"] as $propertyID):

                    if (intval($propertyID) > 0) {
                        if (
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                        elseif (
                            (
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                ||
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                            )
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                    } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
                        $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                        $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                    } else {
                        $inputNum = 1;
                    }

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                        $INPUT_TYPE = "USER_TYPE";
                    else
                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                    switch ($INPUT_TYPE):
                        case "USER_TYPE":
                            for ($i = 0; $i < $inputNum; $i++) {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                                    $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                                } elseif ($i == 0) {
                                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                    $description = "";
                                } else {
                                    $value = "";
                                    $description = "";
                                }
                                ?>
                                <?
                                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                                    <div class="styled-checkbox type-1">
                                    <div class="checkbox-group">
                                <? } else { ?>
                                    <div class="form-group select-group tablet-half">
                                <? } ?><?
                                echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
                                    array(
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID],
                                        array(
                                            "VALUE" => $value,
                                            "DESCRIPTION" => $description,
                                        ),
                                        array(
                                            "VALUE" => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
                                            "DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
                                            "FORM_NAME" => "iblock_add",
                                        ),
                                    ));
                                ?>
                                <?
                                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                                    <label for="<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 70 70" style="enable-background:new 0 0 70 70;"
                                             xml:space="preserve">
                  <path d="M26.474,70c-2.176,0-4.234-1.018-5.557-2.764L3.049,43.639C0.725,40.57,1.33,36.2,4.399,33.875
                    c3.074-2.326,7.441-1.717,9.766,1.35l11.752,15.518L55.474,3.285c2.035-3.265,6.332-4.264,9.604-2.232
                    c3.268,2.034,4.266,6.334,2.23,9.602l-34.916,56.06c-1.213,1.949-3.307,3.175-5.6,3.279C26.685,69.998,26.58,70,26.474,70z"/>
                </svg>
                                    </label>
                                <? } ?>

                                <script type="text/javascript">
                                    <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                                    $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                                    $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]_Y"; ?>');
                                    <? } else { ?>

                                    <? if (intval($propertyID) > 0) {  ?>
                                    $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                                    $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>');
                                    <? } ?>
                                    $("select[name='<? echo "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]"; ?>']").addClass("styled-select");

                                    <? } ?>
                                </script>
                                <?
                                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "SASDCheckbox") { ?>
                                    </div><span><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></span>
                                    </div>
                                <? } else { ?>
                                    </div>
                                <? } ?>
                                <?
                            }
                            break;
                        case "TAGS":
                            $APPLICATION->IncludeComponent(
                                "bitrix:search.tags.input",
                                "",
                                array(
                                    "VALUE" => $arResult["ELEMENT"][$propertyID],
                                    "NAME" => "PROPERTY[" . $propertyID . "][0]",
                                    "TEXT" => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
                                ), null, array("HIDE_ICONS" => "Y")
                            );
                            break;
                        case "HTML":
                            $LHE = new CHTMLEditor;
                            $LHE->Show(array(
                                'name' => "PROPERTY[" . $propertyID . "][0]",
                                'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
                                'inputName' => "PROPERTY[" . $propertyID . "][0]",
                                'content' => $arResult["ELEMENT"][$propertyID],
                                'width' => '100%',
                                'minBodyWidth' => 350,
                                'normalBodyWidth' => 555,
                                'height' => '200',
                                'bAllowPhp' => false,
                                'limitPhpAccess' => false,
                                'autoResize' => true,
                                'autoResizeOffset' => 40,
                                'useFileDialogs' => false,
                                'saveOnBlur' => true,
                                'showTaskbars' => false,
                                'showNodeNavi' => false,
                                'askBeforeUnloadPage' => true,
                                'bbCode' => false,
                                'siteId' => SITE_ID,
                                'controlsMap' => array(
                                    array('id' => 'Bold', 'compact' => true, 'sort' => 80),
                                    array('id' => 'Italic', 'compact' => true, 'sort' => 90),
                                    array('id' => 'Underline', 'compact' => true, 'sort' => 100),
                                    array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
                                    array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
                                    array('id' => 'Color', 'compact' => true, 'sort' => 130),
                                    array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
                                    array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
                                    array('separator' => true, 'compact' => false, 'sort' => 145),
                                    array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
                                    array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
                                    array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
                                    array('separator' => true, 'compact' => false, 'sort' => 200),
                                    array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
                                    array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
                                    array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
                                    array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
                                    array('separator' => true, 'compact' => false, 'sort' => 290),
                                    array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
                                    array('id' => 'More', 'compact' => true, 'sort' => 400)
                                ),
                            ));
                            break;
                        case "T":
                            for ($i = 0; $i < $inputNum; $i++) {

                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                } elseif ($i == 0) {
                                    $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                } else {
                                    $value = "";
                                }
                                ?>
                                <textarea cols="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                          rows="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] ?>"
                                          name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
                                <?
                            }
                            break;

                        case "S":
                        case "N":
                            for ($i = 0; $i < $inputNum; $i++) {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                } elseif ($i == 0) {
                                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                                } else {
                                    $value = "";
                                }
                                ?>
                                <div class="form-group">
                                <input type="text" class="form-control" id="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                       name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                       size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>"
                                       value="<?= $value ?>"  <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?>/><?
                                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime1"):?><?
                                    $APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => 'iblock_add',
                                            'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
                                            'INPUT_VALUE' => $value,
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );
                                    ?>
                                    <small><?= GetMessage("IBLOCK_FORM_DATE_FORMAT") ?><?= FORMAT_DATETIME ?></small><?
                                endif;

                                ?>
                                <? if (intval($propertyID) > 0) { ?>
                                    <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?></label>
                                <? } else { ?>
                                    <label for="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?></label>
                                <? } ?>

                                <script type="text/javascript">
                                    <? if (intval($propertyID) > 0): ?>
                                    $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("data-select-placeholder", "<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>");
                                    $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").attr("id", '<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>');
                                    <? endif ?>

                                    $("input[id='<? echo "PROPERTY[" . $propertyID . "][" . $i . "]"; ?>']").addClass("styled-select");
                                </script>
                                </div><?
                            }
                            break;

                        case "F":
                            for ($i = 0; $i < $inputNum; $i++) {
                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                ?>
                                <input type="hidden"
                                       name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                       value="<?= $value ?>"/>
                                <input type="file"
                                       size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                       name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>
                                <?

                                if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
                                    ?>
                                    <input type="checkbox"
                                           name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                           id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y" <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> /><label
                                            for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
                                    <?

                                    if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
                                        ?>
                                        <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"
                                             height="<?= $arResult["ELEMENT_FILES"][$value]["HEIGHT"] ?>"
                                             width="<?= $arResult["ELEMENT_FILES"][$value]["WIDTH"] ?>" border="0"/>
                                        <?
                                    } else {
                                        ?>
                                        <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
                                        <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?> b
                                        [
                                        <a href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
                                        <?
                                    }
                                }
                            }

                            break;
                        case "L":

                            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                            else
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                            switch ($type):
                                case "checkbox":
                                case "radio":
                                    foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                                        $checked = false;
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                            if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
                                                foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                                                    if ($arElEnum["VALUE"] == $key) {
                                                        $checked = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($arEnum["DEF"] == "Y") $checked = true;
                                        }

                                        ?>
                                        <input type="<?= $type ?>"
                                               name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
                                               value="<?= $key ?>"
                                               id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?> <? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<? endif ?> />
                                        <label for="property_<?= $key ?>"><?= $arEnum["VALUE"] ?></label>
                                        <?
                                    }
                                    break;

                                case "dropdown":
                                case "multiselect":
                                    ?>
                                    <div class="form-group select-group tablet-half">
                                        <select id="PROPERTY[<?= $propertyID ?>]"
                                                data-select-placeholder="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?>"
                                                class="styled-select"
                                                name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
                                            <option value=""><?
                                                echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?></option>
                                            <?
                                            if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                                            else $sKey = "ELEMENT";

                                            foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                                                $checked = false;
                                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                                    foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                                        if ($key == $arElEnum["VALUE"]) {
                                                            $checked = true;
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                                }
                                                ?>
                                                <option value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?
                                    break;

                            endswitch;
                            break;
                    endswitch;
            endforeach; ?>

            </div>
            <? if ($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0): ?>
                <div class="form-group-row">
                    <?= GetMessage("IBLOCK_FORM_CAPTCHA_TITLE") ?>
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                </div>
                <div class="form-group-row">
                    <?= GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT") ?><span class="starrequired">*</span>:
                    <input type="text" name="captcha_word" maxlength="50" value="">
                </div>
            <? endif ?>

        <? endif ?>
        <div class="button-container">
            <button class="btn btn-primary add_request">
                <span><?= GetMessage("IBLOCK_FORM_SUBMIT") ?></span>
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="474.8px" height="474.801px" viewBox="0 0 474.8 474.801" style="enable-background:new 0 0 474.8 474.801;" xml:space="preserve">
              <path d="M396.283,257.097c-1.14-0.575-2.282-0.862-3.433-0.862c-2.478,0-4.661,0.951-6.563,2.857l-18.274,18.271
                c-1.708,1.715-2.566,3.806-2.566,6.283v72.513c0,12.565-4.463,23.314-13.415,32.264c-8.945,8.945-19.701,13.418-32.264,13.418
                H82.226c-12.564,0-23.319-4.473-32.264-13.418c-8.947-8.949-13.418-19.698-13.418-32.264V118.622
                c0-12.562,4.471-23.316,13.418-32.264c8.945-8.946,19.7-13.418,32.264-13.418H319.77c4.188,0,8.47,0.571,12.847,1.714
                c1.143,0.378,1.999,0.571,2.563,0.571c2.478,0,4.668-0.949,6.57-2.852l13.99-13.99c2.282-2.281,3.142-5.043,2.566-8.276
                c-0.571-3.046-2.286-5.236-5.141-6.567c-10.272-4.752-21.412-7.139-33.403-7.139H82.226c-22.65,0-42.018,8.042-58.102,24.126
                C8.042,76.613,0,95.978,0,118.629v237.543c0,22.647,8.042,42.014,24.125,58.098c16.084,16.088,35.452,24.13,58.102,24.13h237.541
                c22.647,0,42.017-8.042,58.101-24.13c16.085-16.084,24.134-35.45,24.134-58.098v-90.797
                C402.001,261.381,400.088,258.623,396.283,257.097z"></path>
                    <path d="M467.95,93.216l-31.409-31.409c-4.568-4.567-9.996-6.851-16.279-6.851c-6.275,0-11.707,2.284-16.271,6.851
                L219.265,246.532l-75.084-75.089c-4.569-4.57-9.995-6.851-16.274-6.851c-6.28,0-11.704,2.281-16.274,6.851l-31.405,31.405
                c-4.568,4.568-6.854,9.994-6.854,16.277c0,6.28,2.286,11.704,6.854,16.274l122.767,122.767c4.569,4.571,9.995,6.851,16.274,6.851
                c6.279,0,11.704-2.279,16.274-6.851l232.404-232.403c4.565-4.567,6.854-9.994,6.854-16.274S472.518,97.783,467.95,93.216z"></path>
            </svg>
            </button>
            <script type="text/javascript">
                $(document).ready(function(){
                    $(document).on("click","button.add_request",function(e){
                        e.stopPropagation();
                        e.preventDefault();
                        $("input[name='iblock_submit']").click();
                        return false;
                    });
                });
            </script>

            <input type="submit" name="iblock_submit" style="display:none;" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
            <? if (strlen($arParams["LIST_URL"]) > 0): ?>
                <input style="display:none;" type="submit" name="iblock_apply" value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
                <input style="display:none;" type="button" name="iblock_cancel" value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                       onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';">
            <? endif ?>
        </div>

    </div>
</form>