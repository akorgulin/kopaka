<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>

<div id="enter" class="popup center animated-modal reg-modal" style="display: none;">
    <div class="logo_header aos-init aos-animate" data-aos="flip-left" data-aos-delay="500"><a href="#">Fijie</a></div>

    <h5>Войти на сайт</h5>
    <div class="registery">или <a href="<?=$arParams["REGISTER_URL"]?>">зарегистрироваться</a></div>

    <div class="forms-grid">

        <?
        if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
            ShowMessage($arResult['ERROR_MESSAGE']);
        ?>

        <?if($arResult["FORM_TYPE"] == "login") {?>

        <form name="system_auth_form" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
            <? if ($arResult["BACKURL"] <> '') { ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
            <? } ?>
            <? foreach ($arResult["POST"] as $key => $value) { ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
            <? } ?>
            <input type="hidden" name="AUTH_FORM" value="Y"/>
            <input type="hidden" name="TYPE" value="AUTH"/>


            <div class="forms-col-12">
                <div class="border-form">
                    <div class="without_icon_input">
                        <div class="icon"><span class="lnr lnr-user"></span></div>
                        <input type="text" placeholder="<?= Loc::getMessage("AUTH_LOGIN") ?>" name="USER_LOGIN"
                               maxlength="50" value="<?= $arResult["USER_LOGIN"] ?>" size="17">
                    </div>
                </div>
            </div>
            <div class="forms-col-12">
                <div class="border-form">
                    <div class="without_icon_input">
                        <div class="icon"><span class="lnr lnr-lock"></span></div>
                        <input type="password" name="USER_PASSWORD" maxlength="50" size="17"
                               placeholder="<?= Loc::getMessage("AUTH_PASSWORD") ?>">
                    </div>
                </div>
            </div>

            <? if ($arResult["SECURE_AUTH"]) { ?>
                <div class="forms-col-12">
                    <div class="border-form">
                        <div class="without_icon_input">
                            <div class="icon"><span class="lnr lnr-lock"></span></div>
                            <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>"
                                  title="<? echo Loc::getMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                                <div class="bx-auth-secure-icon"></div>
                            </span>
                            <noscript>
                            <span class="bx-auth-secure" title="<? echo Loc::getMessage("AUTH_NONSECURE_NOTE") ?>">
                                <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                            </span>
                            </noscript>
                            <script type="text/javascript">
                                document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                            </script>
                        </div>
                    </div>
                </div>
            <? } ?>

            <? if ($arResult["CAPTCHA_CODE"]) { ?>
                <div class="forms-col-12">
                    <div class="border-form">
                        <div class="without_icon_input">
                            <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>"
                                 width="180" height="40" alt="CAPTCHA"/><br/><br/>
                            <input type="text" name="captcha_word" maxlength="50" value=""/>
                        </div>
                    </div>
                </div>
            <? } ?>

            <? if ($arResult["AUTH_SERVICES"] && 1==0) { ?>
                <div class="forms-col-12">
                    <div class="border-form">
                        <div class="without_icon_input">
                            <div class="bx-auth-lbl"><?= Loc::getMessage("socserv_as_user_form") ?></div>
                            <?
                            $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
                                array(
                                    "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                                    "SUFFIX" => "form",
                                ),
                                $component,
                                array("HIDE_ICONS" => "Y")
                            );
                            ?>
                        </div>
                    </div>
                </div>
            <? } ?>

            <? if ($arResult["NEW_USER_REGISTRATION"] == "Y" && 1==0) { ?>
                <noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>"
                            rel="nofollow"><?= Loc::getMessage("AUTH_REGISTER") ?></a></noindex><br/>
            <? } ?>

            <div class="forms-col-12 left">
                <div class="border-form">
                    <? if ($arResult["STORE_PASSWORD"] == "Y") { ?>
                        <label class="checkbox_line">
                            <input type="checkbox" placeholder="<?= Loc::getMessage("AUTH_REMEMBER_ME") ?>"
                                   title="<?= Loc::getMessage("AUTH_REMEMBER_ME") ?>" id="USER_REMEMBER_frm"
                                   name="USER_REMEMBER" value="Y"/>
                            Запомнить меня на этом компьютере
                        </label>
                    <? } ?>
                    <div class="enter_area">
                        <div class="enter_button">
                            <input type="submit" name="Login" value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?>"
                                   style="display:none;"/>
                            <input type="hidden" name="Login" value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?>">
                            <a href="#Login" onclick="javascript:$('form[name=system_auth_form]').submit();"
                               class="btn mint">Войти</a>
                        </div>
                        <div class="enter_info">
                            <span>Забыли свой пароль?</span>
                            <span>Мы поможем его <noindex><a href="<?= $arParams["FORGOT_URL"] ?>"
                                                             rel="nofollow"><?= Loc::getMessage("AUTH_FORGOT_PASSWORD_2") ?></a></noindex>.</span>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?if($arResult["AUTH_SERVICES"]):?>
        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                "AUTH_URL" => $arResult["AUTH_URL"],
                "POST" => $arResult["POST"],
                "POPUP" => "Y",
                "SUFFIX" => "form",
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
        <?endif?>

        <?
        } else if($arResult["FORM_TYPE"] == "otp") {
            ?>
            <form name="system_auth_form" method="post" target="_top"  action="<?= $arResult["AUTH_URL"] ?>">
                <?
                if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <?endif ?>
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="OTP"/>

                <div class="forms-col-12">
                    <div class="border-form">
                        <div class="without_icon_input">
                            <div class="icon"><span class="lnr lnr-user"></span></div>
                            <input placeholder="<?echo Loc::getMessage("auth_form_comp_otp") ?>" type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off"/>
                        </div>
                    </div>
                </div>

                <? if ($arResult["CAPTCHA_CODE"]) { ?>
                <div class="forms-col-12">
                    <div class="border-form">
                        <div class="without_icon_input">
                            <div class="icon"><span class="lnr lnr-lock"></span></div>
                            <?echo Loc::getMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
                            <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"] ?>"/>
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                            <input type="text" name="captcha_word" maxlength="50" value=""/>
                        </div>
                    </div>
                </div>
                <? }?>

                <div class="forms-col-12 left">
                    <div class="border-form">
                        <? if ($arResult["REMEMBER_OTP"] == "Y") {?>
                            <label class="checkbox_line">
                                <input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER"
                                       value="Y"/>
                                <? echo Loc::getMessage("auth_form_comp_otp_remember") ?>
                            </label>
                        <? } ?>
                        <div class="enter_area">
                            <div class="enter_button">
                                <input type="submit" name="Login"
                                       value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?>" style="display:none;"/>
                                <input type="hidden" name="Login" value="<?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?>">
                                <a href="#Login" onclick="javascript:$('form[name=system_auth_form]').submit();"
                                   class="btn mint"><?= Loc::getMessage("AUTH_LOGIN_BUTTON") ?></a>
                            </div>
                            <div class="enter_info">
                                <span>Забыли свой пароль?</span>
                                <span>Мы поможем его <noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
                                                                 rel="nofollow"><?= Loc::getMessage("AUTH_FORGOT_PASSWORD_2") ?></a></noindex>.</span>
                            </div>
                        </div>
                    </div>
            </form>

            <?
        }
        ?>
    </div>
</div>
