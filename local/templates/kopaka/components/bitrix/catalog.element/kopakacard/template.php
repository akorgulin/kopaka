<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(false);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'PAKET_ID' => $mainId.'_buy_packet',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

?>
<section class="section content-section">
    <div id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
        <div class="container">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            );?>
        </div>

        <div class="container" id="<?=$itemIds['ID']?>">
            <div class="vehicle-container">
                <div class="vehicle-images-container">
                    <div class="images-counter">
                        <div class="counter-icon">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="38px" height="33px">
                                <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
                                      d="M36.515,31.513 C35.525,32.505 34.331,33.001 32.933,33.001 L5.068,33.001 C3.669,33.001 2.475,32.505 1.485,31.513 C0.496,30.522 0.001,29.325 0.001,27.924 L0.001,10.153 C0.001,8.752 0.496,7.555 1.485,6.564 C2.475,5.572 3.669,5.076 5.068,5.076 L9.501,5.076 L10.510,2.379 C10.761,1.731 11.219,1.172 11.885,0.703 C12.552,0.233 13.234,-0.001 13.934,-0.001 L24.067,-0.001 C24.766,-0.001 25.449,0.233 26.115,0.703 C26.782,1.172 27.240,1.731 27.491,2.379 L28.500,5.076 L32.933,5.076 C34.332,5.076 35.525,5.572 36.515,6.564 C37.504,7.555 37.999,8.752 37.999,10.153 L37.999,27.924 C37.999,29.325 37.504,30.522 36.515,31.513 ZM25.264,12.762 C23.529,11.023 21.441,10.154 19.000,10.154 C16.559,10.154 14.471,11.023 12.736,12.762 C11.002,14.501 10.134,16.593 10.134,19.039 C10.134,21.485 11.001,23.577 12.736,25.316 C14.472,27.055 16.559,27.924 19.000,27.924 C21.441,27.924 23.529,27.055 25.264,25.316 C26.999,23.577 27.866,21.485 27.866,19.039 C27.866,16.593 26.999,14.500 25.264,12.762 ZM19.000,24.751 C17.430,24.751 16.088,24.192 14.973,23.075 C13.858,21.957 13.300,20.612 13.300,19.039 C13.300,17.465 13.858,16.120 14.973,15.003 C16.088,13.885 17.430,13.327 19.000,13.327 C20.570,13.327 21.913,13.885 23.028,15.003 C24.142,16.120 24.700,17.465 24.700,19.039 C24.700,20.612 24.142,21.957 23.028,23.075 C21.913,24.192 20.570,24.751 19.000,24.751 Z"/>
                            </svg>
                        </div>
                        <span class="value">20</span>
                    </div>
					<div id="<?=$itemIds['BIG_SLIDER_ID']?>" data-entity="images-container">
                    	<div id="big" class="vehicles-main-view-carousel owl-carousel" data-entity="images-slider-block">
                        <?
                        if (!empty($actualItem['MORE_PHOTO']))
                        {
                            foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                            {

                                ?>
                                <a class="item" data-fancybox="gallery" href="<?=$photo['SRC']?>">
                                    <img src="<?=$photo['SRC']?>" alt="">
                                </a>
                                <?
                            }
                        }
                        ?>
                    	</div>
					</div>
                    <div id="thumbs" class="vehicles-thumbs-view-carousel owl-carousel owl-theme">
                        <?
                        if (!empty($actualItem['MORE_PHOTO']))
                        {
                            foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                            {
                                ?>
                                <div class="item">
                                    <img src="<?=$photo['SRC']?>">
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="vehicle-info">
					<div class="item_quantity hidden">
								<div class="product-item-detail-info-container-title"><?=Loc::getMessage('CATALOG_QUANTITY')?></div>
								<div class="quantity buttons_added">
                                     <input type="button" value="-" class="minus">
									<input type="number" step="1" min="1" max="" name="quantity" id="<?=$itemIds['QUANTITY_ID']?>" value="<?=$price['MIN_QUANTITY']?>" title="Qty" class="input-text qty text" size="4" pattern="" inputmode="">
									<input type="button" value="+" class="plus">
								</div>
								<span class="product-item-amount-description-container">
										<span id="<?=$itemIds['QUANTITY_MEASURE']?>">
											<?=$actualItem['ITEM_MEASURE']['TITLE']?>
										</span>
										<span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
								</span>
							</div>
					<div class="item_quantity product-item-detail-info-container hidden" style="<?=(!$actualItem['CAN_BUY'] ? 'display: none;' : '')?>"  data-entity="quantity-block">
                                        <div class="quantity buttons_added">
                                            <div class="product-item-detail-info-container-title"><?=Loc::getMessage('CATALOG_QUANTITY')?></div>
                                            <div class="product-item-amount">
                                                <div class="product-item-amount-field-container">
                                                    <span class="product-item-amount-field-btn-minus no-select" id="<?=$itemIds['QUANTITY_DOWN_ID']?>"></span>
                                                    <input class="product-item-amount-field" id="<?=$itemIds['QUANTITY_ID']?>" type="number"
                                                           value="<?=$price['MIN_QUANTITY']?>">
                                                    <span class="product-item-amount-field-btn-plus no-select" id="<?=$itemIds['QUANTITY_UP_ID']?>"></span>
                                                    <span class="product-item-amount-description-container">
                                                    <span id="<?=$itemIds['QUANTITY_MEASURE']?>">
                                                        <?=$actualItem['ITEM_MEASURE']['TITLE']?>
                                                    </span>
                                                    <span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                             </div>
                    <?
                    if ($arParams['DISPLAY_NAME'] === 'Y')
                    {
                        ?>
                        <h1><?=$name?></h1>
                        <?
                    }

                    if($arResult["PROPERTIES"]["TYPE_WORK"]["VALUE"]) {?>
                    <h4><?=$arResult["PROPERTIES"]["TYPE_WORK"]["VALUE"]?></h4>
                    <? }?>
                    <div class="vehicle-characteristics">
                        <? if (!empty($arResult['PROPERTIES']) )
                        {
                            if (!empty($arResult['PROPERTIES']))
                            {

                                foreach ($arResult['PROPERTIES'] as $code=>$property)
                                {

                                    if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']]))
                                    {
                                        ?>
                                        <div class="item">
                                            <span class="title"><?=$property["NAME"]?></span>
                                            <span class="divider"></span>
                                            <span class="value"><?=(is_array($property['VALUE'])
                                                    ? implode(' / ', $property['VALUE'])
                                                    : $property['VALUE'])?></span>
                                        </div>
                                        <?
                                    }
                                }
                                unset($property);
                            }
                        }
                        ?>
                    </div>
                    <div class="vehicle-map">
                        <div class="map" id="map"></div>
                    </div>
                    <?if($arResult["PROPERTIES"]["ADRESSWORK"]["VALUE"]) { ?>
                    <div class="vehicle-location">
                        <span class="title">Местонахождение:</span>
                        <div class="location">
                            <span class="address"><?=$arResult["PROPERTIES"]["ADRESSWORK"]["VALUE"]?></span>
                            <a target="_blank" href="https://www.google.ru/maps/place/<?=$arResult["PROPERTIES"]["ADRESSWORK"]["VALUE"]?>/@<?=$arResult["PROPERTIES"]["LAT"]["VALUE"]?>,<?=$arResult["PROPERTIES"]["LNG"]["VALUE"]?>z/data=<?=$arResult["PROPERTIES"]["LAT"]["VALUE"]?>!4d<?=$arResult["PROPERTIES"]["LNG"]["VALUE"]?>" class="location-link">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="15px" height="19px">
                                    <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
                                          d="M7.233,0.004 C3.497,0.129 0.394,2.920 0.036,6.481 C-0.038,7.193 0.005,7.884 0.136,8.544 L0.136,8.544 C0.136,8.544 0.148,8.622 0.186,8.769 C0.302,9.264 0.475,9.744 0.691,10.194 C1.443,11.899 3.181,14.752 7.082,17.856 C7.321,18.048 7.672,18.048 7.915,17.856 C11.816,14.756 13.554,11.903 14.310,10.190 C14.530,9.740 14.699,9.264 14.815,8.766 C14.850,8.622 14.865,8.540 14.865,8.540 L14.865,8.540 C14.954,8.098 15.000,7.644 15.000,7.179 C15.000,3.126 11.496,-0.140 7.233,0.004 ZM7.499,10.751 C5.486,10.751 3.856,9.190 3.856,7.264 C3.856,5.337 5.486,3.776 7.499,3.776 C9.511,3.776 11.141,5.337 11.141,7.264 C11.141,9.190 9.511,10.751 7.499,10.751 Z"/>
                                </svg>
                                <span>Показать на карте</span>
                            </a>
                        </div>
                    </div>
                    <? } ?>
                    <div class="vehicle-price-container">
                        <div class="vehicle-price">
                            <?if($arResult["PROPERTIES"]["PRICEHOUR"]["VALUE"]) { ?>
                            <div class="item">
                                <span class="title">Цена за час, Р:</span>
                                <span class="price" id="<?=$itemIds['OLD_PRICE_ID']?>"><?=CurrencyFormat($arResult["PROPERTIES"]["PRICEHOUR"]["VALUE"], 'RUB')?><?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?></span>
                            </div>
                            <? } ?>
                            <?if($price['PRINT_RATIO_PRICE']) { ?>
                            <div class="item">
                                <span class="title">Цена за смену:</span>
                                <span class="price" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></span>
                            </div>
                            <? } ?>
                        </div>
                        <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
                            <?

                            if ($showAddBtn)
                            {
                                ?>
                                <div class="buttons-container">
                                    <a href="javascript:void(0);" class="btn btn-primary <?=$showButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['ADD_BASKET_LINK']?>">
                                        <span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 491.86 491.86" style="enable-background:new 0 0 491.86 491.86;" xml:space="preserve">
              <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69
                C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69
                s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
          </svg>
                                    </a>
                                </div>
                                <?
                            }

                            if ($showBuyBtn)
                            {
                                ?>
                                <div class="buttons-container">
                                    <a href="javascript:void(0);" class="btn btn-primary <?=$buyButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['BUY_LINK']?>">
                                        <span><?=$arParams['MESS_BTN_BUY']?></span>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 491.86 491.86" style="enable-background:new 0 0 491.86 491.86;" xml:space="preserve">
              <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69
                C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69
                s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
          </svg>
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <?
            $APPLICATION->IncludeComponent(
                'bitrix:iblock.vote',
                'stars',
                array(
                    'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'ELEMENT_ID' => $arResult['ID'],
                    'ELEMENT_CODE' => '',
                    'MAX_VOTE' => '5',
                    'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                    'SET_STATUS_404' => 'N',
                    'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                    'CACHE_TIME' => $arParams['CACHE_TIME']
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            );
            ?><br><br><br><br>
            <?

            $componentCommentsParams = array(
                'ELEMENT_ID' => $arResult['ID'],
                'ELEMENT_CODE' => '',
                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
                'URL_TO_COMMENT' => '',
                'WIDTH' => '',
                'COMMENTS_COUNT' => '5',
                'BLOG_USE' => $arParams['BLOG_USE'],
                'FB_USE' => $arParams['FB_USE'],
                'FB_APP_ID' => $arParams['FB_APP_ID'],
                'VK_USE' => $arParams['VK_USE'],
                'VK_API_ID' => $arParams['VK_API_ID'],
                'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                'CACHE_TIME' => $arParams['CACHE_TIME'],
                'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                'BLOG_TITLE' => '',
                'BLOG_URL' => $arParams['BLOG_URL'],
                'PATH_TO_SMILE' => '',
                'EMAIL_NOTIFY' => $arParams['BLOG_EMAIL_NOTIFY'],
                'AJAX_POST' => 'Y',
                'SHOW_SPAM' => 'Y',
                //'SHOW_RATING' => 'Y',
                //'RATING_TYPE' => 'stars',
                'FB_TITLE' => '',
                'FB_USER_ADMIN_ID' => '',
                'FB_COLORSCHEME' => 'light',
                'FB_ORDER_BY' => 'reverse_time',
                'VK_TITLE' => '',
                'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME']
            );
            if(isset($arParams["USER_CONSENT"]))
                $componentCommentsParams["USER_CONSENT"] = $arParams["USER_CONSENT"];
            if(isset($arParams["USER_CONSENT_ID"]))
                $componentCommentsParams["USER_CONSENT_ID"] = $arParams["USER_CONSENT_ID"];
            if(isset($arParams["USER_CONSENT_IS_CHECKED"]))
                $componentCommentsParams["USER_CONSENT_IS_CHECKED"] = $arParams["USER_CONSENT_IS_CHECKED"];
            if(isset($arParams["USER_CONSENT_IS_LOADED"]))
                $componentCommentsParams["USER_CONSENT_IS_LOADED"] = $arParams["USER_CONSENT_IS_LOADED"];
            $APPLICATION->IncludeComponent(
                'bitrix:catalog.comments',
                '',
                $componentCommentsParams,
                $component,
                array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>
    </div>
</section>
<?
        if ($haveOffers)
        {
            $offerIds = array();
            $offerCodes = array();

            $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

            foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
            {
                $offerIds[] = (int)$jsOffer['ID'];
                $offerCodes[] = $jsOffer['CODE'];

                $fullOffer = $arResult['OFFERS'][$ind];
                $measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

                $strAllProps = '';
                $strMainProps = '';
                $strPriceRangesRatio = '';
                $strPriceRanges = '';

                if ($arResult['SHOW_OFFERS_PROPS'])
                {
                    if (!empty($jsOffer['DISPLAY_PROPERTIES']))
                    {
                        foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
                        {
                            $current = '<dt>'.$property['NAME'].'</dt><dd>'.(
                                is_array($property['VALUE'])
                                    ? implode(' / ', $property['VALUE'])
                                    : $property['VALUE']
                                ).'</dd>';
                            $strAllProps .= $current;

                            if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
                            {
                                $strMainProps .= $current;
                            }
                        }

                        unset($current);
                    }
                }

                if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
                {
                    $strPriceRangesRatio = '('.Loc::getMessage(
                            'CT_BCE_CATALOG_RATIO_PRICE',
                            array('#RATIO#' => ($useRatio
                                    ? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
                                    : '1'
                                ).' '.$measureName)
                        ).')';

                    foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
                    {
                        if ($range['HASH'] !== 'ZERO-INF')
                        {
                            $itemPrice = false;

                            foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
                            {
                                if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                                {
                                    break;
                                }
                            }

                            if ($itemPrice)
                            {
                                $strPriceRanges .= '<dt>'.Loc::getMessage(
                                        'CT_BCE_CATALOG_RANGE_FROM',
                                        array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
                                    ).' ';

                                if (is_infinite($range['SORT_TO']))
                                {
                                    $strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                                }
                                else
                                {
                                    $strPriceRanges .= Loc::getMessage(
                                        'CT_BCE_CATALOG_RANGE_TO',
                                        array('#TO#' => $range['SORT_TO'].' '.$measureName)
                                    );
                                }

                                $strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
                            }
                        }
                    }

                    unset($range, $itemPrice);
                }

                $jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
                $jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
                $jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
                $jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
            }

            $templateData['OFFER_IDS'] = $offerIds;
            $templateData['OFFER_CODES'] = $offerCodes;
            unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

            $jsParams = array(
                'AUTHORIZE' => $USER->isAuthorized(),
                'CONFIG' => array(
                    'AUTHORIZE' => $USER->isAuthorized(),
                    'USE_CATALOG' => $arResult['CATALOG'],
                    'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                    'SHOW_PRICE' => true,
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
                    'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                    'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
                    'OFFER_GROUP' => $arResult['OFFER_GROUP'],
                    'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
                    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                    'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
                    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                    'USE_STICKERS' => true,
                    'USE_SUBSCRIBE' => $showSubscribe,
                    'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
                    'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
                    'ALT' => $alt,
                    'TITLE' => $title,
                    'MAGNIFIER_ZOOM_PERCENT' => 200,
                    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
                    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
                    'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                        ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                        : null
                ),
                'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
                'VISUAL' => $itemIds,
                'DEFAULT_PICTURE' => array(
                    'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
                    'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
                ),
                'PRODUCT' => array(
                    'ID' => $arResult['ID'],
                    'ACTIVE' => $arResult['ACTIVE'],
                    'NAME' => $arResult['~NAME'],
                    'CATEGORY' => $arResult['CATEGORY_PATH']
                ),
                'BASKET' => array(
                    'AUTHORIZE' => $USER->isAuthorized(),
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'BASKET_URL' => $arParams['BASKET_URL'],
                    'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
                    'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                ),
                'OFFERS' => $arResult['JS_OFFERS'],
                'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
                'TREE_PROPS' => $skuProps
            );
        }
        else
        {
            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
            {
                ?>
                <div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
                    <?
                    if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
                    {
                        foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
                        {
                            ?>
                            <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
                            <?
                            unset($arResult['PRODUCT_PROPERTIES'][$propId]);
                        }
                    }

                    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
                    if (!$emptyProductProperties)
                    {
                        ?>
                        <table>
                            <?
                            foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
                            {
                                ?>
                                <tr>
                                    <td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
                                    <td>
                                        <?
                                        if (
                                            $arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
                                            && $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
                                        )
                                        {
                                            foreach ($propInfo['VALUES'] as $valueId => $value)
                                            {
                                                ?>
                                                <label>
                                                    <input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
                                                           value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
                                                    <?=$value?>
                                                </label>
                                                <br>
                                                <?
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            <select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
                                                <?
                                                foreach ($propInfo['VALUES'] as $valueId => $value)
                                                {
                                                    ?>
                                                    <option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
                                                        <?=$value?>
                                                    </option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                            <?
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?
                            }
                            ?>
                        </table>
                        <?
                    }
                    ?>
                </div>
                <?
            }

            $jsParams = array(
                'AUTHORIZE' => $USER->isAuthorized(),
                'CONFIG' => array(
                    'AUTHORIZE' => $USER->IsAuthorized(),
                    'USE_CATALOG' => $arResult['CATALOG'],
                    'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                    'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
                    'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
                    'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
                    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
                    'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
                    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                    'USE_STICKERS' => true,
                    'USE_SUBSCRIBE' => $showSubscribe,
                    'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
                    'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
                    'ALT' => $alt,
                    'TITLE' => $title,
                    'MAGNIFIER_ZOOM_PERCENT' => 200,
                    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
                    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
                    'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                        ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                        : null
                ),
                'VISUAL' => $itemIds,
                'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
                'PRODUCT' => array(
                    'ID' => $arResult['ID'],
                    'ACTIVE' => $arResult['ACTIVE'],
                    'PICT' => reset($arResult['MORE_PHOTO']),
                    'NAME' => $arResult['~NAME'],
                    'SUBSCRIPTION' => true,
                    'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
                    'ITEM_PRICES' => $arResult['ITEM_PRICES'],
                    'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
                    'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
                    'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
                    'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
                    'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
                    'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
                    'SLIDER' => $arResult['MORE_PHOTO'],
                    'CAN_BUY' => $arResult['CAN_BUY'],
                    'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
                    'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
                    'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
                    'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
                    'CATEGORY' => $arResult['CATEGORY_PATH']
                ),
                'BASKET' => array(
                    'AUTHORIZE' => $USER->isAuthorized(),
                    'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
                    'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'EMPTY_PROPS' => $emptyProductProperties,
                    'BASKET_URL' => $arParams['BASKET_URL'],
                    'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                    'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
                )
            );
            unset($emptyProductProperties);
        }

        if ($arParams['DISPLAY_COMPARE'])
        {
            $jsParams['COMPARE'] = array(
                'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
                'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
                'COMPARE_PATH' => $arParams['COMPARE_PATH']
            );
        }
        ?>
<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: 'Услуга заказана<?GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>',
		MESSAGE_POPUP: 'Услуга заказана',
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<? $this->SetViewTarget('googleMapinitDetail'); ?>
    <script type="application/javascript">
         <?
            if (!empty($arResult['PROPERTIES'])) {
            $areaIds = array();

                ?>window.locations = [{lat:<?=$arResult['PROPERTIES']["LNG"]["VALUE"]?> , lng:<?=$arResult['PROPERTIES']["LAT"]["VALUE"]?>}];<?
            }

            //$signer = new \Bitrix\Main\Security\Sign\Signer;
            //$signedTemplate = $signer->sign($templateName, 'catalog.top');
            //$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.top');
         ?>
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U"></script>
<?
$this->EndViewTarget('googleMapinitDetail'); ?>

<? $APPLICATION->IncludeComponent("kopaka:techniks.nearest", ".default", array(
    "SORT_FIELD" => "SORT",
    "SORT_BY" => "ASC",
    "CATALOG_IBLOCK_CODE" => "clothes",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "3"
), false
); ?>



<? $APPLICATION->IncludeComponent("kopaka:searches.last", ".default", array(
    "SORT_FIELD" => "SORT",
    "SORT_BY" => "ASC",
    "CATALOG_IBLOCK_CODE" => "clothes",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "3",
	"NAV_ON_PAGE" => "3"
), false
); ?>