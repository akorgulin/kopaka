<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

$colorSchemes = array(
    "green" => "bx-green",
    "yellow" => "bx-yellow",
    "red" => "bx-red",
    "blue" => "bx-blue",
);
if (isset($colorSchemes[$arParams["TEMPLATE_THEME"]])) {
    $colorScheme = $colorSchemes[$arParams["TEMPLATE_THEME"]];
} else {
    $colorScheme = "";
}
?>
<? if ($arResult["nEndPage"] >= 1 && $arResult["NavPageNomer"] < $arResult["nEndPage"]) { ?>

    <div id=""  class="lazyload-container btn_more">
    	<a class="ajax_loadmore" href="javascript:void(0)" data-record-count="<?= $arResult["NavRecordCount"]?>" data-ajax-id="" data-show-more="<?= $arResult["NavNum"] ?>" data-cur-page="<?= $arResult["NavPageNomer"] ?>" data-max-page="<?= $arResult["nEndPage"] ?>" data-next-page="<?= ($arResult["NavPageNomer"] + 1) ?>" class="lazyload-link">
        	<svg
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="50px" height="50px">
            <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
                  d="M35.491,21.920 C35.889,21.523 36.533,21.523 36.930,21.920 L40.038,25.028 L43.146,21.920 C43.544,21.523 44.188,21.523 44.585,21.920 C44.983,22.318 44.983,22.962 44.585,23.359 L40.758,27.187 C40.567,27.378 40.308,27.485 40.038,27.485 C39.768,27.485 39.510,27.378 39.319,27.187 L35.491,23.359 C35.094,22.962 35.094,22.318 35.491,21.920 ZM25.000,41.183 C16.076,41.183 8.817,33.923 8.817,25.000 C8.817,16.077 16.076,8.817 25.000,8.817 C28.575,8.817 31.964,9.959 34.800,12.121 C37.545,14.212 39.584,17.180 40.542,20.476 C40.699,21.016 40.388,21.580 39.849,21.737 C39.309,21.894 38.744,21.584 38.588,21.044 C36.844,15.043 31.256,10.852 25.000,10.852 C17.198,10.852 10.852,17.198 10.852,25.000 C10.852,32.801 17.199,39.148 25.000,39.148 C28.779,39.148 32.332,37.676 35.004,35.004 C35.401,34.607 36.046,34.607 36.443,35.004 C36.840,35.402 36.840,36.046 36.443,36.443 C33.386,39.500 29.323,41.183 25.000,41.183 ZM25.000,50.000 C11.215,50.000 -0.000,38.785 -0.000,25.000 C-0.000,15.621 5.176,7.112 13.509,2.792 C14.008,2.533 14.622,2.728 14.880,3.227 C15.139,3.726 14.944,4.340 14.445,4.598 C6.790,8.567 2.035,16.384 2.035,25.000 C2.035,37.663 12.337,47.965 25.000,47.965 C37.663,47.965 47.965,37.663 47.965,25.000 C47.965,12.337 37.663,2.035 25.000,2.035 C24.438,2.035 23.982,1.579 23.982,1.017 C23.982,0.455 24.438,-0.000 25.000,-0.000 C38.785,-0.000 50.000,11.215 50.000,25.000 C50.000,38.785 38.785,50.000 25.000,50.000 Z"/>
        	</svg>
    	</a>
	</div>
<? } ?>
<script type="text/javascript">
    $(document).ready(function ($) {

        $("#orders div.btn_more").attr("id",bxAjaxIdBbitrixOrder);
        $("#orders div.btn_more>a").attr("data-ajax-id",bxAjaxIdBbitrixOrder);
        $("#orders div.btn_more>a").data("ajax-id",bxAjaxIdBbitrixOrder);


		if($("#orders div.btn_more>a").data("cur-page")>=$("#orders div.btn_more>a").data("max-page")){
           $("#orders div.btn_more").remove();
		}

        $(document).on('click', '#orders [data-show-more]', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var btn = $(this);
            var page = btn.attr('data-next-page');
            var pagec = btn.attr('data-max-page');
            var id = btn.attr('data-show-more');
            var bx_ajax_id_bitrix = btn.attr('data-ajax-id');
            var block_id = "#orders #comp_" + bx_ajax_id_bitrix;
            var record_count = btn.attr('data-record-count');

            var defaultData = {
                bxAjaxidBitrix: bx_ajax_id_bitrix
            };
            defaultData['PAGEN_' + id] = page;
            $.ajax({
                type: "POST",
                url: window.location.href,
                dataType: "html",
                data: defaultData,
                cache: false,
                timeout: 3000,
                success: function (data) {
                    var dom = $(data);
                    $blockAppend = dom.find(block_id).find("a.vehicle-card");
                    $more = dom.find("#orders div.btn_more>a");
                    $(block_id).find("a.vehicle-card").last().after($blockAppend);

                    if($("#orders a.vehicle-card").length>=record_count) {
                        $("#orders div.btn_more").remove();
                    }

                    $("#orders div.btn_more>a").attr('data-next-page', $more.attr('data-next-page'));
                    $("#orders div.btn_more>a").attr('data-max-page', $more.attr('data-max-page'));
                    return false;
                }
            });
            return false;
        });
    });
</script>
