<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;
?>
<?CUtil::InitJSCore(array("image"));?>


<?
if(strlen($arResult["MESSAGE"])>0)
{
	?>
	<div class="blog-textinfo blog-note-box">
		<div class="blog-textinfo-text">
			<?=$arResult["MESSAGE"]?>
		</div>
	</div>
	<?
}
if(strlen($arResult["ERROR_MESSAGE"])>0)
{
	?>
	<div class="blog-errors blog-note-box blog-note-error">
		<div class="blog-error-text">
			<?=$arResult["ERROR_MESSAGE"]?>
		</div>
	</div>
	<?
}
if(strlen($arResult["FATAL_MESSAGE"])>0)
{
	?>
	<div class="blog-errors blog-note-box blog-note-error">
		<div class="blog-error-text">
			<?=$arResult["FATAL_MESSAGE"]?>
		</div>
	</div>
	<?
}
elseif(strlen($arResult["NOTE_MESSAGE"])>0)
{
	?>
	<div class="blog-textinfo blog-note-box">
		<div class="blog-textinfo-text">
			<?=$arResult["NOTE_MESSAGE"]?>
		</div>
	</div>
	<?
}
else
{
	if(!empty($arResult["Post"])>0)
	{
		$className = "blog-post";
		$className .= " blog-post-first";
		$className .= " blog-post-alt";
		$className .= " blog-post-year-".$arResult["Post"]["DATE_PUBLISH_Y"];
		$className .= " blog-post-month-".IntVal($arResult["Post"]["DATE_PUBLISH_M"]);
		$className .= " blog-post-day-".IntVal($arResult["Post"]["DATE_PUBLISH_D"]);
		?>
    <!--Category-->
    <section class="content_wrapper nobackground clearfix">
            <div class="wrapper center">
                <?
                $blog = "fijiblognew";
                $category = "";
                $day = "";
                $month = "";
                $year = "";
                ?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:blog.blog",
                    "categories",
                    array(
                        "BLOG_URL" => $blog,
                        "BLOG_VAR" => "",
                        "CACHE_TIME" => "7200",
                        "CACHE_TIME_LONG" => "604600",
                        "CACHE_TYPE" => "N",
                        "CATEGORY_ID" => $category,
                        "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
                        "DAY" => $day,
                        "FILTER_NAME" => "arFilter",
                        "IMAGE_MAX_HEIGHT" => "600",
                        "IMAGE_MAX_WIDTH" => "600",
                        "MESSAGE_COUNT" => "12",
                        "MONTH" => $month,
                        "NAV_TEMPLATE" => "",
                        "PAGE_VAR" => "",
                        "PATH_TO_BLOG" => "",
                        "PATH_TO_BLOG_CATEGORY" => "",
                        "PATH_TO_POST" => "",
                        "PATH_TO_POST_EDIT" => "",
                        "PATH_TO_SMILE" => "",
                        "PATH_TO_USER" => "",
                        "POST_PROPERTY_LIST" => array(
                            0 => "UF_BLOG_POST_PIC",
                            1 => "UF_BLOG_POST_GROUP",
                        ),
                        "POST_VAR" => "",
                        "RATING_TYPE" => "",
                        "SEO_USER" => "N",
                        "SET_NAV_CHAIN" => "Y",
                        "SET_TITLE" => "Y",
                        "SHOW_RATING" => "",
                        "USER_VAR" => "",
                        "YEAR" => $year,
                        "COMPONENT_TEMPLATE" => ".default"
                    ),
                    false
                );?>

            </div>

            <div class="container page_sidebar">
                <div class="row">
                    <div class="col-8 <? $className?>" id="blg-post-<?=$arResult["Post"]["ID"]?>">

                        <script type="text/javascript">
                        BX.viewImageBind(
                            'blg-post-<?=$arResult["Post"]["ID"]?>',
                            {showTitle: false},
                            {tag:'IMG', attr: 'data-bx-image'}
                        );
                        </script>
                        <h1 class="page_header" data-aos="fade-up" data-aos-delay="300"><?=$arResult["Post"]["TITLE"]?></h1>
                        <div class="post_date" data-aos="fade-up" data-aos-delay="400"><?=$arResult["Post"]["DATE_PUBLISH_FORMATED"]?></div>
                        <? //dump($arResult["POST_PROPERTIES"]["SHOW"]); ?>
                        <?if($arResult["POST_PROPERTIES"]["SHOW"] == "Y") {
                            $eventHandlerID = false;
                            $eventHandlerID = AddEventHandler('main', 'system.field.view.file', Array('CBlogTools', 'blogUFfileShow'));
                            foreach ($arResult["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){ ?>
                                <?
                                if (!empty($arPostField["VALUE"]) && $FIELD_NAME == 'UF_BLOG_POST_PIC'){
                                    $arFile = CFile::GetFileArray($arPostField["VALUE"]);
                                    ?>
                                    <div class="post_preview" data-aos="fade-up" data-aos-delay="500">
                                        <img src="<?=$arFile["SRC"]?>" width="800">
                                    </div>
                                <?} ?>
                            <?}
                            if ($eventHandlerID !== false && (intval($eventHandlerID) > 0))
                                RemoveEventHandler('main', 'system.field.view.file', $eventHandlerID);
                        }?>
                        <p><?=$arResult["Post"]["textFormated"]?></p>
                        <a href="/blog/" class="btn mint iconlink iconleft"><span class="lnr lnr-arrow-left"></span> Вернуться в блог</a>
                    </div>
                    <div class="col-4 sidebar">
                        <?
                        $blog = "fijiblognew";
                        $category = "";
                        $day = "";
                        $month = "";
                        $year = "";
                        ?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:blog.blog",
                            "section",
                            array(
                                "BLOG_URL" => $blog,
                                "BLOG_VAR" => "",
                                "CACHE_TIME" => "7200",
                                "CACHE_TIME_LONG" => "604600",
                                "CACHE_TYPE" => "N",
                                "CATEGORY_ID" => $category,
                                "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
                                "DAY" => $day,
                                "FILTER_NAME" => "arFilter",
                                "IMAGE_MAX_HEIGHT" => "600",
                                "IMAGE_MAX_WIDTH" => "600",
                                "MESSAGE_COUNT" => "4",
                                "MONTH" => $month,
                                "NAV_TEMPLATE" => "",
                                "PAGE_VAR" => "",
                                "PATH_TO_BLOG" => "",
                                "PATH_TO_BLOG_CATEGORY" => "",
                                "PATH_TO_POST" => "",
                                "PATH_TO_POST_EDIT" => "",
                                "PATH_TO_SMILE" => "",
                                "PATH_TO_USER" => "",
                                "POST_PROPERTY_LIST" => array(
                                    0 => "UF_BLOG_POST_PIC",
                                    1 => "UF_BLOG_POST_GROUP",
                                ),
                                "POST_VAR" => "",
                                "RATING_TYPE" => "",
                                "SEO_USER" => "N",
                                "SET_NAV_CHAIN" => "Y",
                                "SET_TITLE" => "Y",
                                "SHOW_RATING" => "",
                                "USER_VAR" => "",
                                "YEAR" => $year,
                                "COMPONENT_TEMPLATE" => ".default"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>

        </section>
		<?
	}
	else
		echo GetMessage("BLOG_BLOG_BLOG_NO_AVAIBLE_MES");
}
?>
