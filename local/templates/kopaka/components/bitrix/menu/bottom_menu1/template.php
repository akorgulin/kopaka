<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

?>

<ul>
<?foreach($arResult as $itemIdex => $arItem):?>
	<li>
		<a href="<?=$arItem["LINK"]?>" class="b-menu__link <?if ($arItem["SELECTED"]):?>active <?endif;?><?if(!empty($arItem['PARAMS']['class'])):?> <?=$arItem['PARAMS']['class']?><?endif;?>"><?=$arItem["TEXT"];?></a>
	</li>
<?endforeach;?>
</ul>