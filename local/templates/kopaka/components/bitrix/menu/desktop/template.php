<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

$ULBegin = false;
?>

<?$i=0; ?>
<?foreach($arResult as $itemIdex => $arItem):?>
    <?if($i==0):?><ul class="hidden-mobile"><?endif;?>
    <li <?php if ($arItem["SELECTED"]){?>class="without_megamenu on"<?php } else { ?>class="without_megamenu"<?php } ?>>
        <a href="<?=$arItem["LINK"]?>">
            <?if($i==0):?><span class="menu_bar"> <span></span> <span></span> <span></span> </span><?endif;?>
            <?=$arItem["TEXT"];?>
        </a>
        <?if($i==0):?>
            <!-- Catalog -->
            <div class="megamenu nopadding">
                <div class="selpak-wrapper">
                    <div id="v-nav">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "tree",
                            array(
                                "ROOT_MENU_TYPE" => "submenu",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "MENU_CACHE_GET_VARS" => array(
                                ),
                                "MAX_LEVEL" => "1",
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "N"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        <?endif;?>
        <?if($i==1):?>
            <div class="megamenu">
                <div class="menu_logos">
                    <?$APPLICATION->IncludeComponent("fijie:brands", "menu_top_brands", array(
                        "SORT_FIELD" => "ID",
                        "SORT_BY" => "DESC",
                        "BRANDS_IBLOCK_CODE" => "brands",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "COUNT_RECORDS" => ""
                    ),
                        false
                    );?>
                </div>
            </div>
        <?endif;?>
    </li>
    <?if($i==(count($arResult)-1) && $ULBegin==false):?></ul><?endif;?>
    <?$i++?>
<?endforeach;?>