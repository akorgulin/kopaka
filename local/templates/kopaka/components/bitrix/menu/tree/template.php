<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
ob_start();
$APPLICATION->IncludeComponent("fijie:brands", "bannermenu", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "brands",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "5"
),
    false
);
$output = ob_get_contents();
ob_end_clean();
?>

<?if (!empty($arResult)):?>
<ul class="general">

<?
$blockSubmenu = '<div class="col-4">
    <div class="menu_title">По брендам</div>
    '.$output.'
</div>
<div class="col-4">
    <div class="banners_images">
        <div class="item"><a href="#"><img src="'.SITE_TEMPLATE_PATH.'/img/banner_001.png" alt=""></a></div>
        <div class="item"><a href="#"><img src="'.SITE_TEMPLATE_PATH.'/img/banner_002.png" alt=""></a></div>
    </div>
</div>';
$previousLevel = 0;
$parent_number = 0;
foreach($arResult as $arItem):
?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></div>".$blockSubmenu."</div></div></div></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>
			<li<?if($arItem["CHILD_SELECTED"] !== true):?> class="menu-close"<?endif?>>
				<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $("ul.general").after($("div.tab-content-<?=$arItem["ITEM_INDEX"]?>"));
                        });
                    </script>
                    <div <?if($parent_number==0){?>style="display: block;"<?}else{?>style="display: none;"<?}?> class="tab-content tab-content-<?=$arItem["ITEM_INDEX"]?>">
                        <div class="container">
                                <div class="row">
                                        <div class="col-4">
                                                <div class="menu_title"><?=$arItem["TEXT"]?></div>
                                                <ul>
                    <?$parent_number++;?>
	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>
				<li>
					<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
				</li>
		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div>".$blockSubmenu."</div></div></div></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?endif?>