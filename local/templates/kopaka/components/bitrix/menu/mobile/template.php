<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

$ULBegin = false;
?>
<nav class="mobile_menu animated bounceInDown">
    <?$i=0; ?>
    <?foreach($arResult as $itemIdex => $arItem):?>
        <?if($i==0):?><ul><?endif;?>
        <li class="<?if($i==0):?>sub-menu<?endif;?> <?php if ($arItem["SELECTED"]){?>on<?php } ?>">
            <a href="<?=$arItem["LINK"]?>">
                <?=$arItem["TEXT"];?>
                <?if($i==0):?><div class="right lnr lnr-chevron-down"></div><?endif;?>
            </a>
            <?if($i==0):?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "mobile_sub",
                    array(
                        "ROOT_MENU_TYPE" => "submenu",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            <?endif;?>
        </li>
        <?if($i==(count($arResult)-1) && $ULBegin==false):?></ul><?endif;?>
        <?$i++?>
    <?endforeach;?>
</nav>