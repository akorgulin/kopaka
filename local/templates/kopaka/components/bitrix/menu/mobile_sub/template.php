<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

$ULBegin = false;
?>
<?if (!empty($arResult)) {?>
<ul class="general_mobile_menu">

<?
$previousLevel = 0;
$parent_number = 0;
foreach($arResult as $arItem) {
?>
<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
    <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
<?endif?>

<?if ($arItem["IS_PARENT"]):?>
<li<?if($arItem["CHILD_SELECTED"] !== true) { ?> class="sub-menu on"<?} else {?>class="sub-menu"<?}?>>
    <a href="<?=$arItem["LINK"]?>">
        <?=$arItem["TEXT"]?>
        <div class="right lnr lnr-chevron-down"></div>
    </a>
    <ul>
    <?$parent_number++;?>
    <?else:?>

    <?if ($arItem["PERMISSION"] > "D") { ?>
        <li>
            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        </li>
    <?}?>
<?endif?>

<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?}?>

<?if ($previousLevel > 1)://close last item tags?>
    <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
    </ul>
<?}?>
