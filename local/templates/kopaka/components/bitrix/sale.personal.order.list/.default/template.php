<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name, $component->arParams['AJAX_OPTION_ADDITIONAL']);
$arParams["bxajaxIdBbitrix"] = $bxajaxid;
$component->bxajaxid = $bxajaxid;

?> <script type="text/javascript"> var bxAjaxIdBbitrixOrder = '<?=$bxajaxid?>';</script>

<div id="comp_<?= $bxajaxid ?>" class="orders-list">
<?php
if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	if (!count($arResult['ORDERS']))
	{
		if ($_REQUEST["filter_history"] == 'Y')
		{
			if ($_REQUEST["show_canceled"] == 'Y')
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER')?></h3>
				<?
			}
			else
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST')?></h3>
				<?
			}
		}
		else
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
			<?
		}
	}
	?>

	<?
	if (!count($arResult['ORDERS']))
	{
		/*?>
        <div class="col-4 sidebar">
			<a href="<?=htmlspecialcharsbx($arParams['PATH_TO_CATALOG'])?>" class="sale-order-history-link">
				<?=Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG')?>
			</a>
		</div>
		<?*/
	}


	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$paymentChangeData = array();
		$orderHeaderStatus = null;

		foreach ($arResult['ORDERS'] as $key => $order)
		{
            $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
			?>
                <? foreach($order["BASKET_ITEMS"] as $i=>$product) {
                       $arSort= Array("NAME"=>"ASC");
                       $arSelect = Array("ID","IBLOCK_ID","PROPERTY_MORE_PHOTO", "PROPERTY_ARTNUMBER","PROPERTY_STATUS","PROPERTY_BLOG_POST_ID","PROPERTY_BLOG_COMMENTS_CNT","CODE","PREVIEW_PICTURE");
                       $arFilter = Array( "ID"=>$product["PRODUCT_ID"]);

                       $res =  CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

                       $srcimage = "";
                       if($ob = $res->GetNextElement()){
                            $arFields = $ob->GetFields();
                            $artnumber=$arFields["PROPERTY_ARTNUMBER_VALUE"];
                            if($arFields["PROPERTY_MORE_PHOTO_VALUE"]>0) {
                                 $srcimage= CFile::GetPath($arFields["PROPERTY_MORE_PHOTO_VALUE"]);
                                 $fileTmp = CFile::ResizeImageGet(
                                     $arFields["PROPERTY_MORE_PHOTO_VALUE"],
                                     array('width' => 224, 'height' => 140),
                                     BX_RESIZE_IMAGE_EXACT,
                                     false
                                 );
                              $srcimage = !empty($fileTmp['src']) ? $fileTmp['src'] : '';
                            }
                       }

                        $arReviewsPost = [];
                        $dateс = date('m,d,Y h:i:s', time() - 1800 * 1);
                        CModule::IncludeModule("blog");
                        $arOrderB = Array("DATE_CREATE" => "DESC");
                        $arFilterB = [
                            "POST_ID"=>$arFields["PROPERTY_BLOG_POST_ID_VALUE"],
                            ">=DATE_CREATE"=> date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,$dateс))
                        ];

                        $arSelectedFields = Array("ID", "BLOG_ID", "POST_ID", "PARENT_ID", "AUTHOR_ID", "AUTHOR_NAME", "AUTHOR_EMAIL", "AUTHOR_IP", "AUTHOR_IP1", "TITLE", "POST_TEXT", "DATE_CREATE", "PUBLISH_STATUS");
                        $dbComment = CBlogComment::GetList($arOrderB, $arFilterB, false, false, $arSelectedFields);
                        while($arBlogComment = $dbComment->Fetch()) {
                            $arReviewsPost[] = $arBlogComment;
                        }


						$property_enums = CIBlockPropertyEnum::GetList(
							["DEF"=>"DESC", "SORT"=>"ASC"],
							[
								"IBLOCK_ID"=>$arFields["IBLOCK_ID"],
								"CODE"=>"STATUS",
								"ID" => $arFields["PROPERTY_STATUS_ENUM_ID"]
							]
						);

						$active = true;

						if($enum_fields = $property_enums->GetNext())
						{ 
							if($enum_fields["XML_ID"]=="free") {
								$active = true;
							} 
							if($enum_fields["XML_ID"]=="closed") {
								$active = false;
							}
					
						}

                        //
                       ?>
				<?if(!$active) { 
					$href = 'href="#"';
				} else {  
					$href = 'href="/catalog/technik/'.$arFields["CODE"].'/"';
				} ?>
                <a <?=$href?> class="item vehicle-card <?if(!$active) { ?>closed<?}?>">
                    <div class="card-info">
                        <span class="date"><?=FormatDateFromDB($product["DATE_UPDATE"], 'DD.MM.YYYY');?></span>
                        <div class="notifications" onclick="javascript:location.href='/personal/comment/?id=<?=$product["ID"]?>'">
                            <?if($arFields["PROPERTY_BLOG_COMMENTS_CNT_VALUE"]>0) { ?>
                                <div class="notifications active">
                                    <div class="notifications-counter">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="25px">
                                            <path fill-rule="evenodd" fill="rgb(187, 192, 198)" d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"></path>
                                        </svg>
                                        <span><?=intval($arFields["PROPERTY_BLOG_COMMENTS_CNT_VALUE"])?> откликов</span>
                                    </div>
                                    <span class="notifications-text visible">(<?=count($arReviewsPost)?> Новых)</span>
                                </div>
                            <?} else { ?>
                            <div class="notifications-counter">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="21px" height="25px">
                                    <path fill-rule="evenodd"  fill="rgb(187, 192, 198)"
                                          d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"/>
                                </svg>
                                <span><?=intval($arFields["PROPERTY_BLOG_COMMENTS_CNT_VALUE"])?> откликов</span>
                            </div>
                            <span class="notifications-text">(<?=count($arReviewsPost)?> Новых)</span>
                            <?}?>
                        </div>
                    </div>
					<?if($srcimage) { ?>
                        <div class="card-image">
                            <img src="<?=$srcimage?>" width="224" height="140" alt="">
                        </div>
                     <? } ?>
                    <div class="card-body">
                        <span class="title"><?=$product["NAME"]?></span>
                        <span class="desc"><?=$arFields["DETAIL_TEXT"]?></span>
                    </div>
					<?if(!$active) { ?>
                    <span class="closed-label">Закрыт</span>
					<? } ?>

                </a>
				<? /*
                <a href="/catalog/hitachi/hitachi_ew60c/" class="item vehicle-card">
                    <div class="card-info">
                        <span class="date">20.09.2018</span>
                        <div class="notifications active" onclick="javascript:location.href='/personal/comment/?id=381'">
                            <div class="notifications-counter">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="21px" height="25px">
                                    <path fill-rule="evenodd"  fill="rgb(187, 192, 198)"
                                          d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"/>
                                </svg>
                                <span>3 отклика</span>
                            </div>
                            <span class="notifications-text visible">(2 Новых)</span>
                        </div>
                    </div>
                    <div class="card-image">
                        <img src="/uploads/vehicles-images/image-3.jpg" alt="">
                    </div>
                    <div class="card-body">
                        <span class="title">Volvo 3CX ECO</span>
                        <span class="desc">Экскаватор-погрузчик</span>
                    </div>
                </a>
                <a href="/catalog/hitachi/hitachi_ew60c/" class="item vehicle-card">
                    <div class="card-info">
                        <span class="date">20.09.2018</span>
                        <div class="notifications active" onclick="javascript:location.href='/personal/comment/?id=381'">
                            <div class="notifications-counter">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="21px" height="25px">
                                    <path fill-rule="evenodd"  fill="rgb(187, 192, 198)"
                                          d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"/>
                                </svg>
                                <span>3 отклика</span>
                            </div>
                            <span class="notifications-text">(0 Новых)</span>
                        </div>
                    </div>
                    <div class="card-image">
                        <img src="/uploads/vehicles-images/image-3.jpg" alt="">
                    </div>
                    <div class="card-body">
                        <span class="title">Volvo 3CX ECO</span>
                        <span class="desc">Экскаватор-погрузчик</span>
                    </div>
                </a>
                <a href="/catalog/hitachi/hitachi_ew60c/" class="item vehicle-card">
                    <div class="card-info">
                        <span class="date">20.09.2018</span>
                        <div class="notifications active" onclick="javascript:location.href='/personal/comment/?id=381'">
                            <div class="notifications-counter">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="21px" height="25px">
                                    <path fill-rule="evenodd"  fill="rgb(187, 192, 198)"
                                          d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"/>
                                </svg>
                                <span>3 отклика</span>
                            </div>
                            <span class="notifications-text">(0 Новых)</span>
                        </div>
                    </div>
                    <div class="card-image">
                        <img src="/uploads/vehicles-images/image-3.jpg" alt="">
                    </div>
                    <div class="card-body">
                        <span class="title">Volvo 3CX ECO</span>
                        <span class="desc">Экскаватор-погрузчик</span>
                    </div>
                </a>
                <a href="/catalog/hitachi/hitachi_ew60c/" class="item vehicle-card closed">
                    <div class="card-info">
                        <span class="date">20.09.2018</span>
                        <div class="notifications" onclick="javascript:location.href='/personal/comment/?id=381'">
                            <div class="notifications-counter">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="21px" height="25px">
                                    <path fill-rule="evenodd"  fill="rgb(187, 192, 198)"
                                          d="M19.225,21.051 L18.899,21.051 L14.989,21.051 C14.538,23.169 12.549,25.000 10.297,25.000 L9.906,25.000 C7.654,25.000 5.664,23.169 5.213,21.051 L1.955,21.051 L0.978,21.051 C0.438,21.051 -0.000,20.608 -0.000,20.063 C-0.000,19.518 0.438,19.076 0.978,19.076 L1.303,19.076 L1.303,12.494 C1.303,8.528 3.944,5.167 7.560,4.075 C7.372,3.696 7.266,3.271 7.266,2.821 C7.266,1.258 8.538,-0.013 10.101,-0.013 C11.664,-0.013 12.936,1.258 12.936,2.821 C12.936,3.271 12.830,3.696 12.642,4.075 C16.259,5.167 18.899,8.528 18.899,12.494 L18.899,19.076 L19.225,19.076 C19.765,19.076 20.202,19.518 20.202,20.063 C20.202,20.608 19.765,21.051 19.225,21.051 ZM9.906,23.046 L10.297,23.046 C11.462,23.046 12.640,22.078 13.034,21.051 L7.169,21.051 C7.563,22.078 8.741,23.046 9.906,23.046 ZM10.101,1.941 C9.616,1.941 9.221,2.336 9.221,2.821 C9.221,3.306 9.616,3.700 10.101,3.700 C10.586,3.700 10.981,3.306 10.981,2.821 C10.981,2.336 10.586,1.941 10.101,1.941 ZM16.944,12.494 C16.944,8.722 13.874,5.654 10.101,5.654 C6.328,5.654 3.258,8.722 3.258,12.494 L3.258,19.076 C3.775,19.076 15.616,19.076 16.944,19.076 L16.944,12.494 Z"/>
                                </svg>
                                <span>0 откликов</span>
                            </div>
                            <span class="notifications-text">(2 Новых)</span>
                        </div>
                    </div>
                    <div class="card-image">
                        <img src="/uploads/vehicles-images/image-3.jpg" alt="">
                    </div>
                    <div class="card-body">
                        <span class="title">Volvo 3CX ECO</span>
                        <span class="desc">Экскаватор-погрузчик</span>
                    </div>
                    <span class="closed-label">Закрыт</span>
</a>*/ ?>
                        <? break; } ?>
		<? } ?>
<?
	}
	else
	{
		$orderHeaderStatus = null;

		if ($_REQUEST["show_canceled"] === 'Y' && count($arResult['ORDERS']))
		{
			?>
			<h1 class="sale-order-title">
				<?= Loc::getMessage('SPOL_TPL_ORDERS_CANCELED_HEADER') ?>
			</h1>
			<?
		}

		foreach ($arResult['ORDERS'] as $key => $order)
		{

				$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
				?>
                <div class="spoiler">
                    <div class="spoiler_title">
                            <span class="without_status">
                                <?= Loc::getMessage('SPOL_TPL_ORDER') ?>
                                <?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
                                <?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER'])?>
                                <?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
                                <?= $order['ORDER']['DATE_INSERT'] ?>,
                                <?= count($order['BASKET_ITEMS']); ?>
                                <?
                                $count = substr(count($order['BASKET_ITEMS']), -1);
                                if ($count == '1')
                                {
                                    echo Loc::getMessage('SPOL_TPL_GOOD');
                                }
                                elseif ($count >= '2' || $count <= '4')
                                {
                                    echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
                                }
                                else
                                {
                                    echo Loc::getMessage('SPOL_TPL_GOODS');
                                }
                                ?>
                                <?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
                                <?= $order['ORDER']['FORMATED_PRICE'] ?>
                            </span>
                                            <span class="status">
                                <?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?>
                            </span>
                    </div>
                    <div class="spoiler_content">
                        <div class="cart_items">
                            <? foreach($order["BASKET_ITEMS"] as $product) {
                                $arSort= Array("NAME"=>"ASC");
                                $arSelect = Array("ID","PROPERTY_MORE_PHOTO", "PROPERTY_ARTNUMBER");
                                $arFilter = Array( "ID"=>$product["PRODUCT_ID"]);

                                $res =  CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

                                $srcimage = "";
                                if($ob = $res->GetNextElement()){
                                    $arFields = $ob->GetFields();
                                    $artnumber=$arFields["PROPERTY_ARTNUMBER_VALUE"];
                                    if($arFields["PROPERTY_MORE_PHOTO_VALUE"]>0) {
                                        $srcimage= CFile::GetPath($arFields["PROPERTY_MORE_PHOTO_VALUE"]);
                                        $fileTmp = CFile::ResizeImageGet(
                                            $arFields["PROPERTY_MORE_PHOTO_VALUE"],
                                            array('width' => 70, 'height' => 70),
                                            BX_RESIZE_IMAGE_EXACT,
                                            false
                                        );

                                        $srcimage = !empty($fileTmp['src']) ? $fileTmp['src'] : '';

                                    }
                                }?>
                                <div class="item">
                                <?if($srcimage) { ?>
                                <div class="thumbnail">
                                    <a href="<?=$product["DETAIL_PAGE_URL"]?>"><img src="<?=$srcimage?>" alt=""></a>
                                </div>
                                <? } ?>
                                <div class="info">
                                    <div class="prod_name"><a href="<?=$product["DETAIL_PAGE_URL"]?>"><?=$product["NAME"]?></a></div>
                                    <?if($artnumber) { ?>
                                    <div class="prod_info">
                                        <span>Артикул: <?=$artnumber?></span>
                                    </div>
                                    <? } ?>
                                    <div class="prod_total_info">
                                        <span><?=$product["QUANTITY"]?> штук</span>
                                        <span class="price"><?=CurrencyFormat($product["BASE_PRICE"], "RUB")?></span>
                                    </div>
                                </div>
                            </div>
                            <? } ?>
                        </div>
                        <div class="total_info right">Оплачено: <span><?= $order['ORDER']['FORMATED_PRICE'] ?></span></div>

                        <div class="col-md-12 col-sm-12 sale-order-list-container hidden">
                            <div class="row">
                                <div class="col-md-12 sale-order-list-inner-accomplished">
                                    <div class="row sale-order-list-inner-row">
                                        <div class="col-md-3 col-sm-12 sale-order-list-about-accomplished">
                                            <a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>">
                                                <?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?>
                                            </a>
                                        </div>
                                        <div class="col-md-3 col-md-offset-6 col-sm-12 sale-order-list-repeat-accomplished">
                                            <a class="sale-order-list-repeat-link sale-order-link-accomplished" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>">
                                                <?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
			<?
		}
	}
	?>
	<div class="clearfix"></div>
	<?
	echo $arResult["NAV_STRING"];

	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$javascriptParams = array(
			"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
			"templateFolder" => CUtil::JSEscape($templateFolder),
			"templateName" => $this->__component->GetTemplateName(),
			"paymentList" => $paymentChangeData
		);
		$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
		</script>
		<?
	}
}
?>   
</div>
