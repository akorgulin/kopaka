<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="name_block">
	<span class="block_marble shadow2 cr30">~ <?=$arResult["IBLOCK"]["ELEMENTS_NAME"]?> ~</span>
</div>


<h1><?=$arResult["NAME"]?></h1>

<div class="text_block shadow_block cr30">
	<article>
		<br>

	<div class="news-detail">
		<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
			<h2><?=$arResult["NAME"]?></h2>
		<?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
			<div class="date-demo"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
		<?endif;?>

		<?if($arResult["NAV_RESULT"]):?>
			<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
			<?echo $arResult["NAV_TEXT"];?>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
		<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
			<?echo $arResult["DETAIL_TEXT"];?>
		<?else:?>
			<?echo $arResult["PREVIEW_TEXT"];?>
		<?endif?>
		<div style="clear:both"></div>
		<br />
	</div>

	<div class="clear"></div>
	<p class="back-demo"><a class="lsnn" href="<?=$arResult["IBLOCK"]["LIST_PAGE_URL"]?>"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a></p>
		<? $ElementID = $arResult["ID"]; ?>
        <?if($arParams["USE_RATING"]=="Y" && $ElementID):?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:iblock.vote",
                "",
                Array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ELEMENT_ID" => $ElementID,
                    "MAX_VOTE" => $arParams["MAX_VOTE"],
                    "VOTE_NAMES" => $arParams["VOTE_NAMES"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                ),
                $component
            );?>
        <?endif?>
		<?if($arParams["USE_CATEGORIES"]=="Y" && $ElementID):
			global $arCategoryFilter;
			$obCache = new CPHPCache;
			$strCacheID = $componentPath.LANG.$arParams["IBLOCK_ID"].$ElementID.$arParams["CATEGORY_CODE"];
			if(($tzOffset = CTimeZone::GetOffset()) <> 0)
				$strCacheID .= "_".$tzOffset;
			if($arParams["CACHE_TYPE"] == "N" || $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N")
				$CACHE_TIME = 0;
			else
				$CACHE_TIME = $arParams["CACHE_TIME"];
			if($obCache->StartDataCache($CACHE_TIME, $strCacheID, $componentPath))
			{
				$rsProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $ElementID, "sort", "asc", array("ACTIVE"=>"Y","CODE"=>$arParams["CATEGORY_CODE"]));
				$arCategoryFilter = array();
				while($arProperty = $rsProperties->Fetch())
				{
					if(is_array($arProperty["VALUE"]) && count($arProperty["VALUE"])>0)
					{
						foreach($arProperty["VALUE"] as $value)
							$arCategoryFilter[$value]=true;
					}
					elseif(!is_array($arProperty["VALUE"]) && strlen($arProperty["VALUE"])>0)
						$arCategoryFilter[$arProperty["VALUE"]]=true;
				}
				$obCache->EndDataCache($arCategoryFilter);
			}
			else
			{
				$arCategoryFilter = $obCache->GetVars();
			}
			if(count($arCategoryFilter)>0):
				$arCategoryFilter = array(
					"PROPERTY_".$arParams["CATEGORY_CODE"] => array_keys($arCategoryFilter),
					"!"."ID" => $ElementID,
				);
				?>
				<hr /><h3><?=GetMessage("CATEGORIES")?></h3>
				<?foreach($arParams["CATEGORY_IBLOCK"] as $iblock_id):?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					$arParams["CATEGORY_THEME_".$iblock_id],
					Array(
						"IBLOCK_ID" => $iblock_id,
						"NEWS_COUNT" => $arParams["CATEGORY_ITEMS_COUNT"],
						"SET_TITLE" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"FILTER_NAME" => "arCategoryFilter",
						"CACHE_FILTER" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
					),
					$component
				);?>
			<?endforeach?>
			<?endif?>
		<?endif?>
		<?if($arParams["USE_REVIEW"]=="Y" && IsModuleInstalled("forum") && $ElementID):?>
			<hr />
			<?$APPLICATION->IncludeComponent(
				"bitrix:forum.topic.reviews",
				"",
				Array(
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
					"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
					"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
					"FORUM_ID" => $arParams["FORUM_ID"],
					"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
					"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
					"ELEMENT_ID" => $ElementID,
					"AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
					"URL_TEMPLATES_DETAIL" => $arParams["POST_FIRST_MESSAGE"]==="Y"? $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"] :"",
				),
				$component
			);?>
		<?endif?>
		<?
		// ������ � ������� ������� �������� �������
		$APPLICATION->AddChainItem($APPLICATION->GetTitle());?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_DIR."include/subscr.php"
			)
		);?>

	</article>
</div><br>

