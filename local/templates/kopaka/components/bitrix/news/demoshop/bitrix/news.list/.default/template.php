<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="name_block">
	<span class="block_marble shadow2 cr30">~ <?=$arResult["ELEMENTS_NAME"]?> ~</span>
</div>


<h1><?=$arResult["ELEMENTS_NAME"]?></h1>

<div class="text_block shadow_block cr30">
    <article>
        <br>

    <div class="news-list">
        <?if($arParams["DISPLAY_TOP_PAGER"]):?>
            <?=$arResult["NAV_STRING"]?><br />
        <?endif;?>

        <ul class="n-demo lsnn">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

            ?>
            <li class="post" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                    <div class="date-demo"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
                <?endif?>

                <h3>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                </h3>
                <div><?echo $arItem["PREVIEW_TEXT"];?></div>

            </li>
        <?endforeach;?>
        </ul>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br /><?=$arResult["NAV_STRING"]?>
        <?endif;?>
    </div>

    <div class="clear"></div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR."include/subscr.php"
        )
    );?>
    <div class="clear"></div>

    </article>
</div><br>
