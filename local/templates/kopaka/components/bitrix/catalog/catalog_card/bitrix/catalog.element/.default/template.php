<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'PAKET_ID' => $mainId.'_buy_packet',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

?>
<section class="section content-section">
    <div id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
        <div class="container">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            );?>
        </div>

        <div class="container" id="<?=$itemIds['ID']?>">
            <div class="vehicle-container">
                <div class="vehicle-images-container">
                    <div class="images-counter">
                        <div class="counter-icon">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="38px" height="33px">
                                <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
                                      d="M36.515,31.513 C35.525,32.505 34.331,33.001 32.933,33.001 L5.068,33.001 C3.669,33.001 2.475,32.505 1.485,31.513 C0.496,30.522 0.001,29.325 0.001,27.924 L0.001,10.153 C0.001,8.752 0.496,7.555 1.485,6.564 C2.475,5.572 3.669,5.076 5.068,5.076 L9.501,5.076 L10.510,2.379 C10.761,1.731 11.219,1.172 11.885,0.703 C12.552,0.233 13.234,-0.001 13.934,-0.001 L24.067,-0.001 C24.766,-0.001 25.449,0.233 26.115,0.703 C26.782,1.172 27.240,1.731 27.491,2.379 L28.500,5.076 L32.933,5.076 C34.332,5.076 35.525,5.572 36.515,6.564 C37.504,7.555 37.999,8.752 37.999,10.153 L37.999,27.924 C37.999,29.325 37.504,30.522 36.515,31.513 ZM25.264,12.762 C23.529,11.023 21.441,10.154 19.000,10.154 C16.559,10.154 14.471,11.023 12.736,12.762 C11.002,14.501 10.134,16.593 10.134,19.039 C10.134,21.485 11.001,23.577 12.736,25.316 C14.472,27.055 16.559,27.924 19.000,27.924 C21.441,27.924 23.529,27.055 25.264,25.316 C26.999,23.577 27.866,21.485 27.866,19.039 C27.866,16.593 26.999,14.500 25.264,12.762 ZM19.000,24.751 C17.430,24.751 16.088,24.192 14.973,23.075 C13.858,21.957 13.300,20.612 13.300,19.039 C13.300,17.465 13.858,16.120 14.973,15.003 C16.088,13.885 17.430,13.327 19.000,13.327 C20.570,13.327 21.913,13.885 23.028,15.003 C24.142,16.120 24.700,17.465 24.700,19.039 C24.700,20.612 24.142,21.957 23.028,23.075 C21.913,24.192 20.570,24.751 19.000,24.751 Z"/>
                            </svg>
                        </div>
                        <span class="value">20</span>
                    </div>

                    <div id="big" class="vehicles-main-view-carousel owl-carousel">
                        <?
                        if (!empty($actualItem['MORE_PHOTO']))
                        {
                            foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                            {

                                ?>
                                <a class="item" data-fancybox="gallery" href="<?=$photo['SRC']?>">
                                    <img src="<?=$photo['SRC']?>" alt="">
                                </a>
                                <?
                            }
                        }
                        ?>
                    </div>
                    <div id="thumbs" class="vehicles-thumbs-view-carousel owl-carousel owl-theme">
                        <?
                        if (!empty($actualItem['MORE_PHOTO']))
                        {
                            foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                            {
                                ?>
                                <div class="item">
                                    <img src="<?=$photo['SRC']?>">
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="vehicle-info">
                    <?
                    if ($arParams['DISPLAY_NAME'] === 'Y')
                    {
                        ?>
                        <h1><?=$name?></h1>
                        <?
                    }

                    ?>
                    <h4>Колесный экскаватор</h4>
                    <div class="vehicle-characteristics">
                       <? if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
                        {
                            if (!empty($arResult['DISPLAY_PROPERTIES']))
                            {

                                foreach ($arResult['DISPLAY_PROPERTIES'] as $code=>$property)
                                {

                                    if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']]))
                                    {
                                        ?>
                                        <div class="item">
                                            <span class="title"><?=$property["NAME"]?></span>
                                            <span class="divider"></span>
                                            <span class="value"><?=(is_array($property['DISPLAY_VALUE'])
                                                    ? implode(' / ', $property['DISPLAY_VALUE'])
                                                    : $property['DISPLAY_VALUE'])?></span>
                                        </div>
                                        <?
                                    }
                                }
                                unset($property);
                            }
                        }
                        ?>
                    </div>
                    <div class="vehicle-map">
                        <div class="map" id="map"></div>
                    </div>
                    <div class="vehicle-location">
                        <span class="title">Местонахождение:</span>
                        <div class="location">
                            <span class="address">Москва, ул. Ленина 230</span>
                            <a href="#" class="location-link">
                                <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="15px" height="19px">
                                    <path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
                                          d="M7.233,0.004 C3.497,0.129 0.394,2.920 0.036,6.481 C-0.038,7.193 0.005,7.884 0.136,8.544 L0.136,8.544 C0.136,8.544 0.148,8.622 0.186,8.769 C0.302,9.264 0.475,9.744 0.691,10.194 C1.443,11.899 3.181,14.752 7.082,17.856 C7.321,18.048 7.672,18.048 7.915,17.856 C11.816,14.756 13.554,11.903 14.310,10.190 C14.530,9.740 14.699,9.264 14.815,8.766 C14.850,8.622 14.865,8.540 14.865,8.540 L14.865,8.540 C14.954,8.098 15.000,7.644 15.000,7.179 C15.000,3.126 11.496,-0.140 7.233,0.004 ZM7.499,10.751 C5.486,10.751 3.856,9.190 3.856,7.264 C3.856,5.337 5.486,3.776 7.499,3.776 C9.511,3.776 11.141,5.337 11.141,7.264 C11.141,9.190 9.511,10.751 7.499,10.751 Z"/>
                                </svg>
                                <span>Показать на карте</span>
                            </a>
                        </div>
                    </div>
                    <div class="vehicle-price-container">

                        <div class="vehicle-price">
                            <div class="item">
                                <span class="title">Цена за час, Р:</span>
                                <span class="price" id="<?=$itemIds['OLD_PRICE_ID']?>"><?=CurrencyFormat($arResult["PROPERTIES"]["PRICEHOUR"]["VALUE"], 'RUB')?><?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?></span>
                            </div>
                            <div class="item">
                                <span class="title">Цена за смену:</span>
                                <span class="price" id="<?=$itemIds['PRICE_ID']?>"><?=$price['PRINT_RATIO_PRICE']?></span>
                            </div>
                        </div>

                        <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
                            <?

                            if ($showAddBtn)
                            {
                                ?>
                                <div class="buttons-container">
                                    <a href="javascript:void(0);" class="btn btn-primary <?=$showButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['ADD_BASKET_LINK']?>">
                                        <span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 491.86 491.86" style="enable-background:new 0 0 491.86 491.86;" xml:space="preserve">
                  <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69
                    C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69
                    s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
              </svg>
                                    </a>
                                </div>
                                <?
                            }

                            if ($showBuyBtn)
                            {
                                ?>
                                <div class="buttons-container">
                                    <a href="javascript:void(0);" class="btn btn-primary <?=$buyButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['BUY_LINK']?>">
                                        <span><?=$arParams['MESS_BTN_BUY']?></span>
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 491.86 491.86" style="enable-background:new 0 0 491.86 491.86;" xml:space="preserve">
                  <path d="M465.167,211.614H280.245V26.691c0-8.424-11.439-26.69-34.316-26.69s-34.316,18.267-34.316,26.69v184.924H26.69
                    C18.267,211.614,0,223.053,0,245.929s18.267,34.316,26.69,34.316h184.924v184.924c0,8.422,11.438,26.69,34.316,26.69
                    s34.316-18.268,34.316-26.69V280.245H465.17c8.422,0,26.69-11.438,26.69-34.316S473.59,211.614,465.167,211.614z"/>
              </svg>
                                    </a>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<? $APPLICATION->IncludeComponent("kopaka:techniks.nearest", ".default", array(
    "SORT_FIELD" => "SORT",
    "SORT_BY" => "ASC",
    "CATALOG_IBLOCK_CODE" => "clothes",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "3"
), false
); ?>



<? $APPLICATION->IncludeComponent("kopaka:searches.last", ".default", array(
    "SORT_FIELD" => "SORT",
    "SORT_BY" => "ASC",
    "CATALOG_IBLOCK_CODE" => "clothes",
    "CACHE_TYPE" => "N",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "3",
	"NAV_ON_PAGE" => "3"
), false
); ?>