<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<!-- Map section search tech  -->
<div id="map" class="map location-map"></div>
<? $this->SetViewTarget('googleMapinit'); ?>
<script type="application/javascript">

        <?
        $cnt = count($arResult["SEARCH"]);
        $actual = 0;
        if($cnt>0) {
            $areaIds = array();

            ?>window.locations = [<?echo "\r\n";
                foreach ($arResult["SEARCH"] as $rowData)
                {
                    $dbEl = CIBlockElement::GetList(Array(), Array("ID"=>$rowData["ID"], "IBLOCK_ID"=>$rowData["IBLOCK_ID"]));
                    if($obEl = $dbEl->GetNextElement())
                    {
                        $props = $obEl->GetProperties();

                        if($props["LAT"]["VALUE"] && $props["LNG"]["VALUE"]) {
                            ?>{lat:<?=$props["LNG"]["VALUE"]?> , lng:<?=$props["LAT"]["VALUE"]?>},<? echo "\r\n";
                            $actual++;
                        }
                    }
                }
            ?>];<?
        }
        ?>

        $(document).ready(function(){
            $("div.results-text span>span.value").text(<?=$actual?>);
        });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALIKtoi9hoBxTVJ3hJRLneHOvlvnMmS_U"></script>
<?
$this->EndViewTarget(); ?>


