<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<section class="section content-section">
    <div class="container desktop-only">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        );?>
    </div>

    <div class="container">
        <?
        //$cnt = count($arResult["SEARCH"]);
        //if($cnt>0) { ?>
            <!--<p data-aos="fade-up" data-aos-delay="500">По запросу: «<?/*=$arResult["REQUEST"]["QUERY"]*/?>» найдено <span class="searchstring"><?/*=$cnt*/?>  товара</span></p>-->
        <?//}?>
        <form action="/search/result/" class="form search-form">
            <h2 class="desktop-only"><?=$APPLICATION->ShowTitle()?></h2>
            <div class="form-group-row location-row type-4">
                <div class="form-group">
                    <input type="text" name="address" class="form-control" id="searchLocation" required>
                    <label for="searchLocation">Адрес проведения работ</label>
                </div>
                <div class="styled-checkbox type-2">
                    <div class="checkbox-group">
                        <input id="locationCheckbox" type="checkbox">
                        <label for="locationCheckbox">
                            <span>Мое текущее местоположение</span>
                            <div class="checkbox"></div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-map">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog",
                    "googleMapSearch",
                    Array(
                        "ACTION_VARIABLE" => "action",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BASKET_URL" => "/personal/basket.php",
                        "BIG_DATA_RCM_TYPE" => "personal",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COMMON_ADD_TO_BASKET_ACTION" => "ADD",
                        "COMMON_SHOW_CLOSE_POPUP" => "N",
                        "COMPATIBLE_MODE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
                        "DETAIL_ADD_TO_BASKET_ACTION" => array("BUY"),
                        "DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array("BUY"),
                        "DETAIL_BACKGROUND_IMAGE" => "-",
                        "DETAIL_BRAND_USE" => "N",
                        "DETAIL_BROWSER_TITLE" => "-",
                        "DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
                        "DETAIL_DETAIL_PICTURE_MODE" => array("POPUP", "MAGNIFIER"),
                        "DETAIL_DISPLAY_NAME" => "Y",
                        "DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
                        "DETAIL_IMAGE_RESOLUTION" => "16by9",
                        "DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(),
                        "DETAIL_MAIN_BLOCK_PROPERTY_CODE" => array(),
                        "DETAIL_META_DESCRIPTION" => "-",
                        "DETAIL_META_KEYWORDS" => "-",
                        "DETAIL_OFFERS_FIELD_CODE" => array("", ""),
                        "DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
                        "DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
                        "DETAIL_SHOW_POPULAR" => "Y",
                        "DETAIL_SHOW_SLIDER" => "N",
                        "DETAIL_SHOW_VIEWED" => "Y",
                        "DETAIL_STRICT_SECTION_CHECK" => "N",
                        "DETAIL_USE_COMMENTS" => "N",
                        "DETAIL_USE_VOTE_RATING" => "N",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "FILTER_HIDE_ON_MOBILE" => "N",
                        "FILTER_VIEW_MODE" => "VERTICAL",
                        "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
                        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "9999",
                        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "9999",
                        "GIFTS_MESS_BTN_BUY" => "Выбрать",
                        "GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
                        "GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
                        "GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
                        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                        "GIFTS_SHOW_IMAGE" => "Y",
                        "GIFTS_SHOW_NAME" => "Y",
                        "GIFTS_SHOW_OLD_PRICE" => "Y",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                        "IBLOCK_ID" => "2",
                        "IBLOCK_TYPE" => "catalog",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "INSTANT_RELOAD" => "N",
                        "LABEL_PROP" => array(),
                        "LAZY_LOAD" => "N",
                        "LINE_ELEMENT_COUNT" => "9999",
                        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                        "LINK_IBLOCK_ID" => "",
                        "LINK_IBLOCK_TYPE" => "",
                        "LINK_PROPERTY_SID" => "",
                        "LIST_BROWSER_TITLE" => "-",
                        "LIST_ENLARGE_PRODUCT" => "STRICT",
                        "LIST_META_DESCRIPTION" => "-",
                        "LIST_META_KEYWORDS" => "-",
                        "LIST_OFFERS_FIELD_CODE" => array("", ""),
                        "LIST_OFFERS_LIMIT" => "9999",
                        "LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                        "LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                        "LIST_PROPERTY_CODE_MOBILE" => array(),
                        "LIST_SHOW_SLIDER" => "Y",
                        "LIST_SLIDER_INTERVAL" => "3000",
                        "LIST_SLIDER_PROGRESS" => "N",
                        "LOAD_ON_SCROLL" => "N",
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_COMPARE" => "Сравнение",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_COMMENTS_TAB" => "Комментарии",
                        "MESS_DESCRIPTION_TAB" => "Описание",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "MESS_PRICE_RANGES_TITLE" => "Цены",
                        "MESS_PROPERTIES_TAB" => "Характеристики",
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "OFFER_ADD_PICT_PROP" => "-",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "9999",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array(),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "PRODUCT_DISPLAY_MODE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "SEARCH_CHECK_DATES" => "Y",
                        "SEARCH_NO_WORD_LOGIC" => "Y",
                        "SEARCH_PAGE_RESULT_COUNT" => "9999",
                        "SEARCH_RESTART" => "N",
                        "SEARCH_USE_LANGUAGE_GUESS" => "Y",
                        "SECTIONS_SHOW_PARENT_NAME" => "Y",
                        "SECTIONS_VIEW_MODE" => "LIST",
                        "SECTION_ADD_TO_BASKET_ACTION" => "ADD",
                        "SECTION_BACKGROUND_IMAGE" => "-",
                        "SECTION_COUNT_ELEMENTS" => "Y",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_TOP_DEPTH" => "2",
                        "SEF_MODE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SHOW_DEACTIVATED" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_TOP_ELEMENTS" => "Y",
                        "SIDEBAR_DETAIL_SHOW" => "N",
                        "SIDEBAR_PATH" => "",
                        "SIDEBAR_SECTION_SHOW" => "Y",
                        "TEMPLATE_THEME" => "blue",
                        "TOP_ADD_TO_BASKET_ACTION" => "ADD",
                        "TOP_ELEMENT_COUNT" => "99999",
                        "TOP_ELEMENT_SORT_FIELD" => "sort",
                        "TOP_ELEMENT_SORT_FIELD2" => "id",
                        "TOP_ELEMENT_SORT_ORDER" => "asc",
                        "TOP_ELEMENT_SORT_ORDER2" => "desc",
                        "TOP_LINE_ELEMENT_COUNT" => "3",
                        "TOP_OFFERS_FIELD_CODE" => array("", ""),
                        "TOP_OFFERS_LIMIT" => "5",
                        "TOP_VIEW_MODE" => "SECTION",
                        "USER_CONSENT" => "N",
                        "USER_CONSENT_ID" => "0",
                        "USER_CONSENT_IS_CHECKED" => "Y",
                        "USER_CONSENT_IS_LOADED" => "N",
                        "USE_BIG_DATA" => "Y",
                        "USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
                        "USE_COMPARE" => "N",
                        "USE_ELEMENT_COUNTER" => "Y",
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "USE_FILTER" => "N",
                        "USE_GIFTS_DETAIL" => "Y",
                        "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
                        "USE_GIFTS_SECTION" => "Y",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "USE_REVIEW" => "N",
                        "USE_SALE_BESTSELLERS" => "Y",
                        "USE_STORE" => "N",
                        "VARIABLE_ALIASES" => Array(
                            "ELEMENT_ID" => "ELEMENT_ID",
                            "SECTION_ID" => "SECTION_ID"
                        )
                    )
                );?>
            </div>
            <div class="form-group-row selectors">
                <div class="form-group">
                    <select id="techniqueType" class="form-control" name="type">
                        <option value="0">Тип 1</option>
                        <option value="1">Тип 2</option>
                        <option value="2">Тип 3</option>
                        <option value="3">Тип 4</option>
                    </select>
                    <label for="techniqueType">Тип техники</label>
                </div>
                <div class="form-group">
                    <select id="workType" class="form-control" name="view">
                        <option value="0">Вид 1</option>
                        <option value="1">Вид 2</option>
                        <option value="2">Вид 3</option>
                        <option value="3">Вид 4</option>
                    </select>
                    <label for="workType">Вид работ</label>
                </div>
            </div>
            <?
            $class = "";
            if($_SESSION["latitude"]) {
                $valuelat = $_SESSION["latitude"];
                $class = "hidden-xs";
            }
            if($_SESSION["longitude"]) {
                $valuelon = $_SESSION["longitude"];
                $class = "hidden-xs";
            }
            ?>
            <div class="form-group-row latitude <?=$class?>">
                <div class="form-group full-width">
                    <input type="text" name="lat" class="form-control" value="<?=$valuelat?>" id="lat">
                    <label for="lat">Ширина</label>
                </div>
            </div>
            <div class="form-group-row longitude <?=$class?>">
                <div class="form-group full-width">
                    <input type="text" name="lon" class="form-control" value="<?=$valuelon?>"  id="lon">
                    <label for="lon">Долгота</label>
                </div>
            </div>

            <div class="form-group-row">
                <div class="form-group full-width">
                    <input type="text" name="q" class="form-control" id="searchQuery" required>
                    <label for="searchQuery">Поисковый запрос</label>
                </div>
            </div>
            <div class="button-container type-2">
                <input type="submit" name="save" class="submitform hidden-xs" value="Отправить" style="display: none;">
                <a href="#save" onclick="javascript:$('input.submitform').click();">
                <button class="btn btn-primary">
                    <span><?=GetMessage("FIND_BUTTON")?></span>
                    <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink"
                            width="36px" height="35px">
                        <path fill-rule="evenodd"  fill="rgb(244, 64, 41)"
                              d="M35.199,34.194 C34.138,35.262 32.417,35.262 31.356,34.194 L25.990,28.797 C25.235,28.038 25.028,26.946 25.347,25.993 L23.310,23.944 C20.834,26.312 17.493,27.773 13.807,27.773 C6.182,27.773 -0.000,21.556 -0.000,13.887 C-0.000,6.217 6.182,-0.000 13.807,-0.000 C21.433,-0.000 27.614,6.217 27.614,13.887 C27.614,16.977 26.598,19.822 24.901,22.129 L27.045,24.285 C27.992,23.964 29.078,24.173 29.833,24.932 L35.199,30.329 C36.261,31.396 36.261,33.127 35.199,34.194 ZM25.219,13.875 C25.219,7.541 20.112,2.406 13.812,2.406 C7.513,2.406 2.406,7.541 2.406,13.875 C2.406,20.209 7.513,25.344 13.812,25.344 C20.112,25.344 25.219,20.209 25.219,13.875 ZM13.797,24.156 C8.162,24.156 3.594,19.560 3.594,13.891 C3.594,8.221 8.162,3.625 13.797,3.625 C19.432,3.625 24.000,8.221 24.000,13.891 C24.000,19.560 19.432,24.156 13.797,24.156 ZM13.812,4.844 C8.842,4.844 4.812,8.894 4.812,13.891 C4.812,18.887 8.842,22.937 13.812,22.937 C18.783,22.937 22.812,18.887 22.812,13.891 C22.812,8.894 18.783,4.844 13.812,4.844 Z"/>
                    </svg>
                </button>
                </a>
            </div>
        </form>
    </div>
</section>
<? return $arResult; ?>