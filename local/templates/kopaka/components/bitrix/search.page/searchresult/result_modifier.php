<?
use Bitrix\Main\Loader;
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;

//if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!Loader::includeModule("iblock"))
{
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
}

$arIDs = array();
foreach($arResult["SEARCH"] as $k=>$item){
    $arIDs[]= $item["ITEM_ID"];
}
$arSelect = array(
    "ID",
    "IBLOCK_ID",
    "CODE",
    "XML_ID",
    "NAME",
    "ACTIVE",
    "DATE_ACTIVE_FROM",
    "DATE_ACTIVE_TO",
    "SORT",
    "PREVIEW_TEXT",
    "PREVIEW_TEXT_TYPE",
    "DETAIL_TEXT",
    "DETAIL_TEXT_TYPE",
    "DATE_CREATE",
    "CREATED_BY",
    "TIMESTAMP_X",
    "MODIFIED_BY",
    "TAGS",
    "IBLOCK_SECTION_ID",
    "DETAIL_PAGE_URL",
    "DETAIL_PICTURE",
    "PREVIEW_PICTURE",
    "PROPERTY_SPECIALOFFER",
    "PROPERTY_NEWPRODUCT",
    "PROPERTY_SALELEADER",
    "PROPERTY_STYLE",
    "PROPERTY_STYLE.PROPERTY_class_stone_color",
    "PROPERTY_STYLE.PROPERTY_code_stone_color"
);

$val = CIBlockElement::GetList(Array('sort'=>'asc'), array("ID"=>$arIDs),false,false,$arSelect);
$val->SetUrlTemplates($arParams["DETAIL_URL"]);

while($arSearchElement = $val->GetNext()) {
    //dump($arSearchElement);
    $arResult["SEARCH_ELEMENT"][] =  $arSearchElement;
}

$arParams["iblock_id"] =20;
$arParams["PRODUCT_DISPLAY_MODE"] = "Y";
$arParams["ADD_PROPERTIES_TO_BASKET"] = "Y";
$arParams["USE_PRODUCT_QUANTITY"] = "Y";
$arParams["ADD_TO_BASKET_ACTION"] = "ADD";

$arParams["PRODUCT_PROPERTIES"]=array(
    0 => "STONE",
    1 => "METAL",
    2 => "METAL_COLOR",
    3 => "STYLE",
    4 => "BELONG",
    5 => "VENDOR",
    6 => "COUNTRY",
    7 => "SPECIALOFFER",
    8 => "NEWPRODUCT",
    9 => "SALELEADER",
    10 => "RECOMMEND",
    11 => "PHOTONAME_COLOR_1",
    12 => "PHOTONAME_COLOR_2",
    13 => "PHOTONAME_COLOR_3",
    14 => "PHOTONAME_COLOR_4",
    15 => "PHOTONAME_COLOR_5",
    16 => "PHOTONAME_COLOR_6",
    17 => "PHOTONAME_COLOR_7",
    18 => "PHOTONAME_COLOR_8",
    19 => "PHOTONAME_COLOR_9",
    20 => "PHOTONAME_COLOR_10",
    21 => "CML2_ATTRIBUTES",
    22 => "CML2_TRAITS",
);

$arParams["PROPERTY_CODE"]=array(
    0 => "COLLECTION",
    1 => "STONE",
    2 => "METAL",
    3 => "METAL_COLOR",
    4 => "METAL_WEIGHT",
    5 => "STYLE",
    6 => "BELONG",
    7 => "VENDOR",
    8 => "COUNTRY",
    9 => "SPECIALOFFER",
    10 => "NEWPRODUCT",
    11 => "SALELEADER",
    12 => "TITLE",
    13 => "HEADER1",
    14 => "KEYWORDS",
    15 => "META_DESCRIPTION",
    16 => "MINIMUM_PRICE",
    17 => "MAXIMUM_PRICE",
    18 => "RECOMMEND",
    19 => "STONE_COLOR",
    20 => "PHOTONAME_COLOR_1",
    21 => "PHOTONAME_COLOR_2",
    22 => "PHOTONAME_COLOR_3",
    23 => "PHOTONAME_COLOR_4",
    24 => "PHOTONAME_COLOR_5",
    25 => "PHOTONAME_COLOR_6",
    26 => "PHOTONAME_COLOR_7",
    27 => "PHOTONAME_COLOR_8",
    28 => "PHOTONAME_COLOR_9",
    29 => "PHOTONAME_COLOR_10",
    30 => "CML2_ARTICLE",
    31 => "CML2_ATTRIBUTES",
    32 => "CML2_TRAITS",
    33 => "CML2_BASE_UNIT",
    34 => "CML2_TAXES",
    35 => "undefined",
    36 => "",
);

$arParams["OFFERS_PROPERTY_CODE"] = array(
    0 => "STD_SIZE",
    1 => "COLOR_STONE",
    2 => "CML2_ATTRIBUTES",
    3 => "CML2_TRAITS",
    4 => "CML2_BASE_UNIT",
    5 => "CML2_TAXES",
    6 => "FILES",
    7 => "CML2_BAR_CODE",
    8 => "",
);

$arParams["OFFERS_FIELD_CODE"] = array(
    0 => "ID",
    1 => "CODE",
    2 => "XML_ID",
    3 => "NAME",
    4 => "TAGS",
    5 => "SORT",
    6 => "PREVIEW_TEXT",
    7 => "PREVIEW_PICTURE",
    8 => "DETAIL_TEXT",
    9 => "DETAIL_PICTURE",
    10 => "DATE_ACTIVE_FROM",
    11 => "ACTIVE_FROM",
    12 => "DATE_ACTIVE_TO",
    13 => "ACTIVE_TO",
    14 => "SHOW_COUNTER",
    15 => "SHOW_COUNTER_START",
    16 => "IBLOCK_TYPE_ID",
    17 => "IBLOCK_ID",
    18 => "IBLOCK_CODE",
    19 => "IBLOCK_NAME",
    20 => "IBLOCK_EXTERNAL_ID",
    21 => "DATE_CREATE",
    22 => "CREATED_BY",
    23 => "CREATED_USER_NAME",
    24 => "TIMESTAMP_X",
    25 => "MODIFIED_BY",
    26 => "USER_NAME",
    27 => "",
);

$arParams["OFFER_TREE_PROPS"] = array(
    1 => "STD_SIZE",
    2 => "COLOR_STONE",
);

$arParams["ACTION_VARIABLE"] = "action";
$arParams["PRODUCT_ID_VARIABLE"] = "id";

$arResultModules = array(
    'iblock' => true,
    'catalog' => false,
    'currency' => false
);

$arResult['MODULES'] = $arResultModules;


global $CACHE_MANAGER;
$arConvertParams = array();
if ($arParams['CONVERT_CURRENCY'] == 'Y')
{
    if (!Loader::includeModule('currency'))
    {
        $arParams['CONVERT_CURRENCY'] = 'N';
        $arParams['CURRENCY_ID'] = '';
    }
    else
    {
        $arResultModules['currency'] = true;
        $currencyIterator = CurrencyTable::getList(array(
            'select' => array('CURRENCY'),
            'filter' => array('=CURRENCY' => $arParams['CURRENCY_ID'])
        ));
        if ($currency = $currencyIterator->fetch())
        {
            $arParams['CURRENCY_ID'] = $currency['CURRENCY'];
            $arConvertParams['CURRENCY_ID'] = $currency['CURRENCY'];
        }
        else
        {
            $arParams['CONVERT_CURRENCY'] = 'N';
            $arParams['CURRENCY_ID'] = '';
        }
        unset($currency, $currencyIterator);
    }
}
$arResult['CONVERT_CURRENCY'] = $arConvertParams;

$bIBlockCatalog = false;
$arCatalog = false;
$boolNeedCatalogCache = false;
$bCatalog = Loader::includeModule('catalog');
$arResult['MODULES']['catalog'] = $bCatalog;
if ($bCatalog)
{

    $arResultModules['catalog'] = true;
    $arResultModules['currency'] = true;
    $arCatalog = CCatalogSKU::GetInfoByIBlock($arParams["iblock_id"]);
    if (!empty($arCatalog) && is_array($arCatalog))
    {
        $bIBlockCatalog = $arCatalog['CATALOG_TYPE'] != CCatalogSKU::TYPE_PRODUCT;
        $boolNeedCatalogCache = true;
    }
}
$arResult['CATALOG'] = $arCatalog;

//This function returns array with prices description and access rights
//in case catalog module n/a prices get values from element properties
$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], array("BASE"));
$arResult['PRICES_ALLOW'] = CIBlockPriceTools::GetAllowCatalogPrices($arResult["PRICES"]);

$arResult["ITEMS"] = array();
$intKey = 0;
$arElementLink = array();

foreach($arResult["SEARCH_ELEMENT"] as $s=>$arItem) {

    $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
    $arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();
    $arItem["PROPERTIES"] = array();
    $arItem["DISPLAY_PROPERTIES"] = array();
    $arItem["PRODUCT_PROPERTIES"] = array();
    $arItem['PRODUCT_PROPERTIES_FILL'] = array();

    $arResult["ITEMS"][$intKey] = $arItem;
    $arResult["ELEMENTS"][$intKey] = $arItem["ID"];
    $arElementLink[$arItem['ID']] = &$arResult["ITEMS"][$intKey];
    unset($arResult["SEARCH_ELEMENT"][$intKey]);
    $intKey++;
    unset($arResult["SEARCH_ELEMENT"][$intKey]);
}
unset($arResult["SEARCH_ELEMENT"]);

$bGetPropertyCodes = !empty($arParams["PROPERTY_CODE"]);
$bGetProductProperties = !empty($arParams["PRODUCT_PROPERTIES"]);
$bGetProperties = $bGetPropertyCodes || $bGetProductProperties;

if (!empty($arResult["ELEMENTS"]) && ($bGetProperties || ($bCatalog && $boolNeedCatalogCache)))
{
    $arPropFilter = array(
        'ID' => $arResult["ELEMENTS"],
        'IBLOCK_ID' => $arParams['iblock_id']
    );

    CIBlockElement::GetPropertyValuesArray($arElementLink, $arParams["iblock_id"], $arPropFilter);

    foreach ($arResult["ITEMS"] as &$arItem)
    {
        if ($bCatalog && $boolNeedCatalogCache)
        {
            CCatalogDiscount::SetProductPropertiesCache($arItem['ID'], $arItem["PROPERTIES"]);
        }

        if ($bGetProperties)
        {
            foreach($arParams["PROPERTY_CODE"] as $pid)
            {
                if (!isset($arItem["PROPERTIES"][$pid]))
                    continue;
                $prop = &$arItem["PROPERTIES"][$pid];
                $boolArr = is_array($prop["VALUE"]);
                if(
                    ($boolArr && !empty($prop["VALUE"]))
                    || (!$boolArr && strlen($prop["VALUE"]) > 0)
                )
                {
                    $arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "catalog_out");
                }
            }

            if ($bGetProductProperties)
            {
                $arItem["PRODUCT_PROPERTIES"] = CIBlockPriceTools::GetProductProperties(
                    $arParams["IBLOCK_ID"],
                    $arItem["ID"],
                    $arParams["PRODUCT_PROPERTIES"],
                    $arItem["PROPERTIES"]
                );
                if (!empty($arItem["PRODUCT_PROPERTIES"]))
                {
                    $arItem['PRODUCT_PROPERTIES_FILL'] = CIBlockPriceTools::getFillProductProperties($arItem['PRODUCT_PROPERTIES']);
                }
            }
        }
    }
    if (isset($arItem))
        unset($arItem);
}


$currentPath = CHTTP::urlDeleteParams(
    $APPLICATION->GetCurPageParam(),
    array($arParams['PRODUCT_ID_VARIABLE'], $arParams['ACTION_VARIABLE']),
    array("delete_system_params" => true)
);
$currentPath .= (stripos($currentPath, '?') === false ? '?' : '&');
if ($arParams['COMPARE_PATH'] == '')
{
    $comparePath = $currentPath;
}
else
{
    $comparePath = $arParams['COMPARE_PATH'];
    $comparePath .= (stripos($comparePath, '?') === false ? '?' : '&');
}

$arResult['~BUY_URL_TEMPLATE'] = $arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
$arResult['BUY_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~BUY_URL_TEMPLATE']);
$arResult['~ADD_URL_TEMPLATE'] = $arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
$arResult['ADD_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~ADD_URL_TEMPLATE']);
$arResult['~SUBSCRIBE_URL_TEMPLATE'] = $currentPath.$arParams["ACTION_VARIABLE"]."=SUBSCRIBE_PRODUCT&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
$arResult['SUBSCRIBE_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~SUBSCRIBE_URL_TEMPLATE']);
$arResult['~COMPARE_URL_TEMPLATE'] = $comparePath.$arParams["ACTION_VARIABLE"]."=ADD_TO_COMPARE_LIST&".$arParams["PRODUCT_ID_VARIABLE"]."=#ID#";
$arResult['COMPARE_URL_TEMPLATE'] = htmlspecialcharsbx($arResult['~COMPARE_URL_TEMPLATE']);
unset($comparePath, $currentPath);

foreach ($arResult["ITEMS"] as &$arItem)
{
    $arItem["PRICES"] = array();
    $arItem["PRICE_MATRIX"] = false;
    $arItem['MIN_PRICE'] = false;
    if($arParams["USE_PRICE_COUNT"])
    {
        if ($bCatalog)
        {
            $arItem["PRICE_MATRIX"] = CatalogGetPriceTableEx($arItem["ID"], 0, $arPriceTypeID, 'Y', $arConvertParams);
            if (isset($arItem["PRICE_MATRIX"]["COLS"]) && is_array($arItem["PRICE_MATRIX"]["COLS"]))
            {
                foreach($arItem["PRICE_MATRIX"]["COLS"] as $keyColumn=>$arColumn)
                    $arItem["PRICE_MATRIX"]["COLS"][$keyColumn]["NAME_LANG"] = htmlspecialcharsbx($arColumn["NAME_LANG"]);
            }
        }
    }
    else
    {
        $arItem["PRICES"] = CIBlockPriceTools::GetItemPrices($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
        if (!empty($arItem["PRICES"]))
        {
            foreach ($arItem['PRICES'] as &$arOnePrice)
            {
                if ('Y' == $arOnePrice['MIN_PRICE'])
                {
                    $arItem['MIN_PRICE'] = $arOnePrice;
                    break;
                }
            }
            unset($arOnePrice);
        }
    }
    $arItem["CAN_BUY"] = CIBlockPriceTools::CanBuy($arParams["IBLOCK_ID"], $arResult["PRICES"], $arItem);

    $arItem['~BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~BUY_URL_TEMPLATE']);
    $arItem['BUY_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['BUY_URL_TEMPLATE']);
    $arItem['~ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~ADD_URL_TEMPLATE']);
    $arItem['ADD_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['ADD_URL_TEMPLATE']);
    $arItem['~SUBSCRIBE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~SUBSCRIBE_URL_TEMPLATE']);
    $arItem['SUBSCRIBE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['SUBSCRIBE_URL_TEMPLATE']);
    if ($arParams['DISPLAY_COMPARE'])
    {
        $arItem['~COMPARE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['~COMPARE_URL_TEMPLATE']);
        $arItem['COMPARE_URL'] = str_replace('#ID#', $arItem["ID"], $arResult['COMPARE_URL_TEMPLATE']);
    }

    if ('Y' == $arParams['CONVERT_CURRENCY'])
    {
        if ($arParams["USE_PRICE_COUNT"])
        {
            if (is_array($arItem["PRICE_MATRIX"]) && !empty($arItem["PRICE_MATRIX"]))
            {
                if (isset($arItem["PRICE_MATRIX"]['CURRENCY_LIST']) && is_array($arItem["PRICE_MATRIX"]['CURRENCY_LIST']))
                    $arCurrencyList = array_merge($arCurrencyList, $arItem["PRICE_MATRIX"]['CURRENCY_LIST']);
            }
        }
        else
        {
            if (!empty($arItem["PRICES"]))
            {
                foreach ($arItem["PRICES"] as &$arOnePrices)
                {
                    if (isset($arOnePrices['ORIG_CURRENCY']))
                        $arCurrencyList[] = $arOnePrices['ORIG_CURRENCY'];
                }
                if (isset($arOnePrices))
                    unset($arOnePrices);
            }
        }
    }
}

if(
    $bCatalog
    && !empty($arResult["ELEMENTS"])
    && (
        !empty($arParams["OFFERS_FIELD_CODE"])
        || !empty($arParams["OFFERS_PROPERTY_CODE"])
    )
)
{

    $offersFilter = array(
        'IBLOCK_ID' => $arParams['iblock_id'],
        'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE']
    );
    if (!$arParams["USE_PRICE_COUNT"])
    {
        $offersFilter['SHOW_PRICE_COUNT'] = $arParams['SHOW_PRICE_COUNT'];
    }
    $arOffers = CIBlockPriceTools::GetOffersArray(
        $offersFilter,
        $arResult["ELEMENTS"],
        array(
            $arParams["OFFERS_SORT_FIELD"] => $arParams["OFFERS_SORT_ORDER"],
            $arParams["OFFERS_SORT_FIELD2"] => $arParams["OFFERS_SORT_ORDER2"],
        ),
        $arParams["OFFERS_FIELD_CODE"],
        $arParams["OFFERS_PROPERTY_CODE"],
        $arParams["OFFERS_LIMIT"],
        $arResult["PRICES"],
        $arParams['PRICE_VAT_INCLUDE'],
        $arConvertParams
    );

    if(!empty($arOffers))
    {
        foreach ($arResult["ELEMENTS"] as $id)
        {
            $arElementLink[$id]['OFFERS'] = array();
        }

        foreach($arOffers as $arOffer)
        {
            if (isset($arElementLink[$arOffer["LINK_ELEMENT_ID"]]))
            {
                $arOffer['~BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~BUY_URL_TEMPLATE']);
                $arOffer['BUY_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['BUY_URL_TEMPLATE']);
                $arOffer['~ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~ADD_URL_TEMPLATE']);
                $arOffer['ADD_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['ADD_URL_TEMPLATE']);
                if ($arParams['DISPLAY_COMPARE'])
                {
                    $arOffer['~COMPARE_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~COMPARE_URL_TEMPLATE']);
                    $arOffer['COMPARE_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['COMPARE_URL_TEMPLATE']);
                }
                $arOffer['~SUBSCRIBE_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['~SUBSCRIBE_URL_TEMPLATE']);
                $arOffer['SUBSCRIBE_URL'] = str_replace('#ID#', $arOffer["ID"], $arResult['SUBSCRIBE_URL_TEMPLATE']);

                $arElementLink[$arOffer["LINK_ELEMENT_ID"]]['OFFERS'][] = $arOffer;

                if ('Y' == $arParams['CONVERT_CURRENCY'])
                {
                    if (!empty($arOffer['PRICES']))
                    {
                        foreach ($arOffer['PRICES'] as &$arOnePrices)
                        {
                            if (isset($arOnePrices['ORIG_CURRENCY']))
                                $arCurrencyList[] = $arOnePrices['ORIG_CURRENCY'];
                        }
                        if (isset($arOnePrices))
                            unset($arOnePrices);
                    }
                }
            }
        }
    }
}


/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
$arViewModeList = array('BANNER','SLIDER', 'SECTION');

$arDefaultParams = array(
    'VIEW_MODE' => 'SECTION',
    'TEMPLATE_THEME' => 'blue',
    'PRODUCT_DISPLAY_MODE' => 'N',
    'ADD_PICT_PROP' => '-',
    'LABEL_PROP' => '-',
    'OFFER_ADD_PICT_PROP' => '-',
    'OFFER_TREE_PROPS' => array('-'),
    'SHOW_DISCOUNT_PERCENT' => 'N',
    'SHOW_OLD_PRICE' => 'N',
    'ADD_TO_BASKET_ACTION' => 'ADD',
    'SHOW_CLOSE_POPUP' => 'Y',
    'ROTATE_TIMER' => 30,
    'SHOW_PAGINATION' => 'Y',
    'MESS_BTN_BUY' => '',
    'MESS_BTN_ADD_TO_BASKET' => '',
    'MESS_BTN_COMPARE' => '',
    'MESS_BTN_DETAIL' => '',
    'MESS_NOT_AVAILABLE' => ''
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!isset($arParams['LINE_ELEMENT_COUNT']))
    $arParams['LINE_ELEMENT_COUNT'] = 3;
$arParams['LINE_ELEMENT_COUNT'] = intval($arParams['LINE_ELEMENT_COUNT']);
if (2 > $arParams['LINE_ELEMENT_COUNT'] || 5 < $arParams['LINE_ELEMENT_COUNT'])
    $arParams['LINE_ELEMENT_COUNT'] = 3;

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
    $arParams['VIEW_MODE'] = 'SECTION';
$arParams['TEMPLATE_THEME'] = (string)($arParams['TEMPLATE_THEME']);
if ('' != $arParams['TEMPLATE_THEME'])
{
    $arParams['TEMPLATE_THEME'] = preg_replace('/[^a-zA-Z0-9_\-\(\)\!]/', '', $arParams['TEMPLATE_THEME']);
    if ('site' == $arParams['TEMPLATE_THEME'])
    {
        $arParams['TEMPLATE_THEME'] = COption::GetOptionString('main', 'wizard_eshop_adapt_theme_id', 'blue', SITE_ID);
    }
    if ('' != $arParams['TEMPLATE_THEME'])
    {
        if (!is_file($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/'.ToLower($arParams['VIEW_MODE']).'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
            $arParams['TEMPLATE_THEME'] = '';
    }
}
if ('' == $arParams['TEMPLATE_THEME'])
    $arParams['TEMPLATE_THEME'] = 'blue';

if ('Y' != $arParams['PRODUCT_DISPLAY_MODE'])
    $arParams['PRODUCT_DISPLAY_MODE'] = 'N';
if ('BANNER' == $arParams['VIEW_MODE'])
    $arParams['PRODUCT_DISPLAY_MODE'] = 'N';
$arParams['ADD_PICT_PROP'] = trim($arParams['ADD_PICT_PROP']);
if ('-' == $arParams['ADD_PICT_PROP'])
    $arParams['ADD_PICT_PROP'] = '';
$arParams['LABEL_PROP'] = trim($arParams['LABEL_PROP']);
if ('-' == $arParams['LABEL_PROP'])
    $arParams['LABEL_PROP'] = '';
$arParams['OFFER_ADD_PICT_PROP'] = trim($arParams['OFFER_ADD_PICT_PROP']);
if ('-' == $arParams['OFFER_ADD_PICT_PROP'])
    $arParams['OFFER_ADD_PICT_PROP'] = '';
if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
{
    if (!is_array($arParams['OFFER_TREE_PROPS']))
        $arParams['OFFER_TREE_PROPS'] = array($arParams['OFFER_TREE_PROPS']);
    foreach ($arParams['OFFER_TREE_PROPS'] as $key => $value)
    {
        $value = (string)$value;
        if ('' == $value || '-' == $value)
            unset($arParams['OFFER_TREE_PROPS'][$key]);
    }
    if (empty($arParams['OFFER_TREE_PROPS']) && isset($arParams['OFFERS_CART_PROPERTIES']) && is_array($arParams['OFFERS_CART_PROPERTIES']))
    {
        $arParams['OFFER_TREE_PROPS'] = $arParams['OFFERS_CART_PROPERTIES'];
        foreach ($arParams['OFFER_TREE_PROPS'] as $key => $value)
        {
            $value = (string)$value;
            if ('' == $value || '-' == $value)
                unset($arParams['OFFER_TREE_PROPS'][$key]);
        }
    }
}
else
{
    $arParams['OFFER_TREE_PROPS'] = array();
}
if ('Y' != $arParams['SHOW_DISCOUNT_PERCENT'])
    $arParams['SHOW_DISCOUNT_PERCENT'] = 'N';
if ('Y' != $arParams['SHOW_OLD_PRICE'])
    $arParams['SHOW_OLD_PRICE'] = 'N';
if ($arParams['ADD_TO_BASKET_ACTION'] != 'BUY')
    $arParams['ADD_TO_BASKET_ACTION'] = 'ADD';
if ($arParams['SHOW_CLOSE_POPUP'] != 'Y')
    $arParams['SHOW_CLOSE_POPUP'] = 'N';
$arParams['ROTATE_TIMER'] = intval($arParams['ROTATE_TIMER']);
if (0 > $arParams['ROTATE_TIMER'])
    $arParams['ROTATE_TIMER'] = 30;
$arParams['ROTATE_TIMER'] *= 1000;
if ('N' != $arParams['SHOW_PAGINATION'])
    $arParams['SHOW_PAGINATION'] = 'Y';
$arParams['MESS_BTN_BUY'] = trim($arParams['MESS_BTN_BUY']);
$arParams['MESS_BTN_ADD_TO_BASKET'] = trim($arParams['MESS_BTN_ADD_TO_BASKET']);
$arParams['MESS_BTN_COMPARE'] = trim($arParams['MESS_BTN_COMPARE']);
$arParams['MESS_BTN_DETAIL'] = trim($arParams['MESS_BTN_DETAIL']);
$arParams['MESS_NOT_AVAILABLE'] = trim($arParams['MESS_NOT_AVAILABLE']);

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

foreach($arResult["ITEMS"] as $i=>$arElement) {

    $arResult['ELEMENT'] = array();
    $arResult['ELEMENT'][] = $arElement;

    if (!empty($arResult['ELEMENT']))
    {

        $arEmptyPreview = false;
        $strEmptyPreview = $this->GetFolder().'/images/no_photo.png';
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview))
        {
            $arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);
            if (!empty($arSizes))
            {
                $arEmptyPreview = array(
                    'SRC' => $strEmptyPreview,
                    'WIDTH' => intval($arSizes[0]),
                    'HEIGHT' => intval($arSizes[1])
                );
            }
            unset($arSizes);
        }
        unset($strEmptyPreview);

        $arSKUPropList = array();
        $arSKUPropIDs = array();
        $arSKUPropKeys = array();
        $boolSKU = false;
        $strBaseCurrency = '';
        $boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

        if ($arResult['MODULES']['catalog'])
        {
            if (!$boolConvert)
                $strBaseCurrency = CCurrency::GetBaseCurrency();
//$arResult['ELEMENT'][0]["OFFERS"][0]["IBLOCK_ID"]
            $arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams["iblock_id"]);
            $boolSKU = !empty($arSKU) && is_array($arSKU);
            if ($boolSKU && !empty($arParams['OFFER_TREE_PROPS']) && 'Y' == $arParams['PRODUCT_DISPLAY_MODE'])
            {
                $arSKUPropList = CIBlockPriceTools::getTreeProperties(
                    $arSKU,
                    $arParams['OFFER_TREE_PROPS'],
                    array(
                        'PICT' => $arEmptyPreview,
                        'NAME' => '-'
                    )
                );

                $arNeedValues = array();
                CIBlockPriceTools::getTreePropertyValues($arSKUPropList, $arNeedValues);
                $arSKUPropIDs = array_keys($arSKUPropList);
                if (empty($arSKUPropIDs))
                    $arParams['PRODUCT_DISPLAY_MODE'] = 'N';
                else
                    $arSKUPropKeys = array_fill_keys($arSKUPropIDs, false);
            }
        }

        $arNewItemsList = array();
        foreach ($arResult['ELEMENT'] as $key => &$arItem)
        {
            $arItem['CHECK_QUANTITY'] = false;
            if (!isset($arItem['CATALOG_MEASURE_RATIO']))
                $arItem['CATALOG_MEASURE_RATIO'] = 1;
            if (!isset($arItem['CATALOG_QUANTITY']))
                $arItem['CATALOG_QUANTITY'] = 0;
            $arItem['CATALOG_QUANTITY'] = (
            0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
                ? floatval($arItem['CATALOG_QUANTITY'])
                : intval($arItem['CATALOG_QUANTITY'])
            );
            $arItem['CATALOG'] = false;
            if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION'])
                $arItem['CATALOG_SUBSCRIPTION'] = 'N';

            CIBlockPriceTools::getLabel($arItem, $arParams['LABEL_PROP']);

            $productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, $arParams['ADD_PICT_PROP']);
            if (empty($productPictures['PICT']))
                $productPictures['PICT'] = $arEmptyPreview;
            if (empty($productPictures['SECOND_PICT']))
                $productPictures['SECOND_PICT'] = $productPictures['PICT'];

            $arItem['PREVIEW_PICTURE'] = $productPictures['PICT'];
            $arItem['PREVIEW_PICTURE_SECOND'] = $productPictures['SECOND_PICT'];
            $arItem['SECOND_PICT'] = true;
            $arItem['PRODUCT_PREVIEW'] = $productPictures['PICT'];
            $arItem['PRODUCT_PREVIEW_SECOND'] = $productPictures['SECOND_PICT'];

            if ($arResult['MODULES']['catalog'])
            {
                $arItem['CATALOG'] = true;
                if (!isset($arItem['CATALOG_TYPE']))
                    $arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
                if (
                    (CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE'])
                    && !empty($arItem['OFFERS'])
                )
                {
                    $arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
                }
                switch ($arItem['CATALOG_TYPE'])
                {
                    case CCatalogProduct::TYPE_SET:
                        $arItem['OFFERS'] = array();
                        $arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
                        break;
                    case CCatalogProduct::TYPE_SKU:
                        break;
                    case CCatalogProduct::TYPE_PRODUCT:
                    default:
                        $arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
                        break;
                }
            }
            else
            {
                $arItem['CATALOG_TYPE'] = 0;
                $arItem['OFFERS'] = array();
            }

            if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
            {
                if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
                {

                    $arMatrixFields = $arSKUPropKeys;
                    $arMatrix = array();

                    $arNewOffers = array();
                    $boolSKUDisplayProperties = false;
                    $arItem['OFFERS_PROP'] = false;

                    $arDouble = array();
                    foreach ($arItem['OFFERS'] as $keyOffer => $arOffer)
                    {
                        $arOffer['ID'] = intval($arOffer['ID']);
                        if (isset($arDouble[$arOffer['ID']]))
                            continue;
                        $arRow = array();
                        foreach ($arSKUPropIDs as $propkey => $strOneCode)
                        {
                            $arCell = array(
                                'VALUE' => 0,
                                'SORT' => PHP_INT_MAX,
                                'NA' => true
                            );
                            if (isset($arOffer['DISPLAY_PROPERTIES'][$strOneCode]))
                            {
                                $arMatrixFields[$strOneCode] = true;
                                $arCell['NA'] = false;
                                if ('directory' == $arSKUPropList[$strOneCode]['USER_TYPE'])
                                {
                                    $intValue = $arSKUPropList[$strOneCode]['XML_MAP'][$arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']];
                                    $arCell['VALUE'] = $intValue;
                                }
                                elseif ('L' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE'])
                                {
                                    $arCell['VALUE'] = intval($arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE_ENUM_ID']);
                                }
                                elseif ('E' == $arSKUPropList[$strOneCode]['PROPERTY_TYPE'])
                                {
                                    $arCell['VALUE'] = intval($arOffer['DISPLAY_PROPERTIES'][$strOneCode]['VALUE']);
                                }
                                $arCell['SORT'] = $arSKUPropList[$strOneCode]['VALUES'][$arCell['VALUE']]['SORT'];
                            }
                            $arRow[$strOneCode] = $arCell;
                        }
                        $arMatrix[$keyOffer] = $arRow;


                        CIBlockPriceTools::clearProperties($arOffer['DISPLAY_PROPERTIES'], $arParams['OFFER_TREE_PROPS']);

                        $arOffer['SECOND_PICT'] = false;
                        $arOffer['PREVIEW_PICTURE_SECOND'] = false;

                        CIBlockPriceTools::setRatioMinPrice($arOffer, false);

                        $offerPictures = CIBlockPriceTools::getDoublePicturesForItem($arOffer, $arParams['OFFER_ADD_PICT_PROP']);
                        $arOffer['OWNER_PICT'] = empty($offerPictures['PICT']);
                        $arOffer['PREVIEW_PICTURE'] = false;
                        $arOffer['PREVIEW_PICTURE_SECOND'] = false;
                        $arOffer['SECOND_PICT'] = true;
                        if (!$arOffer['OWNER_PICT'])
                        {
                            if (empty($offerPictures['SECOND_PICT']))
                                $offerPictures['SECOND_PICT'] = $offerPictures['PICT'];
                            $arOffer['PREVIEW_PICTURE'] = $offerPictures['PICT'];
                            $arOffer['PREVIEW_PICTURE_SECOND'] = $offerPictures['SECOND_PICT'];
                        }
                        if ('' != $arParams['OFFER_ADD_PICT_PROP'] && isset($arOffer['DISPLAY_PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]))
                            unset($arOffer['DISPLAY_PROPERTIES'][$arParams['OFFER_ADD_PICT_PROP']]);

                        $arDouble[$arOffer['ID']] = true;
                        $arNewOffers[$keyOffer] = $arOffer;
                    }
                    $arItem['OFFERS'] = $arNewOffers;

                    $arUsedFields = array();
                    $arSortFields = array();

                    foreach ($arSKUPropIDs as $propkey => $strOneCode)
                    {
                        $boolExist = $arMatrixFields[$strOneCode];
                        foreach ($arMatrix as $keyOffer => $arRow)
                        {
                            if ($boolExist)
                            {
                                if (!isset($arItem['OFFERS'][$keyOffer]['TREE']))
                                    $arItem['OFFERS'][$keyOffer]['TREE'] = array();
                                $arItem['OFFERS'][$keyOffer]['TREE']['PROP_'.$arSKUPropList[$strOneCode]['ID']] = $arMatrix[$keyOffer][$strOneCode]['VALUE'];
                                $arItem['OFFERS'][$keyOffer]['SKU_SORT_'.$strOneCode] = $arMatrix[$keyOffer][$strOneCode]['SORT'];
                                $arUsedFields[$strOneCode] = true;
                                $arSortFields['SKU_SORT_'.$strOneCode] = SORT_NUMERIC;
                            }
                            else
                            {
                                unset($arMatrix[$keyOffer][$strOneCode]);
                            }
                        }
                    }

                    $arItem['OFFERS_PROP'] = $arUsedFields;
                    $arItem['OFFERS_PROP_CODES'] = (!empty($arUsedFields) ? base64_encode(serialize(array_keys($arUsedFields))) : '');

                    Collection::sortByColumn($arItem['OFFERS'], $arSortFields);

                    $arMatrix = array();
                    $intSelected = -1;
                    $arItem['MIN_PRICE'] = false;
                    $arItem['MIN_BASIS_PRICE'] = false;
                    foreach ($arItem['OFFERS'] as $keyOffer => $arOffer)
                    {
                        if (empty($arItem['MIN_PRICE']) && $arOffer['CAN_BUY'])
                        {
                            $intSelected = $keyOffer;
                            $arItem['MIN_PRICE'] = (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']);
                            $arItem['MIN_BASIS_PRICE'] = $arOffer['MIN_PRICE'];
                        }
                        $arSKUProps = false;
                        if (!empty($arOffer['DISPLAY_PROPERTIES']))
                        {
                            $boolSKUDisplayProperties = true;
                            $arSKUProps = array();
                            foreach ($arOffer['DISPLAY_PROPERTIES'] as &$arOneProp)
                            {
                                if ('F' == $arOneProp['PROPERTY_TYPE'])
                                    continue;
                                $arSKUProps[] = array(
                                    'NAME' => $arOneProp['NAME'],
                                    'VALUE' => $arOneProp['DISPLAY_VALUE']
                                );
                            }
                            unset($arOneProp);
                        }

                        $arOneRow = array(
                            'ID' => $arOffer['ID'],
                            'NAME' => $arOffer['~NAME'],
                            'TREE' => $arOffer['TREE'],
                            'DISPLAY_PROPERTIES' => $arSKUProps,
                            'PRICE' => (isset($arOffer['RATIO_PRICE']) ? $arOffer['RATIO_PRICE'] : $arOffer['MIN_PRICE']),
                            'BASIS_PRICE' => $arOffer['MIN_PRICE'],
                            'SECOND_PICT' => $arOffer['SECOND_PICT'],
                            'OWNER_PICT' => $arOffer['OWNER_PICT'],
                            'PREVIEW_PICTURE' => $arOffer['PREVIEW_PICTURE'],
                            'PREVIEW_PICTURE_SECOND' => $arOffer['PREVIEW_PICTURE_SECOND'],
                            'CHECK_QUANTITY' => $arOffer['CHECK_QUANTITY'],
                            'MAX_QUANTITY' => $arOffer['CATALOG_QUANTITY'],
                            'STEP_QUANTITY' => $arOffer['CATALOG_MEASURE_RATIO'],
                            'QUANTITY_FLOAT' => is_double($arOffer['CATALOG_MEASURE_RATIO']),
                            'MEASURE' => $arOffer['~CATALOG_MEASURE_NAME'],
                            'CAN_BUY' => $arOffer['CAN_BUY'],
                        );
                        $arMatrix[$keyOffer] = $arOneRow;
                    }
                    if (-1 == $intSelected)
                        $intSelected = 0;
                    if (!$arMatrix[$intSelected]['OWNER_PICT'])
                    {
                        $arItem['PREVIEW_PICTURE'] = $arMatrix[$intSelected]['PREVIEW_PICTURE'];
                        $arItem['PREVIEW_PICTURE_SECOND'] = $arMatrix[$intSelected]['PREVIEW_PICTURE_SECOND'];
                    }
                    $arItem['JS_OFFERS'] = $arMatrix;
                    $arItem['OFFERS_SELECTED'] = $intSelected;
                    $arItem['OFFERS_PROPS_DISPLAY'] = $boolSKUDisplayProperties;
                }
                else
                {
                    $arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromOffers(
                        $arItem['OFFERS'],
                        $boolConvert ? $arResult['CONVERT_CURRENCY']['CURRENCY_ID'] : $strBaseCurrency
                    );
                }
            }

            if (
                $arResult['MODULES']['catalog']
                && $arItem['CATALOG']
                &&
                ($arItem['CATALOG_TYPE'] == CCatalogProduct::TYPE_PRODUCT
                    || $arItem['CATALOG_TYPE'] == CCatalogProduct::TYPE_SET)
            )
            {
                CIBlockPriceTools::setRatioMinPrice($arItem, false);
            }

            if (!empty($arItem['DISPLAY_PROPERTIES']))
            {
                foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp)
                {
                    if ('F' == $arDispProp['PROPERTY_TYPE'])
                        unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
                }
            }
            $arItem['LAST_ELEMENT'] = 'N';
            $arNewItemsList[$key] = $arItem;
        }
        $arNewItemsList[$key]['LAST_ELEMENT'] = 'Y';
        $arResult['ELEMENT'][$key] = $arNewItemsList;
        $arResult['SKU_PROPS'] = $arSKUPropList;

        $arResult['DEFAULT_PICTURE'] = $arEmptyPreview;

        //dump($arResult['ELEMENT'][$key]);
        //dump($i);
        unset($arResult["ITEMS"][$i]);

        $arResult["ITEMS"][$i] = $arResult['ELEMENT'][$key][0];

        if ('SLIDER' == $arParams['VIEW_MODE'])
        {
            $arRows = array();
            while(!empty($arResult['ELEMENT']))
            {
                $arRows[] = array_splice($arResult['ELEMENT'], 0, $arParams["LINE_ELEMENT_COUNT"]);
            }
            $arResult['ELEMENT'] = $arRows;
        }
        $arResult['CURRENCIES'] = array();

        if ($arResult['MODULES']['currency'])
        {
            if ($boolConvert)
            {
                $currencyFormat = CCurrencyLang::GetFormatDescription($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);
                $arResult['CURRENCIES'] = array(
                    array(
                        'CURRENCY' => $arResult['CONVERT_CURRENCY']['CURRENCY_ID'],
                        'FORMAT' => array(
                            'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
                            'DEC_POINT' => $currencyFormat['DEC_POINT'],
                            'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
                            'DECIMALS' => $currencyFormat['DECIMALS'],
                            'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                            'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
                        )
                    )
                );
                unset($currencyFormat);
            }
            else
            {
                $currencyIterator = CurrencyTable::getList(array(
                    'select' => array('CURRENCY')
                ));
                while ($currency = $currencyIterator->fetch())
                {
                    $currencyFormat = CCurrencyLang::GetFormatDescription($currency['CURRENCY']);
                    $arResult['CURRENCIES'][] = array(
                        'CURRENCY' => $currency['CURRENCY'],
                        'FORMAT' => array(
                            'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
                            'DEC_POINT' => $currencyFormat['DEC_POINT'],
                            'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
                            'DECIMALS' => $currencyFormat['DECIMALS'],
                            'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                            'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
                        )
                    );
                }
                unset($currencyFormat, $currency, $currencyIterator);
            }
        }
    }

}


$arResult["TAGS_CHAIN"] = array();
if($arResult["REQUEST"]["~TAGS"])
{
    $res = array_unique(explode(",", $arResult["REQUEST"]["~TAGS"]));
    $url = array();
    foreach ($res as $key => $tags)
    {
        $tags = trim($tags);
        if(!empty($tags))
        {
            $url_without = $res;
            unset($url_without[$key]);
            $url[$tags] = $tags;
            $result = array(
                "TAG_NAME" => htmlspecialcharsex($tags),
                "TAG_PATH" => $APPLICATION->GetCurPageParam("tags=".urlencode(implode(",", $url)), array("tags")),
                "TAG_WITHOUT" => $APPLICATION->GetCurPageParam((count($url_without) > 0 ? "tags=".urlencode(implode(",", $url_without)) : ""), array("tags")),
            );
            $arResult["TAGS_CHAIN"][] = $result;
        }
    }
}

$arResult["SEARCH"] = $arResult["ITEMS"];
//dump($arResult["SEARCH"]);

?>