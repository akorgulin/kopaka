<?php
function AddOrderProperty($prop_id, $value, $order) {
    if (!strlen($prop_id)) {
        return false;
    }
    if (CModule::IncludeModule('sale')) {
        if ($arOrderProps = CSaleOrderProps::GetByID($prop_id)) {
            $db_vals = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $order, 'ORDER_PROPS_ID' => $arOrderProps['ID']));
            if ($arVals = $db_vals->Fetch()) {
                return CSaleOrderPropsValue::Update($arVals['ID'], array(
                    'NAME' => $arVals['NAME'],
                    'CODE' => $arVals['CODE'],
                    'ORDER_PROPS_ID' => $arVals['ORDER_PROPS_ID'],
                    'ORDER_ID' => $arVals['ORDER_ID'],
                    'VALUE' => $value,
                ));
            } else {
                return CSaleOrderPropsValue::Add(array(
                    'NAME' => $arOrderProps['NAME'],
                    'CODE' => $arOrderProps['CODE'],
                    'ORDER_PROPS_ID' => $arOrderProps['ID'],
                    'ORDER_ID' => $order,
                    'VALUE' => $value,
                ));
            }
    }
    }
}
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", "OnSaleComponentOrderOneStepCompleteHandler");   
function OnSaleComponentOrderOneStepCompleteHandler($ID, $arOrder){
    if($ID>0 && $arOrder["PERSON_TYPE_ID"]==1) {
        AddOrderProperty(25,$ID,$arOrder["ID"]);
    } else {
        AddOrderProperty(26,$ID,$arOrder["ID"]);
    }
}
function dump($var, $vardump = false, $return = false)
{
    static $dumpCnt;

    if (is_null($dumpCnt)) {
        $dumpCnt = 0;
    }
    ob_start();

    echo '<b>DUMP #' . $dumpCnt . ':</b> ';
    echo '<p>';
    echo '<pre>';
    if ($vardump) {
        var_dump($var);
    } else {
        print_r($var);
    }
    echo '</pre>';
    echo '</p>';

    $cnt = ob_get_contents();
    ob_end_clean();
    $dumpCnt++;
    if ($return) {
        return $cnt;
    } else {
        echo $cnt;
    }
}

function deleteGET($url, $name, $amp = true) {
    $url = str_replace("&amp;", "&", $url); // �������� �������� �� ���������, ���� ���������
    list($url_part, $qs_part) = array_pad(explode("?", $url), 2, ""); // ��������� URL �� 2 �����: �� ����� ? � �����
    parse_str($qs_part, $qs_vars); // ��������� ������ � �������� �� ������ � ����������� � �� ����������
    unset($qs_vars[$name]); // ������� ����������� ��������
    if (count($qs_vars) > 0) { // ���� ���� ���������
      $url = $url_part."?".http_build_query($qs_vars); // �������� URL �������
      if ($amp) $url = str_replace("&", "&amp;", $url); // �������� ���������� ������� �� ��������, ���� ���������
    }
    else $url = $url_part; // ���� ���������� �� ��������, �� ������ ���� ��, ��� ��� �� ����� ?
    return $url; // ���������� �������� URL
}

AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error(){

    if(!defined('ADMIN_SECTION') && defined('ERROR_404') && ERROR_404=='Y' || CHTTP::GetLastStatus() == "404 Not Found"){
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        define("error404chain",true);
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php");
        include($_SERVER["DOCUMENT_ROOT"].'/404.php');
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php");
    }
    return false;
}

AddEventHandler('main', 'OnBeforeProlog', '_CheckBasketPage', 1);
function _CheckBasketPage(){

    if(!defined('ADMIN_SECTION') && CHTTP::GetLastStatus() != "404 Not Found"){
        global $APPLICATION;
        $dir = $APPLICATION->GetCurDir();
        if($dir=="/personal/cart/" || $dir=="/personal/"){
            if ($_GET["BasketDelete"] and CModule::IncludeModule("sale"))
            {
                if(count($_POST["arItemsSelected"])>0 && $_POST["arItemsSelected"][0]>0) {
                    foreach($_POST["arItemsSelected"] as $i=>$id):
                        //CSaleBasket::Delete($id);
                    endforeach;
                } elseif($_GET["BasketDelete"]=="yes") {
                    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
                }
            }
        }
    }
    return false;
}
?>