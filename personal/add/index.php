<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавить технику");
?>


<section class="section content-section">

    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        );?>
    </div>
    <div class="container">
        <?$APPLICATION->IncludeComponent(
            "kopaka:iblock.element.add",
            "add_tech",
            array(
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "ALLOW_DELETE" => "N",
                "ALLOW_EDIT" => "N",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                "CUSTOM_TITLE_DETAIL_TEXT" => "",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                "CUSTOM_TITLE_NAME" => "",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                "CUSTOM_TITLE_TAGS" => "",
                "DEFAULT_INPUT_SIZE" => "30",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
                "ELEMENT_ASSOC" => "PROPERTY_ID",
                "ELEMENT_ASSOC_PROPERTY" => "54",
                "GROUPS" => array(
                    0 => "2",
                ),
                "IBLOCK_ID" => "2",
                "IBLOCK_TYPE" => "catalog",
                "LEVEL_LAST" => "N",
                "MAX_FILE_SIZE" => "100000",  
                "MAX_LEVELS" => "100000",
                "MAX_USER_ENTRIES" => "100000",
                "NAV_ON_PAGE" => "10",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
                "PROPERTY_CODES" => array(
                    0 => "49",
                    1 => "55",
                    2 => "56",
                    3 => "57",
                    4 => "45",
                    5 => "59",
                    6 => "60",
                    7 => "13",
                    8 => "47",
                    9 => "48",
                    10 => "76",
                    11 => "NAME",
                    12 => "DATE_ACTIVE_FROM",
                    13 => "DATE_ACTIVE_TO",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "49",
                    1 => "55",
                    2 => "56",
                    3 => "57",
                    4 => "45",
                    5 => "59",
                    6 => "60",
                    7 => "13",
                    8 => "47",
                    9 => "48",
                    //10 => "76",
                    11 => "NAME",
                    12 => "DATE_ACTIVE_FROM",
                    13 => "DATE_ACTIVE_TO",
                ),
                "RESIZE_IMAGES" => "N",
                "SEF_FOLDER" => "/personal/request",
                "SEF_MODE" => "Y",
                "STATUS" => "ANY",
                "STATUS_NEW" => "N",
                "USER_MESSAGE_ADD" => "",
                "USER_MESSAGE_EDIT" => "",
                "USE_CAPTCHA" => "N",
                "COMPONENT_TEMPLATE" => ".default"
            ),
            false
        );?>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>