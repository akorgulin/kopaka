<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Комментарии к товару");
?>


<section class="section content-section">

    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        );?>
    </div>
    <div class="container">
        <?$APPLICATION->IncludeComponent(
	"bitrix:forum.topic.reviews", 
	".default", 
	array(
		"SHOW_LINK_TO_FORUM" => "Y",
		"FILES_COUNT" => "2",
		"FORUM_ID" => "1",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "2",
		"ELEMENT_ID" => $_REQUEST["id"],
		"AJAX_POST" => "N",
		"POST_FIRST_MESSAGE" => "Y",
		"POST_FIRST_MESSAGE_TEMPLATE" => "#IMAGE#[url=#LINK#]#TITLE#[/url]#BODY#",
		"URL_TEMPLATES_READ" => "read.php?FID=#FID#&TID=#TID#",
		"URL_TEMPLATES_DETAIL" => "photo_detail.php?ID=#ELEMENT_ID#",
		"URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",
		"MESSAGES_PER_PAGE" => "10",
		"PAGE_NAVIGATION_TEMPLATE" => "",
		"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"EDITOR_CODE_DEFAULT" => "Y",
		"SHOW_AVATAR" => "Y",
		"SHOW_RATING" => "Y",
		"RATING_TYPE" => "like",
		"SHOW_MINIMIZED" => "N",
		"USE_CAPTCHA" => "Y",
		"PREORDER" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "0",
		"COMPONENT_TEMPLATE" => ".default",
		"NAME_TEMPLATE" => ""
	),
	false
);?>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>