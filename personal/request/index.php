<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Запрос на стоимость работ");
?>
<section class="section content-section">

    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","chain_template",Array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        );?>
    </div>
    <div class="container">
        <?$APPLICATION->IncludeComponent(
	"kopaka:iblock.element.add",
	"addrequest",
	array(
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"ALLOW_DELETE" => "N",
		"ALLOW_EDIT" => "N",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
		"ELEMENT_ASSOC" => "PROPERTY_ID",
		"ELEMENT_ASSOC_PROPERTY" => "70",
		"GROUPS" => array(
			0 => "2",
		),
		"IBLOCK_ID" => "20",
		"IBLOCK_TYPE" => "request",
		"LEVEL_LAST" => "N",
		"MAX_FILE_SIZE" => "10",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"NAV_ON_PAGE" => "10",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
		"PROPERTY_CODES" => array(
			0 => "64",
			1 => "61",
			2 => "65",
			3 => "62",
			4 => "63",
			5 => "66",
			6 => "67",
			7 => "68",
			8 => "69",
			9 => "NAME",
			10 => "DATE_ACTIVE_FROM",
			11 => "DATE_ACTIVE_TO",
		),
		"PROPERTY_CODES_REQUIRED" => array(
            0 => "64",
            1 => "61",
            2 => "65",
            3 => "62",
            4 => "63",
            5 => "66",
            6 => "67",
            7 => "68",
            8 => "69",
			9 => "NAME",
			10 => "DATE_ACTIVE_FROM",
			11 => "DATE_ACTIVE_TO",
		),
		"RESIZE_IMAGES" => "N",
		"SEF_FOLDER" => "/personal/request",
		"SEF_MODE" => "Y",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
    </div>
  </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>