<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

<!--404 page-->
<section class="content_wrapper page_404">
    <div class="container page_404_wrapper">
        <div class="row">
            <div class="col-12 center">
                <h1 class="page_heading" data-aos="flip-up" data-aos-delay="400">404</h1>
                <p data-aos="fade-up" data-aos-delay="800">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/404.php",
                        Array(),
                        Array("MODE"=>"html")
                    );?>
                </p>
                <div class="buttons_link"><a href="/" class="btn mint iconlink iconleft" data-aos="fade-up" data-aos-delay="1000"><span class="lnr lnr-arrow-left"></span>На главную</a>
                    <a href="/catalog/" class="btn mint iconlink" data-aos="fade-up" data-aos-delay="1200">Каталог<span class="lnr lnr-arrow-right"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?$APPLICATION->IncludeComponent("fijie:products", ".default", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "PRODUCTS_IBLOCK_CODE" => "clothes",
    "NEW_PRODUCTS" => "Y",
    "BESTSELLERS" => "Y",
    "SALE_PRODUCTS" => "Y",
    "BY" => "AMOUNT",
    "COUNT_ITEMS" => 20,
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "",

    "ADD_SECTION_CHAIN" => "Y",
    "ADD_ELEMENT_CHAIN" => "Y",
    "SET_STATUS_404" => "Y",
    "DETAIL_DISPLAY_NAME" => "N",
    "USE_ELEMENT_COUNTER" => "Y",
    "USE_FILTER" => "N",
    "FILTER_NAME" => "",
    "FILTER_VIEW_MODE" => "VERTICAL",
    "USE_COMPARE" => "N",
    "PRICE_CODE" => array(
        0 => "BASE",
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRICE_VAT_SHOW_VALUE" => "N",
    "PRODUCT_PROPERTIES" => "",
    "USE_PRODUCT_QUANTITY" => "Y",
    "CONVERT_CURRENCY" => "N",
    "QUANTITY_FLOAT" => "N",
    "OFFERS_CART_PROPERTIES" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
    ),
    "SHOW_TOP_ELEMENTS" => "N",
    "SECTION_COUNT_ELEMENTS" => "N",
    "SECTION_TOP_DEPTH" => "1",
    "SECTIONS_VIEW_MODE" => "TILE",
    "SECTIONS_SHOW_PARENT_NAME" => "N",
    "PAGE_ELEMENT_COUNT" => "15",
    "LINE_ELEMENT_COUNT" => "3",
    "ELEMENT_SORT_FIELD" => "desc",
    "ELEMENT_SORT_ORDER" => "asc",
    "ELEMENT_SORT_FIELD2" => "id",
    "ELEMENT_SORT_ORDER2" => "desc",
    "LIST_PROPERTY_CODE" => array(
        0 => "NEWPRODUCT",
        1 => "SALELEADER",
        2 => "SPECIALOFFER",
        3 => "",
    ),
    "INCLUDE_SUBSECTIONS" => "Y",
    "LIST_META_KEYWORDS" => "UF_KEYWORDS",
    "LIST_META_DESCRIPTION" => "UF_META_DESCRIPTION",
    "LIST_BROWSER_TITLE" => "UF_BROWSER_TITLE",
    "LIST_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "PREVIEW_PICTURE",
        2 => "DETAIL_PICTURE",
        3 => "",
    ),
    "LIST_OFFERS_PROPERTY_CODE" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
        3 => "MORE_PHOTO",
        4 => "ARTNUMBER",
        5 => "",
    ),
    "LIST_OFFERS_LIMIT" => "0",
    "SECTION_BACKGROUND_IMAGE" => "UF_BACKGROUND_IMAGE",
    "DETAIL_PROPERTY_CODE" => array(
        0 => "NEWPRODUCT",
        1 => "MANUFACTURER",
        2 => "MATERIAL",
    ),
    "DETAIL_META_KEYWORDS" => "KEYWORDS",
    "DETAIL_META_DESCRIPTION" => "META_DESCRIPTION",
    "DETAIL_BROWSER_TITLE" => "TITLE",
    "DETAIL_OFFERS_FIELD_CODE" => array(
        0 => "NAME",
        1 => "",
    ),
    "DETAIL_OFFERS_PROPERTY_CODE" => array(
        0 => "ARTNUMBER",
        1 => "SIZES_SHOES",
        2 => "SIZES_CLOTHES",
        3 => "COLOR_REF",
        4 => "MORE_PHOTO",
        5 => "",
    ),
    "DETAIL_BACKGROUND_IMAGE" => "BACKGROUND_IMAGE",
    "LINK_IBLOCK_TYPE" => "",
    "LINK_IBLOCK_ID" => "",
    "LINK_PROPERTY_SID" => "",
    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
    "USE_ALSO_BUY" => "Y",
    "ALSO_BUY_ELEMENT_COUNT" => "4",
    "ALSO_BUY_MIN_BUYES" => "1",
    "OFFERS_SORT_FIELD" => "sort",
    "OFFERS_SORT_ORDER" => "desc",
    "OFFERS_SORT_FIELD2" => "id",
    "OFFERS_SORT_ORDER2" => "desc",
    "PAGER_TEMPLATE" => "round",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
    "PAGER_SHOW_ALL" => "N",
    "ADD_PICT_PROP" => "MORE_PHOTO",
    "LABEL_PROP" => array(
        0 => "NEWPRODUCT",
    ),
    "PRODUCT_DISPLAY_MODE" => "Y",
    "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
    "OFFER_TREE_PROPS" => array(
        0 => "SIZES_SHOES",
        1 => "SIZES_CLOTHES",
        2 => "COLOR_REF",
        3 => "",
    ),
    "SHOW_DISCOUNT_PERCENT" => "Y",
    "SHOW_OLD_PRICE" => "Y",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_COMPARE" => "Сравнение",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Нет в наличии",
    "DETAIL_USE_VOTE_RATING" => "Y",
    "DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
    "DETAIL_USE_COMMENTS" => "Y",
    "DETAIL_BLOG_USE" => "Y",
    "DETAIL_VK_USE" => "N",
    "DETAIL_FB_USE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "USE_STORE" => "Y",
    "BIG_DATA_RCM_TYPE" => "personal",
    "FIELDS" => array(
        0 => "SCHEDULE",
        1 => "STORE",
        2 => "",
    ),
    "USE_MIN_AMOUNT" => "N",
    "STORE_PATH" => "/store/#store_id#",
    "MAIN_TITLE" => "Наличие на складах",
    "MIN_AMOUNT" => "10",
    "DETAIL_BRAND_USE" => "Y",
    "DETAIL_BRAND_PROP_CODE" => array(
        0 => "BRAND_REF",
        1 => "",
    ),
    "COMPATIBLE_MODE" => "N",
    "SIDEBAR_SECTION_SHOW" => "N",
    "SIDEBAR_DETAIL_SHOW" => "N",
    "SIDEBAR_PATH" => "/catalog/sidebar.php",
    "COMPONENT_TEMPLATE" => "catalog_card",
    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
    "LABEL_PROP_MOBILE" => array(
    ),

    "PROPERTY_CODE_MOBILE" => array(
    ),
    "PRODUCT_BLOCKS_ORDER" => "props,price,sku,quantityLimit,quantity,buttons",
    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
    "ENLARGE_PRODUCT" => "STRICT",
    "SHOW_SLIDER" => "Y",
    "SLIDER_INTERVAL" => "3000",
    "SLIDER_PROGRESS" => "N",
),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>