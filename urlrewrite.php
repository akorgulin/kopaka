<?php
$arUrlRewrite=array (
  1 => 
  array (
    'CONDITION' => '#^/bitrix/services/ymarket/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/services/ymarket/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/catalog/(.*)/(.*)/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/catalog/detail.php',
    'SORT' => 100,
  ),
  13 => 
  array (
    'CONDITION' => '#^/search/result/?.*#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/search/result.php',
    'SORT' => 100,
  ),
  11 => 
  array (
    'CONDITION' => '#^/brands/(.*)/(.*)#',
    'RULE' => 'BRAND=$1&PARAMS=$2',
    'ID' => '',
    'PATH' => '/brands/detail.php',
    'SORT' => 100,
  ),
  17 => 
  array (
    'CONDITION' => '#^/personal/request#',
    'RULE' => '',
    'ID' => 'kopaka:iblock.element.add',
    'PATH' => '/personal/update/index.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/personal/order/#',
    'RULE' => '',
    'ID' => 'bitrix:sale.personal.order',
    'PATH' => '/personal/order/index.php',
    'SORT' => 100,
  ),
  16 => 
  array (
    'CONDITION' => '#^/personal/#',
    'RULE' => '',
    'ID' => 'bitrix:sale.personal.section',
    'PATH' => '/personal/index.php',
    'SORT' => 100,
  ),
  12 => 
  array (
    'CONDITION' => '#^/blog/?.*#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/blog/detail.php',
    'SORT' => 100,
  ),
  10 => 
  array (
    'CONDITION' => '#^/catalog/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/catalog/index.php',
    'SORT' => 100,
  ),
  15 => 
  array (
    'CONDITION' => '#^/request#',
    'RULE' => '',
    'ID' => 'bitrix:iblock.element.add',
    'PATH' => '/request/index.php',
    'SORT' => 100,
  ),
  18 => 
  array (
    'CONDITION' => '#^/basket/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/order/index.php',
    'SORT' => 100,
  ),
  6 => 
  array (
    'CONDITION' => '#^/store/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog.store',
    'PATH' => '/store/index.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
);
