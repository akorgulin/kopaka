<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оферта");
?>
<!--About-->
<section class="about_us about_page" data-aos="fade-up" data-aos-delay="200">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="about_header" data-aos="flip-up" data-aos-delay="200">Оферта</h1>

            </div>
        </div>
    </div>
</section>


<?$APPLICATION->IncludeComponent("fijie:brands", "about", array(
    "SORT_FIELD" => "ID",
    "SORT_BY" => "DESC",
    "BRANDS_IBLOCK_CODE" => "brands",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "COUNT_RECORDS" => "",
),
    false
);?>


<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/include/advantages.php",
    Array(),
    Array("MODE"=>"text")
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>