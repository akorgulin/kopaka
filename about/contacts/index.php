<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Задайте вопрос");
?>
<!--Category-->
<section class="content_wrapper nobackground clearfix">
    <div class="container page_sidebar">
        <div class="row">
            <div class="col-8">
                <h1 class="page_header" data-aos="fade-up" data-aos-delay="300">Контакты:</h1>
                    <p><b>Телефон:</b> 8 (495) 212 85 06</p>
                    <p><b>Адрес:</b> г. Москва, ул. 2-я Хуторская, д. 38</p>
                    <iframe width="640" height="490" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.ru/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=%D0%B3.+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+%D1%83%D0%BB.+2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F,+%D0%B4.+38%D0%90&amp;aq=&amp;sll=55,103&amp;sspn=90.84699,270.527344&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+38,+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+127287&amp;ll=55.805478,37.569551&amp;spn=0.023154,0.054932&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.ru/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=%D0%B3.+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+%D1%83%D0%BB.+2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F,+%D0%B4.+38%D0%90&amp;aq=&amp;sll=55,103&amp;sspn=90.84699,270.527344&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=2-%D1%8F+%D0%A5%D1%83%D1%82%D0%BE%D1%80%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+38,+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0,+127287&amp;ll=55.805478,37.569551&amp;spn=0.023154,0.054932&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Просмотреть увеличенную карту</a></small>
                    <h2>Задать вопрос</h2>
                    <?$APPLICATION->IncludeComponent(
                    "fijie:main.feedback",
                    "eshop",
                    Array(
                        "USE_CAPTCHA" => "Y",
                        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                        "EMAIL_TO" => "sale@nyuta.bx",
                        "REQUIRED_FIELDS" => array(),
                        "EVENT_MESSAGE_ID" => array()
                    ),
                    false
                );?>

            </div>
            <div class="col-4 sidebar">
                <div class="grid_blocks category_blog clearfix">
                    <div class="item" data-aos="fade-up" data-aos-delay="400">
                        <div class="post_item" data-aos="fade-left" data-aos-delay="200">
                            <div class="image" data-aos="fade-left" data-aos-delay="300"> <a href="#"><img src="https://placeimg.com/310/200/any/1" alt="" class="js-tilt"></a> </div>
                            <div class="date" data-aos="fade-left" data-aos-delay="350">06 февраля 2019</div>
                            <div class="post_title" data-aos="fade-left" data-aos-delay="400"><a href="#">Хайлайтер: как заставить кожу сиять изнутри</a></div>
                            <div class="description" data-aos="fade-left" data-aos-delay="500">Спешим сообщить, что уникальный пептидный комплекс для лечения проблемной кожи Pim-Pim-Paul теперь можно приобрести в Институте красоты FIJIE! </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>
<!--Footer-->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>