import $ from 'jquery';
import { WOW } from 'wowjs';
import owlCarousel from 'owl.carousel';
import inputmask from 'inputmask/dist/min/jquery.inputmask.bundle.min';
require('select2/dist/js/select2.full.min');
require("@fancyapps/fancybox");

$(function(){
  console.log('Page scripts loaded!!!');

  const wow = new WOW();
  wow.init();

  $('.phone-control').inputmask('+7(999)999-99-99');
  $('select.styled-select').select2({
    placeholder: $(this).data('select-placeholder'),
  });

  // Init vehicles carousel
  initVehiclesCarousel();

  // Init dots animation
  initDotsAnimation();

  // Init select fields
  initSelectFields();

  // Init vehicles preview
  initVehiclesPreview();

  // Init vehicles list carousel
  initVehiclesCardsCarousel();

  // Init content tabs
  initContentTabs();

  // Init Card edit buttons
  initCardEditButton();

  // Init main menu dropdowns
  initMainMenuDropdowns();


  // Init main menu dropdowns
  initPhotosLoad();


  function initPhotosLoad(){
      var load_photo = $('#load_photo');

      load_photo.on('click', function (event) {
          $('#_imagesInput').click();
      });

      load_photo.on('change', function (event) {
          handleFileSelect(event);
      });
  }


  function initVehiclesCarousel(){
    var container = $('.our-vechicles-carousel');

    if(container.length === 0) return false;

    container.owlCarousel({
      items: 1,
      dots: true,
      nav: true,
      navText: [
        '<svg \n' +
        ' xmlns="http://www.w3.org/2000/svg"\n' +
        ' xmlns:xlink="http://www.w3.org/1999/xlink"\n' +
        ' width="9px" height="15px">\n' +
        '<path fill-rule="evenodd"  fill="rgb(86, 99, 124)"\n' +
        ' d="M3.360,6.995 L7.605,11.494 C8.146,12.066 8.146,12.997 7.605,13.571 C7.064,14.144 6.187,14.144 5.646,13.571 L0.422,8.033 C-0.120,7.460 -0.120,6.530 0.422,5.957 L5.646,0.420 C6.187,-0.154 7.064,-0.154 7.605,0.420 C8.146,0.993 8.146,1.922 7.605,2.496 L3.360,6.995 Z"/>\n' +
        '</svg>',
        '<svg \n' +
        ' xmlns="http://www.w3.org/2000/svg"\n' +
        ' xmlns:xlink="http://www.w3.org/1999/xlink"\n' +
        ' width="8px" height="15px">\n' +
        '<path fill-rule="evenodd"  fill="rgb(86, 99, 124)"\n' +
        ' d="M7.578,8.033 L2.354,13.571 C1.813,14.144 0.936,14.144 0.395,13.571 C-0.146,12.997 -0.146,12.066 0.395,11.494 L4.640,6.995 L0.395,2.496 C-0.146,1.922 -0.146,0.993 0.395,0.420 C0.936,-0.154 1.813,-0.154 2.354,0.420 L7.578,5.957 C8.119,6.530 8.119,7.460 7.578,8.033 Z"/>\n' +
        '</svg>'
      ],
      responsive: {
        0: {
          dots: false,
        },
        991: {
          dots: true,
        }
      }
    })
  }
  function initDotsAnimation() {
    var dotsList = $('.dots-line').find('.dot');
    var counter = 0;

    setActiveDot(counter);

    function setActiveDot(id){
      $(dotsList).removeClass('big');
      $(dotsList[id]).addClass('big');

      counter = (id < dotsList.length) ? ++id : 0;

      setTimeout(function(){
        setActiveDot(counter);
      }, 300);
    }
  }

  function handleFileSelect(evt) {
        var file = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                alert("Image only please....");
            }
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" width="90" height="90" title="', escape(theFile.name), '" src="', e.target.result, '" />'].join('');
                    document.getElementById('load_photo').insertBefore(span, null);
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
  }

  function initSelectFields() {
    var selectField = $('.js-example-basic-single');

    if(selectField.length === 0) return false;

    selectField.select2();
  }
  function initVehiclesPreview() {
    var mainContainer = $('.vehicles-main-view-carousel');
    var thumbsContainer = $('.vehicles-thumbs-view-carousel');
    var syncedSecondary = true;

    if(mainContainer.length === 0 || thumbsContainer.length === 0) return false;

    mainContainer.owlCarousel({
      items: 1,
      slideSpeed: 2000,
      autoplay: false,
      dots: false,
      nav: false,
      loop: true,
      responsiveRefreshRate: 200,
      navText: [
        '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
        '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
      ]
    }).on("changed.owl.carousel", syncPosition);

    thumbsContainer.on("initialized.owl.carousel", function() {
      thumbsContainer
          .find(".owl-item")
          .eq(0)
          .addClass("current");
      }).owlCarousel({
        items: 3,
        margin: 4,
        dots: true,
        nav: false,
        smartSpeed: 200,
        slideSpeed: 500,
        autoplay: false,
        slideBy: 3,
        responsiveRefreshRate: 100,
        mouseDrag: false
      })
      .on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
      var count = el.item.count - 1;
      var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

      if (current < 0) {
        current = count;
      }
      if (current > count) {
        current = 0;
      }
      thumbsContainer
        .find(".owl-item")
        .removeClass("current")
        .eq(current)
        .addClass("current");
      var onscreen = thumbsContainer.find(".owl-item.active").length - 1;
      var start = thumbsContainer
        .find(".owl-item.active")
        .first()
        .index();
      var end = thumbsContainer
        .find(".owl-item.active")
        .last()
        .index();

      if (current > end) {
        thumbsContainer.data("owl.carousel").to(current, 100, true);
      }
      if (current < start) {
        thumbsContainer.data("owl.carousel").to(current - onscreen, 100, true);
      }
    }
    function syncPosition2(el) {
      if (syncedSecondary) {
        var number = el.item.index;
        mainContainer.data("owl.carousel").to(number, 100, true);
      }
    }
    thumbsContainer.on("click", ".owl-item", function(e) {
      e.preventDefault();
      var number = $(this).index();
      mainContainer.data("owl.carousel").to(number, 300, true);
    });
  }
  function initVehiclesCardsCarousel() {
    var container = $('.vehicle-cards-carousel');

    if(container.length === 0) return false;

    container.owlCarousel({
      items: 3,
      margin: 30,
      dots: false,
      nav: false,
      navText: [
        '<svg \n' +
        ' xmlns="http://www.w3.org/2000/svg"\n' +
        ' xmlns:xlink="http://www.w3.org/1999/xlink"\n' +
        ' width="9px" height="15px">\n' +
        '<path fill-rule="evenodd"  fill="rgb(86, 99, 124)"\n' +
        ' d="M3.360,6.995 L7.605,11.494 C8.146,12.066 8.146,12.997 7.605,13.571 C7.064,14.144 6.187,14.144 5.646,13.571 L0.422,8.033 C-0.120,7.460 -0.120,6.530 0.422,5.957 L5.646,0.420 C6.187,-0.154 7.064,-0.154 7.605,0.420 C8.146,0.993 8.146,1.922 7.605,2.496 L3.360,6.995 Z"/>\n' +
        '</svg>',
        '<svg \n' +
        ' xmlns="http://www.w3.org/2000/svg"\n' +
        ' xmlns:xlink="http://www.w3.org/1999/xlink"\n' +
        ' width="8px" height="15px">\n' +
        '<path fill-rule="evenodd"  fill="rgb(86, 99, 124)"\n' +
        ' d="M7.578,8.033 L2.354,13.571 C1.813,14.144 0.936,14.144 0.395,13.571 C-0.146,12.997 -0.146,12.066 0.395,11.494 L4.640,6.995 L0.395,2.496 C-0.146,1.922 -0.146,0.993 0.395,0.420 C0.936,-0.154 1.813,-0.154 2.354,0.420 L7.578,5.957 C8.119,6.530 8.119,7.460 7.578,8.033 Z"/>\n' +
        '</svg>'
      ],
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        768: {
          items: 2,
          nav: true
        },
        992: {
          items: 3,
          nav: false
        }
      }
    })
  }
  function initContentTabs() {
    var tabs = $('.tabs').find('.tab-link');
    var tabsContent = $('.tabs-content').find('.tab-content');
    var currentActiveTab = 0;

    if(tabs.length === 0 || tabsContent.length === 0) return false;

    setActiveTab();

    tabs.on('click', function(e){
      e.preventDefault();

      setActiveTab($(this));
    });

    function setActiveTab(_this){

      clearActiveTabs();

      if(!_this){
        $(tabs[currentActiveTab]).addClass('active');
        $(tabsContent[currentActiveTab]).addClass('active');

        return false;
      }

      $(_this).addClass('active');
      $('#' + $(_this).data('tab-link')).addClass('active');
    }
    function clearActiveTabs(){
      tabs.removeClass('active');
      tabsContent.removeClass('active');
    }
  }
  function initCardEditButton() {
    var editBtn = $('.card-edit');

    if(editBtn.length === 0) return false;

    editBtn.on('click', function(e){
      e.stopPropagation();
      e.preventDefault();

      console.log('Edit clicked!');
    })
  }
  function initMainMenuDropdowns() {
    var dropdownItems = $('.menu-container .item.with-dropdown .dropdown-link');

    if(dropdownItems.length === 0) return false;

    dropdownItems.on('click', function(e) {
      e.stopPropagation();
      e.preventDefault();

      if($(this).closest('.with-dropdown').hasClass('open')){
        closeAllDropdowns();
      } else {
        closeAllDropdowns();
        openDropdownItem($(this));
      }
    });

    function openDropdownItem(_this){
      _this.addClass('open');
      _this.closest('.with-dropdown').addClass('open');
      _this.closest('.with-dropdown').find('.dropdown').addClass('open');
    }
    function closeAllDropdowns(){
      dropdownItems.removeClass('open');
      dropdownItems.closest('.with-dropdown').removeClass('open');
      dropdownItems.closest('.with-dropdown').find('.dropdown').removeClass('open');
    }
  }
});