<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Блог");
?>
<?
$blog = "fijiblognew";
$category = "";
$day = "";
$month = "";
$year = "";
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:blog.blog",
	".default",
	array(
		"BLOG_URL" => $blog,
		"BLOG_VAR" => "",
		"CACHE_TIME" => "7200",
		"CACHE_TIME_LONG" => "604600",
		"CACHE_TYPE" => "N",
		"CATEGORY_ID" => $category,
		"DATE_TIME_FORMAT" => "d.m.Y H:i:s",
		"DAY" => $day,
		"FILTER_NAME" => "arFilter",
		"IMAGE_MAX_HEIGHT" => "600",
		"IMAGE_MAX_WIDTH" => "600",
		"MESSAGE_COUNT" => "12",
		"MONTH" => $month,
		"NAV_TEMPLATE" => "",
		"PAGE_VAR" => "",
		"PATH_TO_BLOG" => "",
		"PATH_TO_BLOG_CATEGORY" => "",
		"PATH_TO_POST" => "",
		"PATH_TO_POST_EDIT" => "",
		"PATH_TO_SMILE" => "",
		"PATH_TO_USER" => "",
		"POST_PROPERTY_LIST" => array(
			0 => "UF_BLOG_POST_PIC",
			1 => "UF_BLOG_POST_GROUP",
			2 => "UF_BLOG_POST_CAT"
		),
		"POST_VAR" => "",
		"RATING_TYPE" => "",
		"SEO_USER" => "N",
		"SET_NAV_CHAIN" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_RATING" => "",
		"USER_VAR" => "",
		"YEAR" => $year,
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>

<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>