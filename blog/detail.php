<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");


$blog = "fijiblognew";
$category = "";
$day = "";
$month = "";
$year = "";
$id = $_REQUEST["id"];
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:blog.post",
    "",
    Array(
        "BLOG_URL" => $blog,
        "BLOG_VAR" => "",
        "CACHE_TIME" => "86400",
        "CACHE_TYPE" => "A",
        "DATE_TIME_FORMAT" => "d.m.Y",
        "ID" => $id,
        "IMAGE_MAX_HEIGHT" => "600",
        "IMAGE_MAX_WIDTH" => "600",
        "PAGE_VAR" => "",
        "PATH_TO_BLOG" => "",
        "PATH_TO_BLOG_CATEGORY" => "",
        "PATH_TO_POST_EDIT" => "",
        "PATH_TO_SMILE" => "",
        "PATH_TO_USER" => "",
        "POST_PROPERTY" => array(
            0 => "UF_BLOG_POST_PIC",
            1 => "UF_BLOG_POST_GROUP",
        ),
        "POST_VAR" => "",
        "RATING_TYPE" => "",
        "SEO_USE" => "Y",
        "SEO_USER" => "N",
        "SET_NAV_CHAIN" => "Y",
        "SET_TITLE" => "Y",
        "SHOW_RATING" => "",
        "USER_VAR" => ""
    )
);?>
<?$APPLICATION->IncludeComponent("bitrix:subscribe.form","",Array(
        "USE_PERSONALIZATION" => "Y",
        "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
        "SHOW_HIDDEN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600"
    )
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>