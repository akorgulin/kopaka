<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавить пост в блог Fijie");

$blog = "fijiblognew";
$category = "";
$day = "";
$month = "";
$year = "";
?><?$APPLICATION->IncludeComponent(
    "bitrix:blog.post.edit",
    "",
    Array(
        "ALLOW_POST_CODE" => "Y",
        "ALLOW_POST_MOVE" => "N",
        "BLOG_URL" => $blog,
        "BLOG_VAR" => "",
        "DATE_TIME_FORMAT" => "d F Y",
        "EDITOR_CODE_DEFAULT" => "N",
        "EDITOR_DEFAULT_HEIGHT" => "300",
        "EDITOR_RESIZABLE" => "Y",
        "ID" => $id,
        "IMAGE_MAX_HEIGHT" => "600",
        "IMAGE_MAX_WIDTH" => "600",
        "PAGE_VAR" => "",
        "PATH_TO_BLOG" => "",
        "PATH_TO_DRAFT" => "",
        "PATH_TO_POST" => "",
        "PATH_TO_POST_EDIT" => "",
        "PATH_TO_SMILE" => "",
        "PATH_TO_USER" => "",
        "POST_PROPERTY" => array("UF_BLOG_POST_PIC", "UF_BLOG_POST_GROUP", "UF_BLOG_POST_CAT"),
        "POST_VAR" => "",
        "SEO_USE" => "Y",
        "SET_TITLE" => "Y",
        "SMILES_COUNT" => "4",
        "USER_CONSENT" => "N",
        "USER_CONSENT_ID" => "0",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N",
        "USER_VAR" => ""
    )
);?><br>